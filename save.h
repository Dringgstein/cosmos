//
// Created by efry on 4/11/2017.
//

#ifndef COSMO_ENGINE_SAVE_H
#define COSMO_ENGINE_SAVE_H

#include "defines.h"

namespace cosmos_engine
{


typedef struct SaveGameData {
    uint8 health = 0;
    uint32 score = 0;
    uint16 num_stars_collected = 0;
    uint16 current_level = 0;
    uint16 num_bombs = 0;
    uint8 num_health_bars = 0;
    uint16 cheats_used_flag = 0;
    uint16 has_had_bomb_flag = 0;
    uint8 show_monster_attack_hint = 0;
    uint8 knows_about_powerups_flag = 0;
} SaveGameData;

typedef enum SaveStatus {
    LOADED,
    FILE_IO_ERROR,
    CRC_ERROR
} SaveStatus;

//Data
//extern uint16 cheats_used_flag;

void write_savegame_file(char suffix);
bool load_savegame_file(char suffix);
//SaveStatus load_savegame_data_from_file(char suffix, SaveGameData *data);

bool saveXMLGameState();
bool loadXMLGameState();

};

#endif //COSMO_ENGINE_SAVE_H
