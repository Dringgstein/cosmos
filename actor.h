#ifndef COSMO_ENGINE_ACTOR_H
#define COSMO_ENGINE_ACTOR_H

#include <memory>
#include <base/Singleton.h>
#include <engine/core/CMap.h>
#include "defines.h"
#include "tile.h"

#include <vector>

typedef struct ActorData
{

    void init_struct(int actor_init_cur_actor_num,
                           int actorInfoIndex,
                           int x,
                           int y,
                           int update_while_offscreen,
                           int can_update_if_goes_offscreen,
                           int can_fall_down,
                           int is_non_blocking,
                           void (*update_function)(ActorData *),
                           int data_1,
                           int data_2,
                           int data_3,
                           int data_4,
                           int data_5);

    void update(const bool draw_only);



    int actorInfoIndex = 0;
    int frame_num = 0;
    float x = 0;
    float y = 0;
    int update_while_off_screen_flag = 0;
    int can_update_if_goes_off_screen_flag = 0;
    int non_blocking_flag_maybe = 0;
    int can_fall_down_flag = 0;
    int has_moved_left_flag = 0;
    int has_moved_right_flag = 0;
    uint16 data_1 = 0;
    uint16 data_2 = 0;
    uint16 data_3 = 0;
    uint16 data_4 = 0;
    uint16 data_5 = 0;
    int is_deactivated_flag_maybe = 0;
    int falling_counter = 0;
    int count_down_timer = 0;

    void (*update_function)(struct ActorData *) = nullptr;
    void (*draw_function)(struct ActorData *)  = nullptr;

} ActorData;

extern std::vector< ActorData > actors;

//Data
extern uint16 question_mark_code;
extern bool speech_bubble_hamburger_shown_flag;
extern bool speech_bubble_red_plant_shown_flag;
extern bool speech_bubble_special_switch_shown_flag;
extern bool speech_bubble_clam_trap_shown_flag;
extern bool speech_bubble_silver_robot_shown_flag;
extern bool speech_bubble_purple_boss_shown_flag;
extern bool speech_bubble_pipe_shown_flag;
extern bool speech_bubble_teleporter_shown_flag;
extern bool speech_bubble_hoverboard_shown_flag;
extern bool speech_bubble_rubber_wall_shown_flag;
extern bool speech_bubble_floor_spring_shown_flag;

extern uint8 protection_barrier_active;

extern uint16 rnd_index;

extern uint8 energy_beam_enabled_flag;

extern uint16 num_containers;

extern uint16 num_eye_plants_remaining_to_blow_up;

extern uint16 brightness_effect_enabled_flag;
extern uint16 obj_switch_151_flag;


extern uint8 move_platform_flag;
extern DrawMode actor_tile_display_func_index;

int is_sprite_on_screen(int actorInfoIndex, int frame_num, int x_pos, int y_pos);
BlockingType sprite_blocking_check(int blocking_dir, int actorInfoIndex, int frame_num, int x_pos, int y_pos);

void actor_add_new(int image_index, int x_pos, int y_pos);
int actor_init(const int actor_num, int image_index, int x_pos1, int y_pos1);
void load_actor(int actor_num, int actorType, int x_pos, int y_pos);
void actor_load_tiles();


#define gActorMan cosmos_engine::ActorManager::get()

namespace cosmos_engine
{


class ActorManager : public GsSingleton<ActorManager>
{
public:

    void setMapPtr(std::shared_ptr<CMap> &mapPtr);


    void display_sprite_maybe(const int actorInfoIndex,
                              const int frame_num,
                              const float x_pos,
                              const float y_pos,
                              const DrawMode draw_mode);

    void display_sprite_maybe(const int actorInfoIndex,
                              const int frame_num,
                              const float x_pos,
                              const float y_pos,
                              const DrawMode draw_mode,
                              const GsVec2D<int> &scroll);


    std::shared_ptr<CMap> mpMap;
};

};

void display_sprite_maybe(const int actorInfoIndex,
                                const int frame_num,
                                const float x_pos,
                                const float y_pos,
                                const int tile_display_func_index);

uint16 cosmo_random();

void actor_update_all(const bool draw_only);

TileInfo *actor_get_tile_info(int actorInfoIndex, int frame_num);

ActorData *get_actor(uint16 actor_num);

#endif //COSMO_ENGINE_ACTOR_H
