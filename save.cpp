//
// Created by efry on 4/11/2017.
//

#include <stdlib.h>
#include "save.h"
#include "defines.h"
#include "player.h"
#include "game.h"
#include "map.h"
#include "dialog.h"
#include "config.h"
#include "status.h"

#include <fileio/CSaveGameController.h>
#include <engine/core/CBehaviorEngine.h>

int cleanup_and_exit();

namespace cosmos_engine
{

static SaveGameData temp_save;

uint16 cheats_used_flag = 0;



void write_savegame_file(char suffix)
{
    if(suffix == 'T')
    {
        temp_save.health = health;
        temp_save.score = score;
        temp_save.num_stars_collected = gStatus.numStartsCollected();
        temp_save.current_level = current_level;
        temp_save.num_bombs = num_bombs;
        temp_save.num_health_bars = num_health_bars;
        temp_save.cheats_used_flag = cheats_used_flag;
        temp_save.has_had_bomb_flag = has_had_bomb_flag;
        temp_save.show_monster_attack_hint = show_monster_attack_hint;
        temp_save.knows_about_powerups_flag = knows_about_powerups_flag;
    }
   /* else
    {
        char filename[13];
        char *path;
        sprintf(filename, "COSMO%d.SV%c", get_episode_number(), suffix);
        path = get_save_dir_full_path(filename);
        File savefile;
        if(!file_open(path, "wb", &savefile))
        {
            printf("Error: saving file %s\n", path);
            free(path);
            return;
        }
        free(path);

        file_write2(health, &savefile);
        file_write4(score, &savefile);
        file_write2(gStatus.numStartsCollected(), &savefile);
        file_write2(current_level, &savefile);
        file_write2(num_bombs, &savefile);
        file_write2(num_health_bars, &savefile);
        file_write2(cheats_used_flag, &savefile);
        file_write2(has_had_bomb_flag, &savefile);
        file_write2(show_monster_attack_hint, &savefile);
        file_write2(knows_about_powerups_flag, &savefile);
        uint16 checksum = (uint16)(health + gStatus.numStartsCollected() + current_level + num_bombs + num_health_bars);
        file_write2(checksum, &savefile);

        file_close(&savefile);
    }*/
}

bool load_savegame_file(char suffix)
{
    if(suffix == 'T')
    {
         health = temp_save.health;
         score = temp_save.score;
         gStatus.setNumStartsCollected(temp_save.num_stars_collected);
         current_level = temp_save.current_level;
         num_bombs = temp_save.num_bombs;
         num_health_bars = temp_save.num_health_bars;
         cheats_used_flag = temp_save.cheats_used_flag;
//         has_had_bomb_flag = temp_save.has_had_bomb_flag;
//         show_monster_attack_hint = temp_save.show_monster_attack_hint;
//         knows_about_powerups_flag = temp_save.knows_about_powerups_flag;
    }
/*    else
    {

        SaveGameData data;
        SaveStatus status = load_savegame_data_from_file(suffix, &data);

        if(status == FILE_IO_ERROR)
        {
            return false;
        }

        if(status == CRC_ERROR)
        {
            malformed_savegame_dialog();
            exit(cleanup_and_exit());
        }

        health = data.health;
        score = data.score;
        gStatus.setNumStartsCollected(data.num_stars_collected);
        current_level = data.current_level;
        num_bombs = data.num_bombs;
        num_health_bars = data.num_health_bars;
        cheats_used_flag = data.cheats_used_flag;
        has_had_bomb_flag = data.has_had_bomb_flag;
        show_monster_attack_hint = data.show_monster_attack_hint;
        knows_about_powerups_flag = data.knows_about_powerups_flag;
    }*/

    return true;
}

/*
SaveStatus load_savegame_data_from_file(char suffix, SaveGameData *data)
{
    File file;
    char filename[13];
    char *path;
    sprintf(filename, "COSMO%d.SV%c", get_episode_number(), suffix);

    path = get_save_dir_full_path(filename);
    int status = file_open(path, "rb", &file);
    free(path);

    if(!status)
    {
        return FILE_IO_ERROR;
    }

    data->health = (uint8)file_read2(&file);
    data->score = file_read4(&file);
    data->num_stars_collected = file_read2(&file);
    data->current_level = file_read2(&file);
    data->num_bombs = file_read2(&file);
    data->num_health_bars = (uint8)file_read2(&file);
    data->cheats_used_flag = file_read2(&file);
    data->has_had_bomb_flag = file_read2(&file);;
    data->show_monster_attack_hint = (uint8)file_read2(&file);;
    data->knows_about_powerups_flag = (uint8)file_read2(&file);;

    uint16 checksum = file_read2(&file);

    file_close(&file);

    if(checksum != data->health + data->num_stars_collected + data->current_level + data->num_bombs + data->num_health_bars)
    {
        return CRC_ERROR;
    }

    return LOADED;
}
*/

bool saveXMLGameState()
{
    /// Create tree
    using GsKit::ptree;
    ptree pt;

    /// Create the nodes and store the data as needed
    ptree &stateNode = pt.add("GameState", "");

    /// Save the Game in the CSavedGame object
    // store the episode, level and difficulty
    stateNode.put("difficulty", gBehaviorEngine.mDifficulty);

    // Save number of Players
    const size_t numPlayers = size_t(gBehaviorEngine.numPlayers());
    stateNode.put("NumPlayers", numPlayers);

    stateNode.put("NumStars", gStatus.numStartsCollected());
    stateNode.put("current_level", current_level);

/*
    ptree &deadNode = pt.add("death", "");
    for (size_t id = 0 ; id < mDead.size() ; id++)
    {
        deadNode.put("<xmlattr>.player", id);
        deadNode.put("<xmlattr>.dead", mDead[id]);
        deadNode.put("<xmlattr>.gameover", mGameOver[id]);
    }
*/
    for (size_t id=0 ; id<numPlayers ; id++)
    {
        ptree &playerNode = stateNode.add("Player", "");
        playerNode.put("<xmlattr>.id", id);
        playerNode.put("<xmlattr>.health", health);
        playerNode.put("<xmlattr>.score", score);
        playerNode.put("<xmlattr>.num_bombs", num_bombs);
        playerNode.put("<xmlattr>.num_health_bars", num_health_bars);
        playerNode.put("<xmlattr>.has_had_bomb_flag", has_had_bomb_flag);
        playerNode.put("<xmlattr>.show_monster_attack_hint", show_monster_attack_hint);
        playerNode.put("<xmlattr>.knows_about_powerups_flag", knows_about_powerups_flag);
    }

    if( gSaveGameController.saveXMLTree(pt) )
    {
        return true;
    }

    return false;
}

bool loadXMLGameState()
{
    try {
        /// Create tree
        using GsKit::ptree;
        ptree pt;

        CSaveGameController &savedGame = gSaveGameController;
        if(!savedGame.loadXMLTree(pt))
            return false;

        /// Load the nodes and retrieve the data as needed
        ptree &gameStateNodes = pt.get_child("GameState");

        const size_t numPlayers = gameStateNodes.get<int>("NumPlayers", int(30));
        gBehaviorEngine.setNumPlayers(numPlayers);

        gBehaviorEngine.mDifficulty =
                static_cast<Difficulty>(gameStateNodes.get<int>("difficulty", int(NORMAL) ));

        const size_t stars = gameStateNodes.get<int>("NumStars", int(0));
        gStatus.setNumStartsCollected(stars);

        const int level = gameStateNodes.get<int>("current_level", int(0) );
        current_level = level;


        for( auto &gsNode : gameStateNodes )
        {
            if(gsNode.first == "Player")
            {
                for(auto &playerNode : gsNode.second)
                {
                    if(playerNode.first == "<xmlattr>")
                    {
                        health = playerNode.second.get<int>("health", 3);
                        score = playerNode.second.get<int>("score", 0);
                        num_bombs = playerNode.second.get<int>("num_bombs", 0);
                        num_health_bars = playerNode.second.get<int>("num_health_bars", 0);
                        has_had_bomb_flag = playerNode.second.get<int>("has_had_bomb_flag", 0);
                        show_monster_attack_hint = playerNode.second.get<int>("show_monster_attack_hint", 0);
                        knows_about_powerups_flag = playerNode.second.get<int>("knows_about_powerups_flag", 0);
                    }
                }
            }
        }

        return true;

    }  catch (...) {
        gLogging << "Error reading the savegame node." << CLogFile::endl;
    }

    return false;
}


};
