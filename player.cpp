#include "sound/sfx.h"
#include "actor.h"
#include "dialog.h"
#include "map.h"
#include "input.h"
#include "tile.h"
#include "util.h"
#include "video.h"
#include "save.h"
#include "status.h"
#include "effects.h"
#include "player.h"

#include "base/video/CVideoDriver.h"
#include "base/GsLogging.h"
#include "graphics/GsGraphics.h"

#include <engine/core/CBehaviorEngine.h>

const sint16 player_x_offset_tbl[] = { 0, 0, 1, 1, 1, 0, -1, -1, -1 };
const sint16 player_y_offset_tbl[] = { 0, -1, -1, 0, 1, 1, 1, 0, -1 };

int walk_anim_index = 0;
int player_is_being_pushed_flag = 0;

int player_direction = 0;
int watchingDirection = 0;

uint16 player_sprite_dir_frame_offset = 0;
int player_input_jump_related_flag = 0;

int player_bounce_height_counter = 0;
int player_bounce_flag_maybe = 0;

int is_standing_slipry_slope_left_flg = 0;
int is_standing_slipry_slope_right_flg = 0;

uint8 health = 0;
uint8 num_health_bars = 0;

uint16 num_bombs = 0;
uint16 has_had_bomb_flag = 0;

int teleporter_state_maybe = 0;
int player_is_teleporting_flag = 0;
int teleporter_counter = 0;

uint8 player_in_pneumatic_tube_flag = 0;

uint16 player_invincibility_counter = 0;

int player_fall_off_map_bottom_counter = 0;
int num_hits_since_touching_ground = 0;

uint16 player_push_direction = 0;
uint16 player_push_anim_duration_maybe = 0;
uint16 player_push_anim_counter = 0;
uint16 player_push_duration = 0;
uint16 player_push_frame_num = 0;
uint8 player_dont_push_while_jumping_flag = 0;
uint16 player_push_check_blocking_flag = 0;

int player_idle_counter;
uint16 walk_counter;
int player_falling_state;
bool hitDetectionWithPlayer;
int hide_player_sprite;
int player_spring_jump_flag;
int player_lookstate;
int player_hoverboard_counter;

unsigned char jumpStateIdx;
unsigned char riding_hoverboard;

uint8 show_monster_attack_hint = 0;
bool cheat_hack_mover_enabled = false;

std::vector<Tile> player_tiles;
std::vector<Sprite> player_sprites;


namespace cosmos_engine
{


BlockingType Player::checkMovement(int direction, const float x_pos, const float y_pos)
{
    is_standing_slipry_slope_left_flg = 0;
    is_standing_slipry_slope_right_flg = 0;
    uint8 tile_attr;
    bool canGrabWall = false;

    switch (direction)
    {
        case 0: //UP
            if (player_y_pos - 3 == 0 || player_y_pos - 2 == 0)
            {
                return BLOCKED;
            }

            for (int i = 0; i < 3; i++)
            {
                if (map_get_tile_attr(x_pos + i, y_pos - 4) & TILE_ATTR_BLOCK_UP)
                {
                    return BLOCKED;
                }
            }
            break;

        case 1: //DOWN
            if (map_max_y_offset + 0x12 == player_y_pos)
            {
                return NOT_BLOCKED;
            }

            tile_attr = map_get_tile_attr(x_pos, y_pos);
            if ((tile_attr & TILE_ATTR_BLOCK_DOWN) == 0 && (tile_attr & TILE_ATTR_SLOPED) != 0 &&
                (tile_attr & TILE_ATTR_SLIPPERY) != 0)
            {
                is_standing_slipry_slope_left_flg = 1;
            }

            tile_attr = map_get_tile_attr(x_pos + 2, y_pos);
            if ((tile_attr & TILE_ATTR_BLOCK_DOWN) == 0 &&
                (tile_attr & TILE_ATTR_SLOPED) != 0 &&
                (tile_attr & TILE_ATTR_SLIPPERY) != 0)
            {
                is_standing_slipry_slope_right_flg = 1;
            }

            for (int i = 0; i < 3; i++)
            {
                tile_attr = map_get_tile_attr(x_pos + i, y_pos);
                if (tile_attr & TILE_ATTR_SLOPED)
                {
                    num_hits_since_touching_ground = 0;
                    return SLOPE;
                }
                if (tile_attr & TILE_ATTR_BLOCK_DOWN)
                {
                    num_hits_since_touching_ground = 0;
                    return BLOCKED;
                }
            }
            break;

        case 2: // LEFT
            tile_attr = map_get_tile_attr(x_pos, y_pos - 2);

            canGrabWall = (tile_attr & TILE_ATTR_CAN_GRAB_WALL);

            for (int i = 0; i < 5; i++)
            {
                tile_attr = map_get_tile_attr(x_pos, y_pos - i);
                if (tile_attr & TILE_ATTR_BLOCK_LEFT)
                {
                    mMayGrabWall = canGrabWall;
                    return BLOCKED;
                }

                if (i == 0)
                {
                    if (map_get_tile_attr(x_pos, y_pos) & TILE_ATTR_SLOPED &&
                            (map_get_tile_attr(x_pos, y_pos - 1) & TILE_ATTR_BLOCK_LEFT) == 0)
                    {
                        return SLOPE;
                    }
                }
            }
            break;

        case 3: // RIGHT
            tile_attr = map_get_tile_attr(x_pos + 2, y_pos - 2);
            canGrabWall = (tile_attr & TILE_ATTR_CAN_GRAB_WALL);

            for (int i = 0; i < 5; i++)
            {
                tile_attr = map_get_tile_attr(x_pos + 2, y_pos - i);
                if (tile_attr & TILE_ATTR_BLOCK_RIGHT)
                {
                    mMayGrabWall = canGrabWall;
                    return BLOCKED;
                }

                if (i == 0)
                {
                    if (map_get_tile_attr(x_pos + 2, y_pos) & TILE_ATTR_SLOPED &&
                        (map_get_tile_attr(x_pos + 2, y_pos - 1) & TILE_ATTR_BLOCK_RIGHT) == 0)
                    {
                        return SLOPE;
                    }
                }
            }
            break;

        default : break;
    }

    return NOT_BLOCKED;
}


void Player::handlePush()
{
    int di = 0;
    if(player_is_being_pushed_flag == di)
    {
        return;
    }
    if(cosmoInput.jump_key_pressed != 0 && player_dont_push_while_jumping_flag != 0)
    {
        player_is_being_pushed_flag = di;
        return;
    }

    for(int i=0; i < player_push_duration; i++)
    {
        if(player_x_offset_tbl[player_push_direction] + player_x_pos > 0 &&
                player_x_offset_tbl[player_push_direction] + player_x_pos + 2 < map_width_in_tiles)
        {
            player_x_pos += player_x_offset_tbl[player_push_direction];
        }

        player_y_pos += player_y_offset_tbl[player_push_direction];

        if(player_x_offset_tbl[player_push_direction] + mapwindow_x_offset > 0 &&
                player_x_offset_tbl[player_push_direction] + mapwindow_x_offset < map_width_in_tiles - 37)
        {
            mapwindow_x_offset = mapwindow_x_offset + player_x_offset_tbl[player_push_direction];
        }
        /*if(player_y_offset_tbl[player_push_direction] + mapwindow_y_offset > 2)
        {
            mapwindow_y_offset = mapwindow_y_offset + player_y_offset_tbl[player_push_direction];
        }*/
        if(player_push_check_blocking_flag != 0 &&
           (checkMovement(2, player_x_pos, player_y_pos) != NOT_BLOCKED ||
            checkMovement(3, player_x_pos, player_y_pos) != NOT_BLOCKED ||
            checkMovement(0, player_x_pos, player_y_pos) != NOT_BLOCKED ||
            checkMovement(1, player_x_pos, player_y_pos) != NOT_BLOCKED))
        {
            di = 1;
            break;
        }
    }

    if(di != 0)
    {
        player_x_pos = player_x_pos - player_x_offset_tbl[player_push_direction];
        player_y_pos = player_y_pos - player_y_offset_tbl[player_push_direction];
        //mapwindow_x_offset = mapwindow_x_offset - player_x_offset_tbl[player_push_direction];
        //mapwindow_y_offset = mapwindow_y_offset - player_y_offset_tbl[player_push_direction];
        resetPushVariables();
        return;
    }
    player_push_anim_counter++;
    if(player_push_anim_counter >= player_push_anim_duration_maybe)
    {
        resetPushVariables();
    }
}

void Player::setMapPtr(std::shared_ptr<CMap> &mapPtr)
{
    mpMap = mapPtr;
}

void Player::handleInput()
{
    static int local_bomb_key_counter = 0;
    BlockingType player_movement_status = NOT_BLOCKED;

    auto &cheat = gBehaviorEngine.mCheatmode;

    if(cheat.items)
    {
       showItemsCheatDlg();
       num_health_bars = 5;
       health = 6;
       num_bombs = 9;
       cheat.items = false;
    }

    const bool jumpCheat = cheat.jump;

    if (cheat_hack_mover_enabled) {
        hackMoverUpdate();
        return;
    }

    mMayGrabWall = false;
    if(death_counter != 0 || teleporter_state_maybe != 0 ||
       player_hoverboard_counter != 0 || walk_anim_index != 0 || hide_player_sprite != 0)
    {
        return;
    }

    walk_counter++;
    handlePush();

    if(player_is_being_pushed_flag != 0)
    {
        mHangingOnWallDirection = 0;
        return;
    }

    //int si = 0;

    if(mHangingOnWallDirection != 0)
    {
        uint8 tile_attr = 0;
        if(mHangingOnWallDirection != 2)
        {
            tile_attr = map_get_tile_attr(player_x_pos + 3, player_y_pos - 2);
        }
        else
        {
            tile_attr = map_get_tile_attr(player_x_pos - 1, player_y_pos - 2);
        }
        if((tile_attr & TILE_ATTR_SLIPPERY) != 0 &&
           (tile_attr & TILE_ATTR_CAN_GRAB_WALL) != 0)
        {
            if(checkMovement(1, player_x_pos, player_y_pos + 1) == NOT_BLOCKED)
            {
                player_y_pos = player_y_pos + 1;
                //si = 1;
                if(mHangingOnWallDirection != 2)
                {
                    tile_attr = map_get_tile_attr(player_x_pos + 3, player_y_pos - 2);
                }
                else
                {
                    tile_attr = map_get_tile_attr(player_x_pos - 1, player_y_pos - 2);
                }

                if((tile_attr & TILE_ATTR_SLIPPERY) == 0)
                {
                    if((tile_attr & TILE_ATTR_CAN_GRAB_WALL) == 0)
                    {
                        mHangingOnWallDirection = 0;
                        //si = 0;
                        //goto loc_1DD63;
                    }
                }
            }
            else
            {
                mHangingOnWallDirection = 0;
            }
        }
        else
        {
            if((tile_attr & TILE_ATTR_CAN_GRAB_WALL) == 0)
            {
                mHangingOnWallDirection = 0;
            }
        }
    }
    else
    {
        if(cosmoInput.bomb_key_pressed == 0)
        {
            local_bomb_key_counter = 0;
        }
        if(cosmoInput.bomb_key_pressed != 0 && local_bomb_key_counter == 0)
        {
            local_bomb_key_counter = 2;
        }
        if(local_bomb_key_counter != 0 && local_bomb_key_counter != 1)
        {
            local_bomb_key_counter = local_bomb_key_counter - 1;
            if(local_bomb_key_counter == 1)
            {
                if(player_direction == 0)
                {
                    uint8 tile_attr = map_get_tile_attr(player_x_pos - 1, player_y_pos - 2);
                    bool top_bomb_check_flag = tile_attr & TILE_ATTR_BLOCK_LEFT;
                    tile_attr = map_get_tile_attr(player_x_pos - 2, player_y_pos - 2);
                    bool bottom_bomb_check_flag = tile_attr & TILE_ATTR_BLOCK_LEFT;

                    if(num_bombs != 0 || has_had_bomb_flag != 0)
                    {
                        if(top_bomb_check_flag)
                        {
                            play_sfx(0x1c);
                        }
                        else
                        {
                            if(bottom_bomb_check_flag)
                            {
                                play_sfx(0x1c);
                            }
                            else
                            {
                                if(num_bombs == 0)
                                {
                                    play_sfx(0x1c);
                                }
                                else
                                {
                                    actor_add_new(0x18, player_x_pos - 2, player_y_pos - 2);
                                    num_bombs = num_bombs - 1;
                                    gStatus.displayNumBombsLeft();
                                    play_sfx(0x1d);
                                }
                            }
                        }
                    }
                    else
                    {
                        has_had_bomb_flag = 1;
                        you_havent_found_any_bombs_dialog();
                    }
                }
                else
                {
                    uint8 tile_attr = map_get_tile_attr(player_x_pos + 3, player_y_pos - 2);
                    bool top_bomb_check_flag = tile_attr & TILE_ATTR_BLOCK_RIGHT;
                    tile_attr = map_get_tile_attr(player_x_pos + 4, player_y_pos - 2);
                    bool bottom_bomb_check_flag = tile_attr & TILE_ATTR_BLOCK_RIGHT;

                    if(num_bombs == 0 && has_had_bomb_flag == 0)
                    {
                        has_had_bomb_flag = 1;
                        you_havent_found_any_bombs_dialog();
                    }
                    if(top_bomb_check_flag)
                    {
                        play_sfx(0x1c);
                    }
                    else
                    {
                        if(!bottom_bomb_check_flag)
                        {
                            if(num_bombs > 0)
                            {
                                actor_add_new(0x18, player_x_pos + 3, player_y_pos - 2);
                                num_bombs = num_bombs - 1;
                                gStatus.displayNumBombsLeft();
                                play_sfx(0x1d);
                            }
                            else
                            {
                                play_sfx(0x1c);
                            }
                        }
                        else
                        {
                            play_sfx(0x1c);
                        }
                    }
                }
            }
        }
        else
        {
            cosmoInput.bomb_key_pressed = 0;
        }
    }

    if(jumpStateIdx != 0 || cosmoInput.bomb_key_pressed == 0 || riding_hoverboard != 0 ||
       mHangingOnWallDirection != 0 ||
       (cosmoInput.jump_key_pressed != 0 && player_input_jump_related_flag == 0))
    {
        player_lookstate = 0;
        //ax = checkMovement(1, player_x_pos, player_y_pos + 1);
        if(is_standing_slipry_slope_left_flg == 0 || is_standing_slipry_slope_right_flg == 0)
        {
            if(is_standing_slipry_slope_right_flg != 0)
            {
                if(mHangingOnWallDirection == 0)
                {
                    player_x_pos = player_x_pos - 1;
                }
                if(checkMovement(1, player_x_pos, player_y_pos + 1) == 0 && mHangingOnWallDirection == 0)
                {
                    player_y_pos = player_y_pos + 1;
                }
                /*if(player_y_pos - mapwindow_y_offset > 14)
                {
                    mapwindow_y_offset = mapwindow_y_offset + 1;
                }*/
                if(player_x_pos - mapwindow_x_offset < 12 && mapwindow_x_offset > 0)
                {
                    mapwindow_x_offset = mapwindow_x_offset - 1;
                }
                mHangingOnWallDirection = 0;
            }
            if(is_standing_slipry_slope_left_flg != 0)
            {
                if(mHangingOnWallDirection == 0)
                {
                    player_x_pos++;
                }
                if(checkMovement(1, player_x_pos, player_y_pos + 1) == 0 && mHangingOnWallDirection == 0)
                {
                    player_y_pos++;
                }
                /*if(player_y_pos - mapwindow_y_offset > 14)
                {
                    mapwindow_y_offset = mapwindow_y_offset + 1;
                }*/
                if(player_x_pos - mapwindow_x_offset > 0x17)
                {
                    if(map_width_in_tiles - 38 > mapwindow_x_offset)
                    {
                        mapwindow_x_offset = mapwindow_x_offset + 1;
                    }
                }
                mHangingOnWallDirection = 0;
            }
        }
        if(cosmoInput.left_key_pressed != 0 && mHangingOnWallDirection == 0 && cosmoInput.right_key_pressed == 0)
        {
            BlockingType di = checkMovement(1, player_x_pos, player_y_pos + 1);

            if(mWatchingDirection != 2)
            {
                mWatchingDirection = 2;
            }
            else
            {
                player_x_pos = player_x_pos - 1.0f;
            }
            player_direction = 0;
            if(player_x_pos >= 1)
            {
                player_movement_status = checkMovement(2, player_x_pos, player_y_pos);
                if(player_movement_status == BLOCKED)
                {
                    player_x_pos = player_x_pos + 1;
                    if(checkMovement(1, player_x_pos, player_y_pos + 1) == NOT_BLOCKED &&
                            mMayGrabWall)
                    {
                        mHangingOnWallDirection = 2;
                        player_bounce_flag_maybe = 0;
                        player_bounce_height_counter = 0;
                        play_sfx(4);
                        riding_hoverboard = 0;
                        jumpStateIdx = 0;
                        player_falling_state = 0;
                        if(cosmoInput.jump_key_pressed == 0)
                        {
                            player_input_jump_related_flag = 0;
                        }
                        else
                        {
                            player_input_jump_related_flag = 1;
                        }
                    }
                }
            }
            else
            {
                player_x_pos = player_x_pos + 1;
            }
            if(player_movement_status != 2)
            {
                if(di == SLOPE)
                {
                    if(checkMovement(1, player_x_pos, player_y_pos + 1) == NOT_BLOCKED)
                    {
                        riding_hoverboard = 0;
                        jumpStateIdx = 0;
                        player_y_pos++;
                    }
                }
            }
            else
            {
                player_y_pos--;
            }
        }
        if(cosmoInput.right_key_pressed != 0 &&
           mHangingOnWallDirection == 0 &&
           cosmoInput.left_key_pressed == 0)
        {
            BlockingType di = checkMovement(1, player_x_pos, player_y_pos + 1);
            if(mWatchingDirection != 3)
            {
                mWatchingDirection = 3;
            }
            else
            {
                player_x_pos = player_x_pos + 1;
            }
            player_direction = 0x17;
            if(map_width_in_tiles - 4 >= player_x_pos)
            {
                player_movement_status = checkMovement(3, player_x_pos, player_y_pos);
                if(player_movement_status == BLOCKED)
                {
                    player_x_pos = player_x_pos - 1;
                    if(checkMovement(1, player_x_pos, player_y_pos + 1) == NOT_BLOCKED &&
                            mMayGrabWall)
                    {
                        mHangingOnWallDirection = 3;
                        player_bounce_flag_maybe = 0;
                        player_bounce_height_counter = 0;
                        play_sfx(4);
                        jumpStateIdx = 0;
                        riding_hoverboard = 0;
                        player_falling_state = 0;
                        if(cosmoInput.jump_key_pressed == 0)
                        {
                            player_input_jump_related_flag = 0;
                        }
                        else
                        {
                            player_input_jump_related_flag = 1;
                        }
                    }
                }
            }
            else
            {
                player_x_pos = player_x_pos - 1;
            }
            if(player_movement_status != 2)
            {
                if(di == SLOPE)
                {
                    if(checkMovement(1, player_x_pos, player_y_pos + 1) == NOT_BLOCKED)
                    {
                        riding_hoverboard = 0;
                        player_falling_state = 0;
                        player_y_pos++;
                    }
                }
            }
            else
            {
                player_y_pos--;
            }
        }
        if(mHangingOnWallDirection != 0 && player_input_jump_related_flag != 0 && cosmoInput.jump_key_pressed == 0)
        {
            player_input_jump_related_flag = 0;
        }
        if(player_bounce_height_counter != 0 ||
                (cosmoInput.jump_key_pressed != 0 && riding_hoverboard == 0 && player_input_jump_related_flag == 0) ||
                (mHangingOnWallDirection != 0 && cosmoInput.jump_key_pressed != 0 && player_input_jump_related_flag == 0))
        {
            bool var_4 = false;
            if(player_bounce_flag_maybe != 0 && player_bounce_height_counter > 0)
            {
                player_bounce_height_counter = player_bounce_height_counter - 1;
                if(player_bounce_height_counter < 10)
                {
                    player_spring_jump_flag = 0;
                }
                if(player_bounce_height_counter > 1)
                {
                    player_y_pos = player_y_pos - 1;
                }
                if(player_bounce_height_counter > 13)
                {
                    player_bounce_height_counter = player_bounce_height_counter - 1;
                    if(checkMovement(0, player_x_pos, player_y_pos) != 0)
                    {
                        player_spring_jump_flag = 0;
                    }
                    else
                    {
                        player_y_pos = player_y_pos - 1;
                    }
                }
                if(player_bounce_height_counter == 0)
                {
                    jumpStateIdx = 0;
                    player_bounce_flag_maybe = 0;
                    player_falling_state = 0;
                    player_spring_jump_flag = 0;
                    player_input_jump_related_flag = 1;
                }
            }
            else
            {
                if(mHangingOnWallDirection == 2)
                {
                    if(cosmoInput.left_key_pressed == 0)
                    {
                        if(cosmoInput.right_key_pressed != 0)
                        {
                            player_direction = 0x17;
                        }
                    }
                    else
                    {
                        mHangingOnWallDirection = 0;
                    }
                }
                if(mHangingOnWallDirection == 3)
                {
                    if(cosmoInput.right_key_pressed == 0)
                    {
                        if(cosmoInput.left_key_pressed != 0)
                        {
                            player_direction = 0;
                        }
                    }
                    else
                    {
                        mHangingOnWallDirection = 0;
                    }
                }
                if(mHangingOnWallDirection == 0)
                {
                    static const int jumpAcceleration[10] = {-2, -1, -1, -1, -1, -1, -1, 0, 0, 0};
                    player_y_pos += jumpAcceleration[jumpStateIdx];
                }
                if(jumpStateIdx == 0 && checkMovement(0, player_x_pos, player_y_pos + 1) != 0)
                {
                    player_y_pos++;
                }
                player_bounce_flag_maybe = 0;
                var_4 = true;
            }
            mHangingOnWallDirection = 0;
            BlockingType blockingCheck = checkMovement(0, player_x_pos, player_y_pos);
            if(blockingCheck == NOT_BLOCKED)
            {
                if(var_4 && jumpStateIdx == 0)
                {
                    play_sfx(2);
                }
            }
            else
            {
                if(jumpStateIdx > 0 || player_bounce_flag_maybe != 0)
                {
                    play_sfx(5);
                }
                player_bounce_height_counter = 0;
                player_bounce_flag_maybe = 0;
                blockingCheck = checkMovement(0, player_x_pos, player_y_pos + 1);
                if(blockingCheck != NOT_BLOCKED)
                {
                    player_y_pos++;
                }
                player_y_pos++;
                riding_hoverboard = 1;
                if(cosmoInput.jump_key_pressed != 0)
                {
                    player_input_jump_related_flag = 1;
                }
                player_falling_state = 0;
                player_spring_jump_flag = 0;
            }
            if(player_bounce_flag_maybe == 0)
            {
                if(jumpStateIdx > 6)
                {
                    riding_hoverboard = 1;
                    if(cosmoInput.jump_key_pressed != 0)
                    {
                        player_input_jump_related_flag = 1;
                    }
                    player_falling_state = 0;
                }

                jumpStateIdx = jumpCheat ? 1 : jumpStateIdx + 1;

            }
        }
        if(mHangingOnWallDirection == 0)
        {
            if(riding_hoverboard != 0 && cosmoInput.jump_key_pressed != 0)
            {
                player_input_jump_related_flag = 1;
            }
            if((cosmoInput.jump_key_pressed == 0 || player_input_jump_related_flag != 0) && riding_hoverboard == 0)
            {
                riding_hoverboard = 1;
                player_falling_state = 0;
            }
            if(riding_hoverboard != 0 && player_bounce_flag_maybe == 0)
            {
                player_y_pos += 1.0f;
                if(checkMovement(1, player_x_pos, player_y_pos) != NOT_BLOCKED)
                {
                    if(player_falling_state != 0)
                    {
                        play_sfx(3);
                    }
                    riding_hoverboard = 0;
                    player_y_pos -= 1.0f;
                    jumpStateIdx = 0;
                    if(cosmoInput.jump_key_pressed == 0)
                    {
                        player_input_jump_related_flag = 0;
                    }
                    else
                    {
                        player_input_jump_related_flag = 1;
                    }
                    player_falling_state = 0;
                }
                if(player_falling_state > 3) // Now he starts falling
                {
                    player_y_pos += 1.0f;

                    if(checkMovement(1, player_x_pos, player_y_pos) != NOT_BLOCKED) // Landing
                    {
                        play_sfx(3);
                        riding_hoverboard = 0;
                        player_y_pos -= 1.0f;
                        //mapwindow_y_offset = mapwindow_y_offset - 1;
                        jumpStateIdx = 0;
                        if(cosmoInput.jump_key_pressed == 0)
                        {
                            player_input_jump_related_flag = 0;
                        }
                        else
                        {
                            player_input_jump_related_flag = 1;
                        }
                        player_falling_state = 0;
                    }
                }
                if(player_falling_state < 0x19)
                {
                    player_falling_state = player_falling_state + 1;
                }
            }
            if(riding_hoverboard != 0 &&
               player_falling_state == 1 &&
               player_bounce_flag_maybe == 0)
            {                
                player_y_pos = player_y_pos - 1;
            }
        }
    }
    else
    {
        if(cosmoInput.left_key_pressed == 0)
        {
            if(cosmoInput.right_key_pressed == 0)
            {
                if(mWatchingDirection != 2)
                {
                    if(mWatchingDirection == 3)
                    {
                        player_lookstate = 3;
                    }
                }
                else
                {
                    player_lookstate = 2;
                }
            }
            else
            {
                mWatchingDirection = 3;
                player_lookstate = 3;
                player_direction = 0x17;
            }
        }
        else
        {
            mWatchingDirection = 2;
            player_lookstate = 2;
            player_direction = 0;
        }
    }

    if (player_lookstate == 0)
    {
        // Change to either look up or down?
        if ((cosmoInput.up_key_pressed != 0 || cosmoInput.down_key_pressed != 0) &&
             cosmoInput.left_key_pressed == 0 && cosmoInput.right_key_pressed == 0 &&
            riding_hoverboard == 0 && cosmoInput.jump_key_pressed == 0)
        {
            player_idle_counter = 0;

            // Looking up?
            if (cosmoInput.up_key_pressed != 0 && player_is_teleporting_flag == 0 && hit_hint_globe == 0)
            {                
                pov_y--;
                pov_y = std::max(-max_pov_y, pov_y);

                // Set lookup sprite frame accordingly
                player_sprite_dir_frame_offset = (mHangingOnWallDirection == 0) ? 5 : 11;
                return;
            }

            // Looking Down?
            if (cosmoInput.down_key_pressed != 0)
            {
                pov_y++;
                pov_y = std::min(max_pov_y, pov_y);

                player_sprite_dir_frame_offset = (mHangingOnWallDirection == 0) ? 6 : 12;
            }
            return;
        }
        else
        {
            if(pov_y < 0)
                pov_y++;
            else if(pov_y > 0)
                pov_y--;
        }

        // node 0001e6bd-0001e6c2 #insn=2 use={} def={} in={} out={} pred={ 1E5DC} FALLTHROUGH follow=0001e8d8
        //loc_1E6BD:
        if(mHangingOnWallDirection != 2)
        {
            if(mHangingOnWallDirection != 3)
            {
                if((riding_hoverboard == 0 || player_bounce_flag_maybe != 0) && (jumpStateIdx <= 6 || riding_hoverboard != 0))
                {
                    if((cosmoInput.jump_key_pressed == 0 || player_input_jump_related_flag != 0) && player_bounce_flag_maybe == 0)
                    {
                        if(cosmoInput.left_key_pressed == cosmoInput.right_key_pressed)
                        {
                            uint8 rvalue = (uint8)(cosmo_rand() % 0x32);

                            player_sprite_dir_frame_offset = 4;
                            if(cosmoInput.left_key_pressed == 0 && cosmoInput.right_key_pressed == 0 && riding_hoverboard == 0)
                            {
                                updateIdleAnim();
                            }
                            if(player_sprite_dir_frame_offset != 5 && player_sprite_dir_frame_offset != 6 && (rvalue == 0 || rvalue == 0x1f))
                            {
                                //loc_1E89B:
                                player_sprite_dir_frame_offset = 0x12;
                                //goto loc_1E8D8;
                            }
                        }
                        else
                        {
                            if(riding_hoverboard == 0)
                            {
                                player_idle_counter = 0;
                                if((walk_counter & 1u) != 0)
                                {
                                    if((player_sprite_dir_frame_offset & 1u) != 0)
                                    {
                                        play_sfx(0x13);
                                    }
                                    player_sprite_dir_frame_offset = player_sprite_dir_frame_offset + 1;
                                }
                                if(player_sprite_dir_frame_offset > 3)
                                {
                                    player_sprite_dir_frame_offset = 0;
                                }
                            }
                        }
                    }
                    else
                    {
                        player_idle_counter = 0;
                        player_sprite_dir_frame_offset = 7;
                        if(player_bounce_flag_maybe != 0 && player_spring_jump_flag != 0)
                        {
                            player_sprite_dir_frame_offset = 0x16;
                        }
                        if(player_bounce_height_counter < 3 && player_bounce_flag_maybe != 0)
                        {
                            player_sprite_dir_frame_offset = 8;
                        }
                    }
                }
                else
                {
                    player_idle_counter = 0;
                    if(player_bounce_flag_maybe != 0 || riding_hoverboard != 0 || jumpStateIdx <= 6)
                    {
                        if(player_falling_state < 10 || player_falling_state >= 0x19)
                        {
                            if(player_falling_state != 0x19)
                            {
                                if(riding_hoverboard != 0)
                                {
                                    player_sprite_dir_frame_offset = 8;
                                }
                                else
                                {
                                    player_sprite_dir_frame_offset = 7;
                                }
                            }
                            else
                            {
                                player_sprite_dir_frame_offset = 0x10;
                                hands_on_eyes = 1;
                            }
                        }
                        else
                        {
                            player_sprite_dir_frame_offset = 13;
                        }
                    }
                    else
                    {
                        player_sprite_dir_frame_offset = 8;
                    }
                }
            }
            else
            {
                player_idle_counter = 0;
                if(cosmoInput.left_key_pressed == 0)
                {
                    player_sprite_dir_frame_offset = 9;
                }
                else
                {
                    player_sprite_dir_frame_offset = 10;
                }
            }
        }
        else
        {
            player_idle_counter = 0;
            if(cosmoInput.right_key_pressed != 0)
            {
                player_sprite_dir_frame_offset = 10;
            }
            else
            {
                player_sprite_dir_frame_offset = 9;
            }
        }
    }
    else
    {
        player_idle_counter = 0;
        player_sprite_dir_frame_offset = 14;
    }


}


void Player::syncDrawPosition()
{
    posToDraw.y = player_y_pos;
    posToDraw.x = player_x_pos;
    oldPosDraw = posToDraw;
}

bool pov_moving = false;

void Player::handleScrolling()
{
    const auto gameRes = gVideoDriver.getGameResolution();

    const auto scroll = mpMap->getScrollCoords(1);

    using namespace std;

    // Y Part
    const auto player_pov_y = player_y_pos+pov_y;
    const auto diffY = player_pov_y - scroll.y/8;

    const bool modernStyle = gBehaviorEngine.mOptions[GameOption::MODERN].value;
    const int camMaxDistY = modernStyle ? 14+6 : 14;
    const int camMinDistY = 8;

    // If style is modern, no padding in scroll areas, otherwise set a bottom one
    const auto numScrollPlanes = mpMap->getNumScrollingPlanes();
    for(unsigned int i=0 ; i<numScrollPlanes ; i++)
    {
        auto &scroll = mpMap->getScrollingPlane(i);
        scroll.setPaddingBottom(modernStyle ? 0 : 100);
    }

    mLocalMapScroll.y = static_cast<float>(scroll.y);

    if(diffY >= camMaxDistY)
    {        
        // scroll down
        if(scroll.y+gameRes.dim.y <= (map_height_in_tiles*8)-(2*camMinDistY))
        {
            mLocalMapScroll.y = (player_pov_y - camMaxDistY)*8.0f;
        }

        const auto y_diff = (player_pov_y - camMaxDistY) - mapwindow_y_offset;
        if(y_diff != 0)
        {
            mapwindow_y_offset = mapwindow_y_offset + ((y_diff > 0) ? 0.5f : -0.5f);
            mapwindow_y_offset = max(mapwindow_y_offset, 0.0f);
        }

        oldPosDraw.y = player_y_pos;
    }
    else if(diffY <= camMinDistY)
    {
        // scroll up
        if(scroll.y >= camMinDistY*4)
        {
            mLocalMapScroll.y = (player_pov_y - camMinDistY)*8.0f;
        }

        const auto y_diff =  (player_pov_y - camMinDistY) - mapwindow_y_offset;
        if(y_diff != 0)
        {            
            mapwindow_y_offset = mapwindow_y_offset + ((y_diff > 0) ? 0.5f : -0.5f);
            mapwindow_y_offset = max(mapwindow_y_offset, 0.0f);
        }

        oldPosDraw.y = player_y_pos;
    }

    if(mLocalMapScroll.y == scroll.y)
        pov_moving = false;

    if(pov_y && !pov_moving)
        pov_moving = true;

    // While player looking up or down, don't extra smooth the scrolling
    if(pov_moving)
        mLocalMapScroll.y = float(scroll.y);

    // X Part
    const auto diffX = player_x_pos - scroll.x/8;
    const float camMaxDistX = 23.0f;
    const float camMinDistX = 12.0f;

    mLocalMapScroll.x = float(scroll.x);

    if(diffX >= camMaxDistX &&
       map_stride_bit_shift_amt > 5)
    {
        // scroll right
        if(map_width_in_tiles - 38 > mapwindow_x_offset)
        {                        
            mLocalMapScroll.x = (player_x_pos - camMaxDistX)*8.0f;
            const auto x_diff = (player_x_pos - camMaxDistX) - mapwindow_x_offset;
            if(x_diff != 0)
            {
                mapwindow_x_offset = mapwindow_x_offset + ((x_diff > 0) ? 0.25f : -0.25f);
            }

            oldPosDraw.x = player_x_pos;
        }

    }
    else if(diffX <= camMinDistX && mapwindow_x_offset > 0)
    {
        // scroll left
        mLocalMapScroll.x = (player_x_pos - camMinDistX)*8.0f;
        const auto x_diff = (player_x_pos - camMinDistX) - mapwindow_x_offset;
        if(x_diff != 0)
        {
            mapwindow_x_offset = mapwindow_x_offset + ((x_diff > 0) ? 0.25f : -0.25f);
        }

        oldPosDraw.x = player_x_pos;
    }
}


void Player::resetState()
{
    mWatchingDirection = 3;
    hit_hint_globe = 0;
    player_input_jump_related_flag = 1;
    mHangingOnWallDirection = 0;
    player_bounce_flag_maybe = 0;
    player_bounce_height_counter = 0;
    player_sprite_dir_frame_offset = 0;
    player_direction = 0x17;
    death_counter = 0;
    player_invincibility_counter = (0x28)*4;
    player_hoverboard_counter = 0;
    player_is_teleporting_flag = 0;
    player_in_pneumatic_tube_flag = 0;
}

void Player::resetWalkCycle()
{
    hands_on_eyes = 0;
    walk_anim_index = 0;
}


void Player::resetPushVariables()
{
    player_is_being_pushed_flag = 0;
    player_push_direction = 0;
    player_push_anim_duration_maybe = 0;
    player_push_anim_counter = 0;
    player_push_duration = 0;
    player_push_frame_num = 0;
    player_dont_push_while_jumping_flag = 0;
    player_bounce_flag_maybe = 0;
    player_bounce_height_counter = 0;
    riding_hoverboard = 1;
    player_falling_state = 0;
}


GsVec2D<float> Player::getLocalMapScroll() const
{
    return mLocalMapScroll;
}

void Player::loadSprites()
{
    gGraphics.Palette.setupColorPalettes(nullptr, 0);

    mSprites.assign(player_sprites[0].num_frames, GsSprite());

    gLogging << "Loading " << player_sprites[0].num_frames << " sprites." << CLogFile::endl;

    for(unsigned int i=0 ; i<mSprites.size() ; i++)
    {
        TileInfo *info = &player_sprites[0].frames[i];
        auto &sprite = mSprites[i];

        const auto pal = gGraphics.Palette.m_Palette;
        const auto flags = gVideoDriver.mpVideoEngine->getBlitSurface()->flags;

        Tile *tile = &player_tiles[info->tile_num];

        sprite.setSize(info->width*8, info->height*8);
        sprite.createSurface( flags, pal );

        auto &gsSfc = sprite.Surface();

        // Fill the whole surface with transparency, before we blit
        // the sprite tiles on it.
        Uint8 r, g, b;
        gsSfc.readColorKey(r, g, b);
        gsSfc.fillRGB(r, g, b);

        gsSfc.lock();

        for(int y = 0 ; y < sprite.getHeight()/TILE_HEIGHT ; y++)
        {
            for(int x = 0 ; x < sprite.getWidth()/TILE_WIDTH ; x++)
            {                
                GsWeakSurface tileWeakSfc(tile->sfc);
                GsRect<Uint16> dstRect(x*8, y*8, TILE_WIDTH, TILE_HEIGHT);
                GsRect<Uint16> srcRect(0, 0, TILE_WIDTH, TILE_HEIGHT);

                tileWeakSfc.blitTo(gsSfc, srcRect.SDLRect(), dstRect.SDLRect());
                tile++;
            }
        }

        gsSfc.unlock();

        /*
        {
            std::string name = "SPR";
            name = name + itoa(i) + ".bmp";

            const auto fullpath = GetWriteFullFileName( JoinPaths("CosmoSpr", std::string(name)), true );

            if(SDL_SaveBMP(gsSfc.getSDLSurface(), fullpath.c_str()) != 0)
            {
                // Error saving bitmap
                gLogging.ftextOut("SDL_SaveBMP failed: %s\n", SDL_GetError());
            }
        }
        */
    }

}


void Player::loadTiles()
{
	uint16 num_tile_info_records;
    uint16 num_tiles;
    player_tiles = load_tiles("PLAYERS.MNI", TileType::TRANSP, &num_tiles);
    gLogging << "Loading " << num_tiles << "player tiles." << CLogFile::endl;

    player_sprites = load_tile_info("PLYRINFO.MNI", &num_tile_info_records);
    gLogging << "Loading " << num_tile_info_records << "player tile info records." << CLogFile::endl;

    loadSprites();
}

void Player::displaySprite(const int frame_num,
                           const float x_pos,
                           const float y_pos,
                           const DrawMode tile_display_func_index)
{
    if(tile_display_func_index == DrawMode::ON_DIALOG)
    {
        TileInfo *info = &player_sprites[0].frames[frame_num];
        Tile *tile = &player_tiles[info->tile_num];

        for(int y=0;y < info->height;y++)
        {
            const uint16 screen_y = (y_pos + (y - float(info->height - 1))) * 8;
            for (int x = 0; x < info->width; x++)
            {
                const uint16 screen_x = (x_pos + float(x)) * 8;
                video_draw_tile(tile, screen_x, screen_y);
                tile++;
            }
        }

        return;
    }

    if( player_push_frame_num == 0xff || teleporter_state_maybe ||
       (player_invincibility_counter & 1u) || hide_player_sprite)
    {
        return;
    }

    TileInfo *info = &player_sprites[0].frames[frame_num];
    posToDraw = {x_pos, y_pos};
    GsSprite &playSpr = mSprites[frame_num];

    // Makes player move smoothly at any time (when map scrolling/not scrolling)
    const float smoothMaxYDist = 6.0f;
    const float smoothMaxXDist = 32.0f;
    {
        if(std::abs(oldPosDraw.x - posToDraw.x) > smoothMaxXDist)
            oldPosDraw.x = posToDraw.x;
        if(std::abs(oldPosDraw.y - posToDraw.y) > smoothMaxYDist)
            oldPosDraw.y = posToDraw.y;

        if(oldPosDraw.x < posToDraw.x)
            oldPosDraw.x += 0.25f;
        if(oldPosDraw.x > posToDraw.x)
            oldPosDraw.x -= 0.25f;

        if(oldPosDraw.y < posToDraw.y)
            oldPosDraw.y += 0.5f;
        if(oldPosDraw.y > posToDraw.y)
            oldPosDraw.y -= 0.5f;
    }

    // These lines make the sprites within the screen smoother, but worse when transition goes over to scrolling
    const uint16 screen_x = (oldPosDraw.x) * 8.0f - (mLocalMapScroll.x);
    const uint16 screen_y = (oldPosDraw.y - float(info->height) + 1.0f) * 8.0f - (mLocalMapScroll.y);

    drawSpriteToOverlay(playSpr,
                        screen_x,
                        screen_y);
}


void Player::displaySprite(const int frame_num,
                   const DrawMode tile_display_func_index)
{
    displaySprite(frame_num, player_x_pos,
                  player_y_pos, tile_display_func_index);
}


int Player::updateSprite()
{
    static uint8 fallcycle_sprite = 0;

    auto &actorMan = gActorMan;

    if (map_max_y_offset + 0x15 < player_y_pos && death_counter == 0)
    {
        player_fall_off_map_bottom_counter = 1;
        death_counter = 1;

        if (map_max_y_offset + 0x16 == player_y_pos)
        {
            player_y_pos++;
        }

        fallcycle_sprite++;
        if (fallcycle_sprite == 5)
        {
            fallcycle_sprite = 0;
        }
    }
    if (player_fall_off_map_bottom_counter != 0)
    {
        player_fall_off_map_bottom_counter = player_fall_off_map_bottom_counter + 1;
        if (player_fall_off_map_bottom_counter == 2)
        {
            play_sfx(14);
        }

        while (player_fall_off_map_bottom_counter < 12)
        {
            cosmo_wait(2);
            player_fall_off_map_bottom_counter++;
        }

        if (player_fall_off_map_bottom_counter == 13)
        {
            play_sfx(7);
        }
        if (player_fall_off_map_bottom_counter > 12 && player_fall_off_map_bottom_counter < 0x13)
        {
            actorMan.display_sprite_maybe(0xde, fallcycle_sprite, player_x_pos - 1,
                                       player_y_pos - player_fall_off_map_bottom_counter + 13, DrawMode::SOLID);
        }
        if (player_fall_off_map_bottom_counter > 0x12)
        {
            actorMan.display_sprite_maybe(0xde, fallcycle_sprite, player_x_pos - 1, player_y_pos - 6, DrawMode::SOLID);
        }
        if (player_fall_off_map_bottom_counter > 0x1e)
        {
            load_savegame_file('T');
            load_level(current_level);
            player_fall_off_map_bottom_counter = 0;
            return 1;
        }
    }
    else
    {
        if (death_counter == 0)
        {
            if(player_invincibility_counter)
                syncDrawPosition();

            if (player_invincibility_counter != 0x2c*4)
            {
                if (player_invincibility_counter > 0x28*4)
                {
                    displaySprite(player_direction + 15, DrawMode::NORMAL);
                }
            }
            else
            {
                displaySprite(player_direction + 15, DrawMode::SOLID_WHITE);
            }
            if (player_invincibility_counter != 0)
            {
                player_invincibility_counter = player_invincibility_counter - 1;
            }
            if (player_invincibility_counter < 0x29*4)
            {
                if (player_is_being_pushed_flag != 0)
                {
                    displaySprite((uint8)player_push_frame_num, DrawMode::NORMAL);
                }
                else
                {
                    displaySprite(player_direction + player_sprite_dir_frame_offset,
                                          DrawMode::NORMAL);
                }
            }
        }
        else
        {
            if (death_counter >= 40)
            {
                if (death_counter > 36)
                {
                    if (mapwindow_y_offset > 0 && death_counter < 48)
                    {
                        mapwindow_y_offset = mapwindow_y_offset - 0.25f;
                    }
                    if (death_counter == 40)
                    {
                        play_sfx(7);
                    }
                    player_y_pos = player_y_pos - 0.25f;
                    death_counter = death_counter + 1;

                    const auto deathFrame = (death_counter%8 > 4) ? 1 : 0;

                    displaySprite(deathFrame + 0x2e,
                                  player_x_pos - 1, player_y_pos, DrawMode::SOLID);
                    if (death_counter > 0x24*4)
                    {
                        load_savegame_file('T');
                        load_level(current_level);
                        return 1;
                    }
                }
            }
            else
            {
                if (death_counter == 1)
                {
                    play_sfx(14);
                }
                death_counter = death_counter + 1;

                const auto deathFrame = (death_counter%8 > 4) ? 1 : 0;
                displaySprite(deathFrame + 0x2e,
                              player_x_pos - 1, player_y_pos, DrawMode::SOLID);
            }
        }
    }

    return 0;
}


void Player::updateWalkAnim()
{
    const uint8 player_walk_frame_tbl_maybe[] = {
            0x13, 0x14, 0x15,
            0x14, 0x13, 0x14,
            0x15, 0x14, 0x13
    };

    if(mHangingOnWallDirection != 0)
    {
        hands_on_eyes = 0;
        walk_anim_index = 0;
    }
    if(hands_on_eyes != 0 && checkMovement(1, player_x_pos, player_y_pos + 1) != 0)
    {
        hands_on_eyes = 0;
        walk_anim_index = 8;
        play_sfx(3);
    }
    if(walk_anim_index != 0)
    {
        player_sprite_dir_frame_offset = player_walk_frame_tbl_maybe[walk_anim_index];// * ((walk_anim_index << 1) + player_walk_frame_tbl_maybe);
        walk_anim_index = walk_anim_index - 1;
        riding_hoverboard = 0;
        if(walk_anim_index > 8)
        {
            gCosmoPlayer.resetWalkCycle();
        }
    }
}



int Player::checkCollisionWithActor(const int actorInfoIndex,
                                    const int frame_num,
                                    int x_pos,
                                    const int y_pos)
{
    if(death_counter == 0)
    {
        TileInfo *tileInfo = actor_get_tile_info(actorInfoIndex, frame_num);

        assert(tileInfo);

        uint16 di = tileInfo->height;
        uint16 si = tileInfo->width;

        if(x_pos > map_width_in_tiles && x_pos <= -1 && actorInfoIndex == 0x1a)
        {
            si += x_pos;
            x_pos = 0;
        }
        if(((player_x_pos <= x_pos && player_x_pos + 3 > x_pos) || (player_x_pos >= x_pos && x_pos + si > player_x_pos)) &&
                ((y_pos - di < player_y_pos && player_y_pos <= y_pos) || (player_y_pos - 4 <= y_pos && y_pos <= player_y_pos)))
        {
            return 1;
        }
    }
    return 0;
}



int Player::bounceInAir(int bounce_height)
{
    static int player_bounce_height_counter_in_air = 0;

    if(death_counter != 0 || walk_anim_index != 0)
    {
        return 0;
    }

    if((player_bounce_flag_maybe == 0 || player_bounce_height_counter < 2) &&
            ((riding_hoverboard != 0 && player_falling_state >= 0) || jumpStateIdx > 6) &&
            hitDetectionWithPlayer)
    {
        player_bounce_height_counter = bounce_height + 1;
        player_bounce_height_counter_in_air = player_bounce_height_counter;
        player_bounce_flag_maybe = 1;
        gCosmoPlayer.resetWalkCycle();
        if(bounce_height <= 0x12)
        {
            player_spring_jump_flag = 0;
        }
        else
        {
            player_spring_jump_flag = 1;
        }
        show_monster_attack_hint = 2;
        if(bounce_height != 7)
        {
            num_hits_since_touching_ground = 0;
        }
        else
        {
            num_hits_since_touching_ground++;
            if(num_hits_since_touching_ground == 10)
            {
                num_hits_since_touching_ground = 0;
                gCosmoPlayer.addSpeechBubble(POINTS_50000);
            }
        }
        return 1;
    }

    if(player_bounce_height_counter_in_air - 2 >= player_bounce_height_counter)
    {
        return 0;
    }
    else
    {
        if(!hitDetectionWithPlayer)
        {
            return 0;
        }
        else
        {
            if(player_bounce_flag_maybe == 0)
            {
                return 0;
            }
            else
            {
                gCosmoPlayer.resetWalkCycle();
                if(player_bounce_height_counter <= 0x12)
                {
                    player_spring_jump_flag = 0;
                }
                else
                {
                    player_spring_jump_flag = 1;
                }
                show_monster_attack_hint = 2;
                //return 1;
            }
        }
    }
    return 1;
}



void Player::addToScore(const int amount_to_add_low)
{
    gStatus.addToScoreUpdateOnDisplay(amount_to_add_low);
}

void Player::addScoreForActor(int actorInfoIndex)
{
    switch (actorInfoIndex)
    {
        case 46:
            addToScore(0x320);
            break;

        case 51:
        case 54:
        case 78:
        case 80:
            addToScore(0x190);
            break;

        case 20:
            addToScore(0xc80);
            break;

        case 41:
        case 47:
        case 86:
            addToScore(0x640);
            break;

        case 92:
        case 101:
            addToScore(0x1900);
            break;

        case 17:
        case 18:
        case 87:
        case 89:
            addToScore(0xfa);
            break;

        case 106:
        case 113:
            addToScore(0x3e8);
            break;

        case 69:
        case 125:
            addToScore(0x3200);
            break;

        case 126:
            addToScore(0x7d0);
            break;

        case 3:
        case 4:
        case 88:
        case 111:
        case 112:
        case 118:
        case 127:
            addToScore(0x1f4);
            break;

        case 129:
            addToScore(0xc350);
            break;

        case 74:
        case 75:
        case 95:
        case 96:
        case 128:
        case 187:
            addToScore(0x64);
            break;

        case 1:
        case 25:
        case 55:
        case 65:
        case 83:
        case 84:
        case 124:
        case 188:
            addToScore(0xc8);
            break;

        default: break;
    }
}


void Player::decreaseHealth()
{
    if(gBehaviorEngine.mCheatmode.god)
    {
        return;
    }

    if(death_counter == 0 &&
       hide_player_sprite == 0 &&
       teleporter_state_maybe == 0 &&
       protection_barrier_active == 0 &&
       player_in_pneumatic_tube_flag == 0 &&
       player_invincibility_counter == 0)
    {
        mHangingOnWallDirection = 0;
        if(mHasShownOuchBubble == 0)
        {
            mHasShownOuchBubble = 1;
            gCosmoPlayer.addSpeechBubble(OUCH);
            if(show_monster_attack_hint == 0)
            {
                show_monster_attack_hint = 1;
            }
        }
        health--;
        if(health != 0)
        {
            gStatus.updateHealthBarDisplay();
            player_invincibility_counter = (0x2c)*4;
            play_sfx(14);
        }
        else
        {
            death_counter = 1;
            player_hoverboard_counter = 0;
        }
    }
}

void Player::pushAround(int push_direction,
                int push_anim_duration,
                int push_duration,
                int player_frame_num,
                int dont_push_while_jumping_flag,
                int check_for_blocking_flag)
{
    player_push_direction = push_direction;
    player_push_anim_duration_maybe = push_anim_duration;
    player_push_anim_counter = 0;
    player_push_duration = push_duration;
    player_push_frame_num = player_frame_num;
    player_dont_push_while_jumping_flag = dont_push_while_jumping_flag;
    player_is_being_pushed_flag = 1;
    player_hoverboard_counter = 0;
    player_push_check_blocking_flag = check_for_blocking_flag;
    player_bounce_flag_maybe = 0;
    player_bounce_height_counter = 0;
    gCosmoPlayer.resetWalkCycle();
}

void Player::updateHoverboard()
{
    static uint16 drop_bomb_state = 0;

    resetWalkCycle();
    hitDetectionWithPlayer = false;
    player_bounce_height_counter = 0;
    riding_hoverboard = 0;
    if (death_counter != 0)
    {
        return;
    }

    if (player_hoverboard_counter <= 1)
    {
        if (cosmoInput.jump_key_pressed != 0)
        {
            player_input_jump_related_flag = 1;
            player_hoverboard_counter = 0;
            riding_hoverboard = 1;
            player_falling_state = 1;
            player_bounce_flag_maybe = 0;
            hitDetectionWithPlayer = true;
            bounceInAir(9);
            player_bounce_height_counter -= 2;
            play_sfx(2);
            return;
        }
    }
    else
    {
        cosmoInput.up_key_pressed = 1;
        player_hoverboard_counter--;
    }

    if (cosmoInput.left_key_pressed != 0 && cosmoInput.right_key_pressed == 0)
    {
        if (player_direction == 0)
        {
            player_x_pos--;
        }

        player_direction = 0;
        player_sprite_dir_frame_offset = 4;
        if (player_x_pos < 1)
        {
            player_x_pos++;
        }

        if (checkMovement(2, player_x_pos, player_y_pos) != 0 ||
            checkMovement(2, player_x_pos, player_y_pos + 1) != 0)
        {
            player_x_pos++;
        }

        if ((static_cast<int>(player_x_pos) & 1) != 0)
        {
            effect_add_sprite(0x13, 4, player_x_pos + 3, player_y_pos + 1, 3, 1);
            play_sfx(0x18);
        }
    }

    if (cosmoInput.right_key_pressed != 0 && cosmoInput.left_key_pressed == 0)
    {
        if (player_direction != 0)
        {
            player_x_pos++;
        }

        player_direction = 0x17;
        player_sprite_dir_frame_offset = 4;
        if (map_width_in_tiles - 4 < player_x_pos)
        {
            player_x_pos--;
        }

        if (checkMovement(3, player_x_pos, player_y_pos) != 0 ||
            checkMovement(3, player_x_pos, player_y_pos + 1) != 0)
        {
            player_x_pos--;
        }
        if ((static_cast<int>(player_x_pos) & 1) != 0)
        {
            effect_add_sprite(0x13, 4, player_x_pos - 1, player_y_pos + 1, 7, 1);
            play_sfx(0x18);
        }
    }

    if (cosmoInput.up_key_pressed == 0 || cosmoInput.down_key_pressed != 0)
    {
        if (cosmoInput.down_key_pressed == 0 || cosmoInput.up_key_pressed != 0)
        {
            player_sprite_dir_frame_offset = 4;
        }
        else
        {
            player_sprite_dir_frame_offset = 6;
            if (map_max_y_offset + 0x11 > player_y_pos)
            {
                player_y_pos++;
            }
            if (checkMovement(1, player_x_pos, player_y_pos + 1) != 0)
            {
                player_y_pos--;
            }
        }
    }
    else
    {
        player_sprite_dir_frame_offset = 5;
        if (player_y_pos > 4)
        {
            player_y_pos--;
        }
        if (checkMovement(0, player_x_pos, player_y_pos) != 0)
        {
            player_y_pos++;
        }
        if ((static_cast<int>(player_y_pos) & 1) != 0)
        {
            effect_add_sprite(0x13, 4, player_x_pos + 1, player_y_pos + 1, 5, 1);
            play_sfx(0x18);
        }
    }

    if (cosmoInput.bomb_key_pressed == 0)
    {
        drop_bomb_state = 0;
    }
    if (cosmoInput.bomb_key_pressed != 0 && drop_bomb_state == 0)
    {
        drop_bomb_state = 1;
        player_sprite_dir_frame_offset = 14;
    }
    if (drop_bomb_state == 0 || drop_bomb_state == 2)
    {
        cosmoInput.bomb_key_pressed = 0;
    }
    else
    {
        player_sprite_dir_frame_offset = 14;
        if (drop_bomb_state != 0)
        {
            drop_bomb_state = 2;
            if (player_direction == 0)
            {
                uint8 tr1 = map_get_tile_attr(player_x_pos - 1, player_y_pos - 2);
                uint8 tr2 = map_get_tile_attr(player_x_pos - 2, player_y_pos - 2);
                if ((tr1 & TILE_ATTR_BLOCK_LEFT) == 0 && (tr2 & TILE_ATTR_BLOCK_LEFT) == 0 && num_bombs > 0)
                {
                    actor_add_new(0x18, player_x_pos - 2, player_y_pos - 2);
                    num_bombs = num_bombs - 1;
                    gStatus.displayNumBombsLeft();
                    play_sfx(0x1d);
                }
                else
                {
                    play_sfx(0x1c);
                }
            }
            else
            {
                uint8 tr1 = map_get_tile_attr(player_x_pos + 3, player_y_pos - 2);
                uint8 tr2 = map_get_tile_attr(player_x_pos + 4, player_y_pos - 2);
                if ((tr1 & TILE_ATTR_BLOCK_RIGHT) == 0 && (tr2 & TILE_ATTR_BLOCK_RIGHT) == 0 && num_bombs > 0)
                {
                    actor_add_new(0x18, player_x_pos + 3, player_y_pos - 2);
                    num_bombs = num_bombs - 1;
                    gStatus.displayNumBombsLeft();
                    play_sfx(0x1d);
                }
                else
                {
                    play_sfx(0x1c);
                }
            }
        }
    }
/*
    if (player_y_pos - mapwindow_y_offset <= 14)
    {
        if (player_bounce_height_counter > 10 && player_y_pos - mapwindow_y_offset < 7 && mapwindow_y_offset > 0)
        {
            mapwindow_y_offset = mapwindow_y_offset - 1;
        }
        if (player_y_pos - mapwindow_y_offset < 7 && mapwindow_y_offset > 0)
        {
            mapwindow_y_offset = mapwindow_y_offset - 1;
        }
    }
    else
    {
        mapwindow_y_offset = mapwindow_y_offset + 1;
    }

    if (player_x_pos - mapwindow_x_offset > 0x17 && map_width_in_tiles - 38 > mapwindow_x_offset)
    {
        mapwindow_x_offset = mapwindow_x_offset + 1;
    }
    else
    {
        if (player_x_pos - mapwindow_x_offset < 12 && mapwindow_x_offset > 0)
        {
            mapwindow_x_offset = mapwindow_x_offset - 1;
        }
    }
    */
}


void Player::moveOnPlatform(int platform_x_left,
                            int platform_x_right,
                            int x_offset_tbl_index,
                            int y_offset_tbl_index)
{
    if(player_hoverboard_counter != 0)
    {
        return;
    }

    int player_right_x_pos = player_sprites[0].frames[0].width + player_x_pos - 1;

    if(mHangingOnWallDirection != 0 && checkMovement(DOWN, player_x_pos, player_y_pos + 1) != 0)
    {
        mHangingOnWallDirection = 0;
    }

    if((player_x_pos < platform_x_left || player_x_pos > platform_x_right) && (player_right_x_pos < platform_x_left || player_right_x_pos > platform_x_right))
    {
        return;
    }

    player_x_pos = player_x_pos + player_x_offset_tbl[x_offset_tbl_index];
    player_y_pos = player_y_pos + player_y_offset_tbl[y_offset_tbl_index];

    if((cosmoInput.up_key_pressed != 0 || cosmoInput.down_key_pressed != 0) && cosmoInput.left_key_pressed == 0 && cosmoInput.right_key_pressed == 0)
    {
        if(cosmoInput.up_key_pressed != 0 && mapwindow_y_offset > 0 && player_y_pos - mapwindow_y_offset < 0x11)
        {
            mapwindow_y_offset = mapwindow_y_offset - 1;
        }
        if(cosmoInput.down_key_pressed != 0 && (mapwindow_y_offset + 4 < player_y_pos || (player_y_offset_tbl[y_offset_tbl_index] == 1 && mapwindow_y_offset + 3 < player_y_pos)))
        {
            mapwindow_y_offset = mapwindow_y_offset + 1;
        }
    }

    if(player_y_pos - mapwindow_y_offset <= 0x11)
    {
        if(player_y_pos - mapwindow_y_offset < 3 && mapwindow_y_offset > 0)
        {
            mapwindow_y_offset = mapwindow_y_offset - 1;
        }
    }
    else
    {
        mapwindow_y_offset = mapwindow_y_offset + 1;
    }

    if(player_x_pos - mapwindow_x_offset <= 0x17 || map_width_in_tiles - 38 <= mapwindow_x_offset)
    {
        if(player_x_pos - mapwindow_x_offset < 12 && mapwindow_x_offset > 0)
        {
            mapwindow_x_offset = mapwindow_x_offset - 1;
        }
    }
    else
    {
        mapwindow_x_offset = mapwindow_x_offset + 1;
    }

    if(player_y_offset_tbl[y_offset_tbl_index] == 1 && player_y_pos - mapwindow_y_offset > 14)
    {
        mapwindow_y_offset = mapwindow_y_offset + 1;
    }

    if(player_y_offset_tbl[y_offset_tbl_index] == -1 && player_y_pos - mapwindow_y_offset < 3)
    {
        if(mapwindow_y_offset > 0)
        {
            mapwindow_y_offset = mapwindow_y_offset - 1;
        }
    }
}



void Player::updateIdleAnim()
{
    player_idle_counter++;
    if(player_idle_counter <= 0x64 || player_idle_counter >= 0x6e)
    {
        if(player_idle_counter <= 0x8b || player_idle_counter >= 0x96)
        {
            if(player_idle_counter == 0xb4)
            {
                //close eyes
                player_sprite_dir_frame_offset = 0x13;
            }
            else
            {
                if(player_idle_counter == 0xb5)
                {
                    player_sprite_dir_frame_offset = 0x14;
                }
                else
                {
                    if(player_idle_counter != 0xb6)
                    {
                        if(player_idle_counter != 0xb7)
                        {
                            if(player_idle_counter != 0xb8)
                            {
                                if(player_idle_counter == 0xb9)
                                {
                                    player_idle_counter = 0;
                                }
                            }
                            else
                            {
                                player_sprite_dir_frame_offset = 0x13;
                            }
                        }
                        else
                        {
                            player_sprite_dir_frame_offset = 0x14;
                        }
                    }
                    else
                    {
                        player_sprite_dir_frame_offset = 0x15;
                    }
                }
            }
        }
        else
        {
            //look down
            player_sprite_dir_frame_offset = 6;
        }
    }
    else
    {
        //look up.
        player_sprite_dir_frame_offset = 5;
    }
}

int Player::xPos() const
{
    return player_x_pos;
}

int Player::yPos() const
{
    return player_y_pos;
}

void Player::setPos(const int x, const int y)
{
    player_x_pos = x;
    player_y_pos = y;
}

int Player::hangingOnWallDir()
{
    return mHangingOnWallDirection;
}

void Player::setHangingOnWallDir(const int dir)
{
    mHangingOnWallDirection = dir;
}

void Player::addSpeechBubble(const SpeechBubbleType type)
{
    actor_add_new(type, player_x_pos - 1, player_y_pos - 5);
}

void Player::hackMoverUpdate()
{
    if (cosmoInput.left_key_pressed) {
        if (player_x_pos > 0) {
            player_x_pos = player_x_pos - 1;
        }
        move_map_window(-1, 0);
    }
    if (cosmoInput.right_key_pressed) {
        if (player_x_pos + 3 < map_width_in_tiles - 1) {
            player_x_pos = player_x_pos + 1;
        }
        move_map_window(1, 0);
    }

    if (cosmoInput.up_key_pressed) {
        if (player_y_pos > 0) {
            player_y_pos = player_y_pos - 1;
        }
        move_map_window(0, -1);
    }

    if (cosmoInput.down_key_pressed) {
        if (player_y_pos < map_height_in_tiles - 1) {
            player_y_pos = player_y_pos + 1;
        }
        move_map_window(0, 1);
    }
}

};
