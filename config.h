//
// cosmo_engine created on 31/Dec/2017.
//
#ifndef COSMO_ENGINE_CONFIG_H
#define COSMO_ENGINE_CONFIG_H

#include <SDL_keycode.h>
#include "defines.h"

#include <string>

int start_cosmo();

void load_config_file();

uint8 keycode_to_scancode(SDL_Keycode keycode);
const char *scancode_to_string(uint8 scan_code);

//char *get_data_dir_full_path(const char *filename);
//char *get_save_dir_full_path(const char *filename);
std::string get_game_dir_full_path(const std::string &filename);

void config_cleanup();

#endif //COSMO_ENGINE_CONFIG_H
