//
// Created by Eric Fry on 31/10/2017.
//

#include "files/file.h"
#include <stdlib.h>
#include "game.h"

#include "tile.h"

#include "base/video/CVideoDriver.h"

uint8 tileattr_mni_data[7000];

Tile::Tile()
{
    pixels.fill(0);
}

Tile::Tile(const Tile &other)
{
    pixels.fill(0);
    if(other.sfc)
        sfc.createCopy(other.sfc);
    if(other.sfcFlipped)
        sfcFlipped.createCopy(other.sfc);
}


void tile_attr_load()
{
    load_file("TILEATTR.MNI", (unsigned char *)&tileattr_mni_data, 7000);
}
uint8 get_tile_size(TileType type)
{
    return type == TileType::SOLID ? (uint8)32 : (uint8)40;
}
uint16 get_number_of_tiles(File *file, TileType type)
{
    uint8 tile_size = get_tile_size(type);

    uint16 num_tiles = 0;
    uint32 filesize = file->size;
    while(filesize > 0xffff)
    {
        num_tiles += 0xffff / tile_size;
        filesize -= 0xffff;
    }

    if(filesize > 0)
    {
        num_tiles += filesize / tile_size;
    }

    return num_tiles;
}

uint8 getPixelAtColumnPosition(uint8 *rowPtr, uint8 column, TileType tileType)
{
    uint8 color = 0;

    if (tileType == TileType::TRANSP) {
        if (*rowPtr & (1 << (7 - column))) {
            return TRANSPARENT_COLOR;
        }
        rowPtr++;
    }

    if (tileType == TileType::FONT) //Font tiles have an inverted mask layer.
    {
        if ((*rowPtr & (1 << (7 - column))) == 0) {
            return TRANSPARENT_COLOR;
        }
        rowPtr++;
    }

    for (int i = 0; i < 4; i++, rowPtr++) {

        color |= (((*rowPtr >> (7 - column)) & 1) << (i));
    }

    return color;
}

void load_tile(uint8 *planarData, TileType type, Tile &tile)
{
    tile.type = type;

    uint8 *pixelPtr = tile.pixels.data();
    auto &sfc = tile.sfc;

    uint8 color = 0;
    SDL_Surface *blit = gVideoDriver.getBlitSurface();
    SDL_PixelFormat *format = blit->format;

    SDL_Rect rect;
    rect.w = TILE_WIDTH;
    rect.h = TILE_HEIGHT;
    sfc.createRGBSurface(rect);

    sfc.lock();

    Uint8 *pixel = sfc.PixelPtr();
    const size_t bpp = format->BytesPerPixel;

    GsColor colorObj;

    for( Uint8 y=0 ; y<sfc.height() ; y++ )
    {
        for( Uint8 x=0 ; x<sfc.width() ; x++ )
        {
            color = getPixelAtColumnPosition(planarData, x, type);
            *pixelPtr = color;

            if(color != TRANSPARENT_COLOR)
            {
                colorObj.readFromEGA(color);
                const Uint32 sdl_col = colorObj.get(format);
                memcpy( pixel, &sdl_col, bpp );
            }

            pixel += bpp;
            pixelPtr++;
        }

        planarData += type == TileType::SOLID ? (uint8)4 : (uint8)5;
    }

    sfc.unlock();
}

std::vector<Tile> load_tiles(const char *filename,
                 const TileType type,
                 uint16 *num_tiles_loaded)
{
    File file;
    std::vector<Tile> tiles;
    if(!open_file(filename, &file))
    {
        printf("Error: opening %s\n", filename);
        return tiles;
    }

    uint8 tile_size = get_tile_size(type);

    uint16 num_tiles = get_number_of_tiles(&file, type);

    for(int i=0;i<num_tiles;i++)
    {
        if(i != 0 && i % (0xffff / tile_size) == 0)
        {
            file_seek(&file, file.pos + (0xffff % tile_size)); //skip the last (unused) bytes from the block.
        }

        Tile tile;
        uint8 planarData[40];
        file_read_to_buffer(&file, planarData, tile_size);
        load_tile(planarData, type, tile);
        tiles.push_back(tile);        
    }

    *num_tiles_loaded = num_tiles;
    file_close(&file);    

    return tiles;
}

uint16 get_number_of_sprite_frames(uint16 current_frame_num, uint16 offset, uint16 num_total_sprites, File *file)
{
    uint16 next_offset = (uint16)(file->size / 2);
    if(current_frame_num < num_total_sprites - 1)
    {
        next_offset = file_read2(file);
    }

    return (uint16)((next_offset - offset) / 4);
}

std::vector<Sprite> load_tile_info(const char *filename, uint16 *num_records_loaded)
{
    File file;
    std::vector<Sprite> sprites;
    if(!open_file(filename, &file))
    {
        printf("Error: opening tileinfo file %s\n", filename);
        return sprites;
    }

    *num_records_loaded = (uint16)file_read2(&file);

    sprites.resize(*num_records_loaded);

    for(uint16 sprite_index=0;sprite_index < *num_records_loaded; sprite_index++)
    {
        auto &curSprite = sprites[sprite_index];

        curSprite.num_frames = 0;
        for(int j=sprite_index; curSprite.num_frames == 0 && j < *num_records_loaded; j++) //advance through the sprite info offsets until we find some frames
        {
            file_seek(&file, (uint32) j * 2);
            curSprite.num_frames = 0;

            uint16 offset = file_read2(&file);
            curSprite.num_frames = get_number_of_sprite_frames(j, offset, *num_records_loaded, &file);

            if (curSprite.num_frames != 0)
            {
                file_seek(&file, (uint32) offset * 2);

                curSprite.frames.resize(curSprite.num_frames);

                for (auto &frame : curSprite.frames)
                {
                    frame.height = file_read2(&file);
                    frame.width = file_read2(&file);
                    uint32 frameOffset = file_read4(&file);
                    frame.tile_num = (uint16)((frameOffset >> 16) * 1638 + (frameOffset & 0xffff) / 40);
                }
            }
        }
    }

    file_close(&file);

    return sprites;
}

