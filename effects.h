//
// cosmo_engine created on 12/Nov/2017.
//
#ifndef COSMO_ENGINE_EFFECTS_H
#define COSMO_ENGINE_EFFECTS_H


#include "defines.h"

void explosion_add_sprite(int x_pos, int y_pos);
int explosion_collision_test(int actorInfoIndex, int frame_num, int x_pos, int y_pos);
void explosion_update_sprites();
void explosion_clear_sprites();

int blow_up_actor_with_bomb(int actorInfoIndex, int frame_num, int x_pos, int y_pos);
void effect_add_sprite(int actorInfoIndex, int frame_num, int x_pos, int y_pos, int arg_8, int counter);

void effect_update_sprites(const bool draw_only);
void effect_update_rain_sprites(const bool draw_only);
void effect_clear_sprites();

void explode_effect_add_sprite(int actorInfoIndex, int frame_num, int x_pos, int y_pos);
void explode_effect_update_sprites();
void explode_effect_clear_sprites();

void exploding_balls_effect(int x_pos, int y_pos);

void update_rain_effect();

void add_brightness_obj(uint8 type, int x_pos, int y_pos);

void update_brightness_objs();
void clear_brightness_objs();

#endif //COSMO_ENGINE_EFFECTS_H
