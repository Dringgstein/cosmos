//
// Created by efry on 25/10/2017.
//

#ifdef __EMSCRIPTEN__
#include "emscripten.h"
#endif
#include <SDL_timer.h>
#include <SDL_events.h>
#include "sound/sfx.h"
#include "actor.h"
#include "palette.h"
#include "player.h"
#include "files/vol.h"
#include "tile.h"
#include "game.h"
#include "input.h"
#include "dialog.h"
#include "map.h"
#include "status.h"
#include "video.h"
#include "actor_toss.h"
#include "effects.h"
#include "font.h"
#include "platforms.h"
#include "fullscreen_image.h"
#include "cartoon.h"
#include "high_scores.h"
#include "config.h"

#define COSMO_INTERVAL 10

using namespace cosmos_engine;

//Data
GAME_PLAY_MODE game_play_mode = GAME_PLAY_MODE::GAME;
uint32 score;

uint8 knows_about_powerups_flag;
uint8 finished_game_flag_maybe = 0;
uint8 finished_level_flag_maybe;
uint8 cheat_mode_flag = 0;

uint8 episode_number = 1;
bool quick_start_mode = false;

void game_wait();
void select_next_level();
void end_sequence();

uint8 get_episode_number()
{
    return episode_number;
}

bool is_quick_start()
{
    return quick_start_mode;
}

void game_init()
{
    if(!is_quick_start())
    {
        display_fullscreen_image(0);
        wait_for_time_or_key(0xc8);
    }

    load_config_file();

    gStatus.loadTiles();
    tile_attr_load();
    gCosmoPlayer.loadTiles();
    actor_load_tiles();
    map_load_tiles();
    load_cartoon_images();
    font_load_tiles();
    load_sfx();
}

void set_initial_game_state()
{
    score = 0;
    health = 4;
    num_health_bars = 3;
    current_level = 0;
    num_bombs = 0;
    gStatus.init();
    has_had_bomb_flag = 0;
    knows_about_powerups_flag = 0;
    return;
}

void reset_game_state()
{
    finished_game_flag_maybe = 0;
    riding_hoverboard = 1;
    jumpStateIdx = 0;
    player_falling_state = 1;
    finished_level_flag_maybe = 0;
    teleporter_counter = 0;
    teleporter_state_maybe = 0;
    energy_beam_enabled_flag = 1;
    byte_2E17C = 0;

    gCosmoPlayer.resetState();
    gCosmoPlayer.resetWalkCycle();

    hide_player_sprite = 0;
    move_platform_flag = 1;
    protection_barrier_active = 0;
    palette_2E1EE = 0;
    palette_index = 0;
    rnd_index = 0;
    player_fall_off_map_bottom_counter = 0;
    show_hint_dialog = 0;
    num_containers = 0;
    num_eye_plants_remaining_to_blow_up = 0;
    num_hits_since_touching_ground = 0;

    //Actor values
    speech_bubble_hamburger_shown_flag = false;
    speech_bubble_red_plant_shown_flag = false;
    speech_bubble_special_switch_shown_flag = false;
    speech_bubble_clam_trap_shown_flag = false;
    speech_bubble_silver_robot_shown_flag = false;
    speech_bubble_purple_boss_shown_flag = false;
    speech_bubble_pipe_shown_flag = false;
    speech_bubble_teleporter_shown_flag = false;
    speech_bubble_hoverboard_shown_flag = false;
    speech_bubble_rubber_wall_shown_flag = false;
    speech_bubble_floor_spring_shown_flag = false;
}

int lps_tick = 0;
const int SKIP_TICKS=4;

//const int SKIP_TICKS=8;

bool executeLogicsLegacy()
{
    /*
    lps_tick++;       

    if(lps_tick%SKIP_TICKS == 0)
    {
        update_palette_anim();

        handle_player_input_maybe();
        if (player_hoverboard_counter != 0)
        {
            player_hoverboard_update();
        }

        if (hands_on_eyes != 0 || walk_anim_index != 0)
        {
            player_update_walk_anim(); //TODO check name I think this might be fall anim
        }

        update_moving_platforms();

        update_mud_fountains();
    }


    if(lps_tick%SKIP_TICKS == 0)
    {
        map_display();

        if (player_update_sprite() != 0) {
            return true;
        }

        display_mud_fountains();

        actor_update_all(false);

        explode_effect_update_sprites();

        actor_toss_update();

        update_rain_effect();

        struct6_update_sprites();

        effect_update_sprites();

        update_brightness_objs();

        if (game_play_mode != GAME_PLAY_MODE::GAME)
        {
            actorMan.display_sprite_maybe(0x10a, 0, 17, 4, 6); //DEMO sign.
        }

        if (show_monster_attack_hint == 1)
        {
            show_monster_attack_hint = 2;
            monster_attack_hint_dialog();
        }

        display_ingame_hint_dialog();
    }

    if(lps_tick%SKIP_TICKS == 0)
    {
        if (finished_level_flag_maybe)
        {
            finished_level_flag_maybe = 0;
            play_sfx(11);
            select_next_level();
            load_level(current_level);
        }

        if (finished_game_flag_maybe)
        {
            end_sequence();
            return false;
        }
    }
    */
    return true;
}

bool executeLogics(sint8 &local_selected_ingame_hint_dialog)
{
    lps_tick++;

    auto &actorMan = gActorMan;
    auto &player = gCosmoPlayer;

    const bool draw_only = (lps_tick%SKIP_TICKS != 0);

    if(!draw_only)
    {
        update_palette_anim();
    }


    if(!draw_only)
    {
        player.handleInput();
        if (player_hoverboard_counter != 0)
        {
            player.updateHoverboard();
        }
    }

    player.handleScrolling();

    if (player.hands_on_eyes != 0 || player.walk_anim_index != 0)
    {
        player.updateWalkAnim();
    }

    update_moving_platforms(draw_only);

    update_mud_fountains(draw_only);

    map_display();

    if (player.updateSprite() != 0)
    {
        return true;
    }

    display_mud_fountains();
    display_moving_platforms();

    actor_update_all(draw_only);

    explode_effect_update_sprites();

    actor_toss_update(draw_only);

    update_rain_effect();

    explosion_update_sprites();

    effect_update_sprites(draw_only);
    effect_update_rain_sprites(draw_only);

    update_brightness_objs();

    if (game_play_mode != GAME_PLAY_MODE::GAME)
    {
        actorMan.display_sprite_maybe(0x10a, 0, 17, 4, DrawMode::ON_DIALOG); //DEMO sign.
    }

    if (show_monster_attack_hint == 1)
    {
        show_monster_attack_hint = 2;
        monster_attack_hint_dialog();
    }

    update_ingame_hint_dialog(local_selected_ingame_hint_dialog);

    if (finished_level_flag_maybe)
    {
        gStatus.hide(true);
        finished_level_flag_maybe = 0;
        play_sfx(11);
        select_next_level();
        load_level(current_level);
    }

    if (finished_game_flag_maybe)
    {
        gStatus.hide(true);
        end_sequence();
        return false;
    }

    return true;
}


bool executeTick()
{
    /*
    auto input_state = read_input();

    if (input_state == PAUSED)
        return true;

    if (input_state == QUIT)
        return false;

    executeLogicsLegacy();
*/
    return true;
}

void game_loop()
{
    for(;executeTick();)
    {
        //locked to 10 FPS here.
        game_wait();        
    }
}

uint32 time_left()
{
    static uint32 next_time = 0;
    uint32 now;

    now = SDL_GetTicks();

    if ( next_time <= now ) {
        next_time = now+COSMO_INTERVAL;
        return(0);
    }
    Uint32 delay = next_time-now;
    next_time += COSMO_INTERVAL;
    return(delay);
}

void game_wait()
{
    SDL_Delay(time_left());
}

void select_next_level()
{
    gStatus.hide(true);
    const uint32 tmp_num_stars_collected = gStatus.numStartsCollected();
    if (game_play_mode == GAME_PLAY_MODE::GAME)
    {
        switch (current_level)
        {
            case 2:
            case 6:
            case 10:
            case 14:
            case 18:
            case 22:
            case 26:
                display_end_of_level_score_dialog("Bonus Level Completed!!", "Press ANY key.");
                current_level += 2;
                break;

            case 3:
            case 7:
            case 11:
            case 15:
            case 19:
            case 23:
            case 27:
                display_end_of_level_score_dialog("Bonus Level Completed!!", "Press ANY key.");
                current_level++;
                break;

            case 0:
            case 4:
            case 8:
            case 12:
            case 16:
            case 20:
            case 24:
                current_level++;
                break;

            case 1:
            case 5:
            case 9:
            case 13:
            case 17:
            case 21:
            case 25:
                display_end_of_level_score_dialog("Section Completed!", "Press ANY key.");
                if(tmp_num_stars_collected <= 24)
                {
                    current_level += 3;
                }
                else
                {
                    fade_to_black(0);
                    display_fullscreen_image(3);
                    play_sfx(0x2d);
                    if(tmp_num_stars_collected > 49)
                    {
                        current_level++;
                    }
                    current_level++;
                    cosmo_wait(0x96);
                }
                break;

            default: break;
        }
    }
    else //DEMO Mode
    {
        switch (current_level)
        {
            case 0:
                current_level = 13;
                break;

            case 13:
                current_level = 5;
                break;

            case 5:
                current_level = 9;
                break;

            case 9:
                current_level = 0x10;
                break;
        }
    }
}

std::string get_game_vol_filename()
{
    std::string volfname;

    switch(get_episode_number())
    {
        case 1 : volfname = "COSMO1.VOL"; break;
        case 2 : volfname = "COSMO2.VOL"; break;
        case 3 : volfname = "COSMO3.VOL"; break;
        default : break;
    }

    return volfname;
}

const char *get_game_stn_filename()
{
    switch(get_episode_number())
    {
        case 1 : return "COSMO1.STN";
        case 2 : return "COSMO2.STN";
        case 3 : return "COSMO3.STN";
        default : break;
    }

    return NULL;
}
//Protected

unsigned char *load_file(const char *filename, unsigned char *buf, uint32 buf_size)
{
    uint32 bytes_read;
    const std::string volfname = get_game_vol_filename();
    if(vol_file_load(volfname.c_str(), filename, buf, buf_size, &bytes_read))
    {
        return buf;
    }

    return vol_file_load(get_game_stn_filename(), filename, buf, buf_size, &bytes_read);
}

unsigned char *load_file_in_new_buf(const char *filename, uint32 *file_size)
{
    unsigned char *buf;
    const std::string volfname = get_game_vol_filename();
    buf = vol_file_load(volfname.c_str(), filename, nullptr, 0, file_size);
    if(buf)
    {
        return buf;
    }

    return vol_file_load(get_game_stn_filename(), filename, nullptr, 0, file_size);
}

bool open_file(const std::string &filename, File *file)
{
    const std::string vol_fname(get_game_vol_filename());

    if(vol_file_open(vol_fname, filename, file))
    {
        return true;
    }

    const std::string stn_fname(get_game_stn_filename());
    return vol_file_open(stn_fname, filename, file);
}

void set_episode_number(uint8 episode)
{
    episode_number = episode;
}

void enable_quick_start_mode()
{
    quick_start_mode = true;
}
