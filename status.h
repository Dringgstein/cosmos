#ifndef COSMO_ENGINE_STATUS_H
#define COSMO_ENGINE_STATUS_H

#include "tile.h"

#include <base/Vector2D.h>
#include <base/Singleton.h>
#include <base/interface/Geometry.h>

#include <vector>

#define gStatus cosmos_engine::Status::get()

namespace cosmos_engine
{

class Status : public GsSingleton<Status>
{
public:

    void loadTiles();

    void init();

    void initPanel();

    void displayEverything();


    void addToScoreUpdateOnDisplay(const int amount_to_add_lows);

    void updateHealthBarDisplay();

    void displayNumStarsCollected();

    void displayNumBombsLeft();

    void setNumStartsCollected(const int value);
    int numStartsCollected() const ;

    void addStar();

    void hide(const bool value);

    bool isHidden();

private:

    /**
     * @brief display Displays background
     */
    void displayBox();

    void display_number(int x_pos, int y_pos, uint32 number);


    int numStarsCollected = 0;

    GsRect<int> mHUDDim;

    bool mHidden = false;

    const int  mBarHeight = 6;
    const int  mBarWidth = 38;

    std::vector<Tile> mTiles;
};

};



#endif //COSMO_ENGINE_STATUS_H
