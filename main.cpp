//
// Created by efry on 21/07/2017.
//

#include <SDL.h>
#include "sound/audio.h"
#include "sound/music.h"
#include "game.h"
#include "map.h"
#include "dialog.h"
#include "video.h"
#include "status.h"
#include "config.h"
#include "high_scores.h"
#include "demo.h"
#include "b800.h"
#include "input.h"

#include <base/GsArguments.h>
#include <base/GsLogging.h>

int cleanup_and_exit();

using namespace cosmos_engine;

int start_cosmo()
{
    gLogging << "Initializiung graphics subsystem..." << CLogFile::endl;
    video_init();

    gLogging << "Initializiung audio subsystem..." << CLogFile::endl;
    audio_init();
    gLogging << "Loading main game files..." << CLogFile::endl;
    game_init();

    video_fill_screen_with_black();

    gLogging << "Starting the game..." << CLogFile::endl;
    if(is_quick_start())
    {        
        set_initial_game_state();

        const auto argLevel = gArgs.getValue("level");

        if(!argLevel.empty())
        {
            current_level = atoi(argLevel.c_str());
        }

        game_play_mode = GAME_PLAY_MODE::GAME;
    }


    return 0;
}

void load_current_level()
{
    load_level(current_level);
}


int run_gameplay()
{
    while(game_play_mode != GAME_PLAY_MODE::QUIT)
    {
        load_level(current_level);
        game_loop();
        stop_music();
        if(game_play_mode == GAME_PLAY_MODE::GAME)
        {
            show_high_scores();
        }
        game_play_mode = main_menu();
    }

    stop_music();
    display_exit_text();

    return cleanup_and_exit();
}

int cleanup_and_exit()
{
    config_cleanup();
    video_shutdown();

    return 0;
}
