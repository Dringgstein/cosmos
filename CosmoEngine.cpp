
#include "CosmoEngine.h"
#include "cosmosintro.h"

#include <base/video/CVideoDriver.h>
#include <graphics/GsGraphics.h>
#include <base/interface/FindFile.h>

#include <base/CInput.h>
#include <engine/core/menu/ControlSettings.h>
#include <engine/core/menu/CHelpMenu.h>

#include <fileio/CSaveGameController.h>


#include "input.h"

#include "cosmogameplay.h"
#include "game.h"
#include "config.h"
#include "font.h"
#include "dialog.h"
#include "status.h"
#include "fullscreen_image.h"

#include <base/GsArguments.h>

void set_game_data_dir(const char *dir, const int len);

SDL_Surface *get_cur_vid_surface();

GsSurface &get_vid_surface_overlay();

using namespace std;

namespace cosmos_engine
{


void pumpCommonCosmosEvents(const std::shared_ptr<CEvent> &evPtr, const Style style)
{
    if( const auto pNewGame =
        std::dynamic_pointer_cast<const NewGamePlayersEvent>(evPtr) )
    {
        gBehaviorEngine.setNumPlayers(pNewGame->mSelection);
        gEventManager.add( new OpenMenuEvent(new CDifficultySelection(style)) );
        game_play_mode = GAME_PLAY_MODE::GAME;
        return;
    }
    else if( const auto pStart = std::dynamic_pointer_cast<const StartNewGameEvent>(evPtr) )
    {
        gBehaviorEngine.mDifficulty = pStart->mDifficulty;
        gEventManager.add( new cosmos_engine::SwitchScene(new CosmoGameplay) );
        gMenuController.clearMenuStack();

        if(game_play_mode != GAME_PLAY_MODE::DEMO)
        {
            game_play_mode = GAME_PLAY_MODE::GAME;
            display_fullscreen_image(5);
        }

        return;
    }
}


CosmosEngine::CosmosEngine(const bool openedGamePlay,
                           const int ep,
                           const std::string &dataPath) :
GameEngine(openedGamePlay, dataPath),
mEp(ep)
{
    gSaveGameController.setEpisode(mEp);
    set_episode_number(mEp);
}

CosmosEngine::~CosmosEngine()
{}

bool CosmosEngine::setupFont()
{
    gGraphics.freeFonts();
    gGraphics.createEmptyFontmaps(1);

    uint16 num_tiles;
    auto font_tiles = load_tiles("FONTS.MNI", TileType::FONT, &num_tiles);
    gLogging << "Loading " << num_tiles << " font tiles." << CLogFile::endl;

    SDL_Color *Palette = gGraphics.Palette.m_Palette;
    const auto flags = gVideoDriver.getScrollSurface(0).getFlags();

    auto &font = gGraphics.getFontLegacy(0);
    font.setLetterAsciiOffset(-22);
    font.setLowerCaseLetterOffset(26);
    font.setTintingAllowed(false);
    font.setColumns(10);
    font.setRows(10);

    const auto width = 8*10;
    const auto height = (num_tiles/10)*8;
    font.CreateSurface(Palette, flags,
                       width, height);

    GsWeakSurface weak(font.SDLSurfacePtr());
    weak.fill(16);

    for(uint16 k=0 ; k<num_tiles ; k++)
    {
        font.setWidthToCharacter(8, k);
        Tile &fonttile = font_tiles[k];

        const auto y = (k/10)*8;
        const auto x = (k%10)*8;

        GsRect<Uint16> dstRect(x, y, width, height);
        fonttile.sfc.blitTo(weak, dstRect);
    }

    font.deriveHighResSurfaces();

    return true;
}

/**
 * @brief start Starts the Cosmos engine
 */
bool CosmosEngine::start()
{
    gStatus.hide(true);

    if(!setupNativeRes("Cosmos", 3))
        return false;

    bool quickstart = false;
    const auto argLevel = gArgs.getValue("level");

    if(!argLevel.empty())
    {
        quickstart = true;
        enable_quick_start_mode();
    }

    const auto gameDir = GetFullFileName(mDataPath);

    set_game_data_dir(gameDir.c_str(),
                      gameDir.size());

    start_cosmo();

    if(!setupFont())
        return false;

    if(!quickstart)
    {
        mpScene.reset( new CosmosIntro );
        mpScene->start();
    }
    else
    {
        auto gameplay = new CosmoGameplay;
        mpScene.reset( gameplay );
        gameplay->start();
        gameplay->loadCurrentLevel();
    }

    return true;
}

/**
 * @brief ponder    Everything logic related here
 * @param deltaT    timestep
 */
void CosmosEngine::ponder(const float deltaT)
{
    cosmoInput.processInput();

    if(mpScene)
    {
        auto &overlay = get_vid_surface_overlay();
        overlay.fillRGBA(0x0, 0x0, 0x0, 0x0);
        mpScene->ponder(deltaT);
    }
}


void CosmosEngine::pumpEvent(const std::shared_ptr<CEvent> &evPtr)
{
    if(mpScene)
    {
        mpScene->pumpEvent(evPtr);
    }

    if( const auto switchScene =
            std::dynamic_pointer_cast<const SwitchScene>(evPtr))
    {
        mpScene.reset(switchScene->mpEngine);
        mpScene->start();

        auto &&scene = std::move( mpScene );
        auto *gameplay = dynamic_cast<CosmoGameplay*>(scene.get());
        if( gameplay )
        {
            gameplay->loadCurrentLevel();
        }
    }
    else if( const auto ctrlMenu =
             std::dynamic_pointer_cast<const OpenControlMenuEvent>(evPtr) )
    {
        const int players = ctrlMenu->mSelection;
        gEventManager.add( new OpenMenuEvent(
                                new CControlsettings(players, Style::VORTICON) ) );
    }
    else if( std::dynamic_pointer_cast<const LoadGameEvent>(evPtr) ) // If GamePlayMode is not running but loading is requested...
    {
        auto &&scene = std::move( mpScene );
        auto *gameplay = dynamic_cast<CosmoGameplay*>(scene.get());
        if( !gameplay )
        {
            mpScene.reset(new CosmoGameplay);
            mpScene->start();
            gameplay = dynamic_cast<CosmoGameplay*>( mpScene.get() );
        }

       gameplay->loadGameState();
    }
    else if( const auto scene =
             std::dynamic_pointer_cast<const StartHelpEv>(evPtr) )
    {
        gMenuController.clearMenuStack();

        if(scene->mType ==
           StartHelpEv::Variant::ORDERING)
        {
            if (get_episode_number() == 1)
            {
                ordering_info_dialog();
            }
            else
            {
                commercial_ordering_information_dialog();
            }
        }
        else if(scene->mType ==
           StartHelpEv::Variant::ABOUT_ID)
        {
           apogee_bbs_dialog();
        }
        else if(scene->mType ==
           StartHelpEv::Variant::STORY)
        {
           story_dialog();
        }
        else if(scene->mType ==
           StartHelpEv::Variant::THEGAME)
        {
           instructions_dialog();
        }

    }
    else if(std::dynamic_pointer_cast<const EventHighScores>(evPtr))
    {
        gMenuController.clearMenuStack();
        //fade_to_black_speed_3();
        //video_fill_screen_with_black();
        display_high_score_dialog(true);
    }


}


/**
 * @brief render    Everything to ne drawn here!
 */
void CosmosEngine::render()
{
    if(mpScene)
    {
        mpScene->render();
    }

    GsWeakSurface curSfc(get_cur_vid_surface());
    GsWeakSurface dst(gVideoDriver.getBlitSurface());

    curSfc.blitTo(dst);

    auto &overlay = get_vid_surface_overlay();

    GsRect<Uint16> srcRect = { 0,0,
                               overlay.width(), overlay.height() };
    GsRect<Uint16> dstRect = { 0,0,
                               dst.width(), dst.height() };

    overlay.blitTo(dst, srcRect.SDLRect(), dstRect.SDLRect());
}


}


StartCosmosEngine::StartCosmosEngine(const bool openedGamePlay,
                                     const int ep,
                                     const std::string &dataPath) :
    SwitchEngineEvent(
        new cosmos_engine::CosmosEngine(openedGamePlay, ep, dataPath) )
{ }
