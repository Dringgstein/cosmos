//
// cosmo_engine created on 31/Dec/2017.
//

#include <SDL_keycode.h>
#include <SDL_filesystem.h>
#include "files/file.h"
#include "sound/music.h"
#include "sound/sfx.h"
#include "config.h"
#include "high_scores.h"
#include "input.h"
#include "game.h"
#include "video.h"

#include <fileio/ResourceMgmt.h>
#include <base/GsLogging.h>

#define MAX_SCORE_STRING_LENGTH 13
#define NUM_SCAN_CODES 89

char *data_directory = nullptr;
char *save_directory = nullptr;
char *game_data_directory = nullptr;

SDL_Keycode scancode_to_keycode(uint8 scan_code);
uint8 keycode_to_scancode(SDL_Keycode keycode);

uint32 read_score(File *file)
{
    uint32 cur_pos = file_get_current_position(file);
    uint32 file_len = file_get_filesize(file);
    char buf[MAX_SCORE_STRING_LENGTH+1];
    memset(buf, 0, MAX_SCORE_STRING_LENGTH+1);
    for(int i=0; i < MAX_SCORE_STRING_LENGTH && cur_pos + i < file_len; i++)
    {
        char c = file_read1(file);
        if(c == ' ')
        {
            break;
        }
        buf[i] = c;
    }

    return (uint32)strtol(buf, NULL, 10);
}

void read_score_name(File *file, char *buf, uint16 buf_size)
{
    uint32 cur_pos = file_get_current_position(file);
    uint32 file_len = file_get_filesize(file);
    memset(buf, 0, buf_size);
    for(int i=0; i < buf_size-1 && cur_pos + i < file_len; i++)
    {
        char c = file_read1(file);
        if(c == 0xa)
        {
            break;
        }
        buf[i] = c;
    }
}

const char *get_config_filename()
{
    switch(get_episode_number())
    {
        case 1 : return "COSMO1.CFG";
        case 2 : return "COSMO2.CFG";
        case 3 : return "COSMO3.CFG";
        default : break;
    }

    return nullptr;
}

void load_config_file()
{
    clear_high_score_table();
    {
        set_input_command_key(CMD_KEY_UP, SDLK_UP);
        set_input_command_key(CMD_KEY_DOWN, SDLK_DOWN);
        set_input_command_key(CMD_KEY_LEFT, SDLK_LEFT);
        set_input_command_key(CMD_KEY_RIGHT, SDLK_RIGHT);
        set_input_command_key(CMD_KEY_JUMP, SDLK_LCTRL);
        set_input_command_key(CMD_KEY_BOMB, SDLK_LALT);
        music_on_flag = 1;
        sfx_on_flag = 1;

        add_high_score_with_name(0xf4240, "BART");
        add_high_score_with_name(0xdbba0, "LISA");
        add_high_score_with_name(0xc3500, "MARGE");
        add_high_score_with_name(0xaae60, "ITCHY");
        add_high_score_with_name(0x927c0, "SCRATCHY");
        add_high_score_with_name(0x7a120, "MR. BURNS");
        add_high_score_with_name(0x61a80, "MAGGIE");
        add_high_score_with_name(0x493e0, "KRUSTY");
        add_high_score_with_name(0x30d40, "HOMER");
    }
}

const char scancode_string_tbl[NUM_SCAN_CODES][6] = {
        "NULL",
        "ESC",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "0",
        "-",
        "=",
        "BKSP",
        "TAB",
        "Q",
        "W",
        "E",
        "R",
        "T",
        "Y",
        "U",
        "I",
        "O",
        "P",
        " ",
        " ",
        "ENTR",
        "CTRL",
        "A",
        "S",
        "D",
        "F",
        "G",
        "H",
        "J",
        "K",
        "L",
        ";",
        "\"",
        " ",
        "LSHFT",
        " ",
        "Z",
        "X",
        "C",
        "V",
        "B",
        "N",
        "M",
        ",",
        ".",
        "?",
        "RSHFT",
        "PRTSC",
        "ALT",
        "SPACE",
        "CAPLK",
        "F1",
        "F2",
        "F3",
        "F4",
        "F5",
        "F6",
        "F7",
        "F8",
        "F9",
        "F10",
        "NUMLK",
        "SCRLK",
        "HOME",
        "\x18",
        "PGUP",
        "-",
        "\x1b",
        "5",
        "\x1c",
        "+",
        "END",
        "\x19",
        "PGDN",
        "INS",
        "DEL",
        "SYSRQ",
        " ",
        " ",
        "F11",
        "F12"
};

const SDL_Keycode scancode_to_keycode_tbl[NUM_SCAN_CODES] = {
        SDLK_UNKNOWN,
        SDLK_ESCAPE,
        SDLK_1,
        SDLK_2,
        SDLK_3,
        SDLK_4,
        SDLK_5,
        SDLK_6,
        SDLK_7,
        SDLK_8,
        SDLK_9,
        SDLK_0,
        SDLK_MINUS,
        SDLK_EQUALS,
        SDLK_BACKSPACE,
        SDLK_TAB,
        SDLK_q,
        SDLK_w,
        SDLK_e,
        SDLK_r,
        SDLK_t,
        SDLK_y,
        SDLK_u,
        SDLK_i,
        SDLK_o,
        SDLK_p,
        SDLK_LEFTBRACKET,
        SDLK_RIGHTBRACKET,
        SDLK_RETURN,
        SDLK_LCTRL,
        SDLK_a,
        SDLK_s,
        SDLK_d,
        SDLK_f,
        SDLK_g,
        SDLK_h,
        SDLK_j,
        SDLK_k,
        SDLK_l,
        SDLK_SEMICOLON,
        SDLK_QUOTEDBL,
        SDLK_QUOTE,
        SDLK_UNKNOWN, // Left Shift
        SDLK_SLASH,
        SDLK_z,
        SDLK_x,
        SDLK_c,
        SDLK_v,
        SDLK_b,
        SDLK_n,
        SDLK_m,
        SDLK_COMMA,
        SDLK_PERIOD,
        SDLK_QUESTION,
        SDLK_UNKNOWN, // right shift
        SDLK_PRINTSCREEN,
        SDLK_LALT,
        SDLK_SPACE,
        SDLK_CAPSLOCK,
        SDLK_F1,
        SDLK_F2,
        SDLK_F3,
        SDLK_F4,
        SDLK_F5,
        SDLK_F6,
        SDLK_F7,
        SDLK_F8,
        SDLK_F9,
        SDLK_F10,
        SDLK_NUMLOCKCLEAR,
        SDLK_SCROLLLOCK,
        SDLK_HOME,
        SDLK_UP,
        SDLK_PAGEUP,
        SDLK_KP_MINUS,
        SDLK_LEFT,
        SDLK_KP_5,
        SDLK_RIGHT,
        SDLK_KP_PLUS,
        SDLK_END,
        SDLK_DOWN,
        SDLK_PAGEDOWN,
        SDLK_INSERT,
        SDLK_DELETE,
        SDLK_SYSREQ,
        SDLK_UNKNOWN,
        SDLK_UNKNOWN,
        SDLK_F11,
        SDLK_F12
};

SDL_Keycode scancode_to_keycode(uint8 scan_code)
{
    if(scan_code >= NUM_SCAN_CODES)
    {
        return SDLK_UNKNOWN;
    }

    return scancode_to_keycode_tbl[scan_code];
}

uint8 keycode_to_scancode(SDL_Keycode keycode)
{
    for(uint8 i = 0; i < NUM_SCAN_CODES; i++)
    {
        if(scancode_to_keycode_tbl[i] == keycode)
        {
            return i;
        }
    }

    return 0;
}

const char *scancode_to_string(uint8 scan_code)
{
    if(scan_code >= NUM_SCAN_CODES)
    {
        scan_code = 0;
    }

    return &scancode_string_tbl[scan_code][0];
}

void set_game_data_dir(const char *dir, const int len)
{
    if(!game_data_directory)
        game_data_directory = (char *)SDL_malloc(len+1);

    strcpy(game_data_directory, dir);
}
char *get_full_path(const char *base_dir, const char *filename)
{
    char *path = (char *)malloc(strlen(base_dir) + strlen(filename) + 2); //2 = path delimiter + \0
    sprintf(path, "%s%c%s", base_dir, '/', filename); //FIXME handle windows path separator
    return path;
}

std::string get_game_dir_full_path(const std::string &filename)
{
    return getResourceFilename(filename, game_data_directory);
}

void config_cleanup()
{
    if(game_data_directory)
    {
        SDL_free(game_data_directory);
    }

    if(save_directory)
    {
        SDL_free(save_directory);
    }
}
