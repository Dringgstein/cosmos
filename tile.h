//
// Created by Eric Fry on 31/10/2017.
//

#ifndef COSMO_ENGINE_TILE_H
#define COSMO_ENGINE_TILE_H

#include "defines.h"

#include "graphics/GsSurface.h"

#include <vector>
#include <array>

enum class TileType
{
    SOLID,
    TRANSP,
    FONT
};

enum TileAttr
{
    BLOCK_DOWN = 0x1,
    BLOCK_UP = 0x2,
    BLOCK_LEFT = 0x4,
    BLOCK_RIGHT = 0x8,
    SLIPPERY = 0x10,
    IN_FRONT = 0x20,
    SLOPED = 0x40,
    CAN_GRAB_WALL = 0x80,
};

struct Tile
{
    TileType type = TileType::SOLID;
    std::array<uint8, 64>  pixels;
    GsSurface sfc;
    GsSurface sfcFlipped;

    Tile();
    Tile(const Tile &tile);
};

struct TileInfo
{
    uint16 height = 0;
    uint16 width = 0;
    uint16 tile_num = 0;
};

struct Sprite
{
    uint16 num_frames;
    std::vector<TileInfo> frames;
};

#define TILE_ATTR_NONE              0u
#define TILE_ATTR_BLOCK_DOWN      0x1u
#define TILE_ATTR_BLOCK_UP        0x2u
#define TILE_ATTR_BLOCK_LEFT      0x4u
#define TILE_ATTR_BLOCK_RIGHT     0x8u
#define TILE_ATTR_SLIPPERY       0x10u
#define TILE_ATTR_IN_FRONT       0x20u
#define TILE_ATTR_SLOPED         0x40u
#define TILE_ATTR_CAN_GRAB_WALL  0x80u

#define TILE_HEIGHT 8
#define TILE_WIDTH 8

#define TRANSPARENT_COLOR 255

extern uint8 tileattr_mni_data[7000];

void tile_attr_load();

std::vector<Tile> load_tiles(const char *filename, const TileType type, uint16 *num_tiles_loaded);
std::vector<Sprite> load_tile_info(const char *filename, uint16 *num_records_loaded);

#endif //COSMO_ENGINE_TILE_H
