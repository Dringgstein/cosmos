#include "cosmogameplay.h"

#include "files/file.h"
#include "tile.h"
#include "map.h"
#include "player.h"
#include "actor.h"
#include "status.h"
#include "dialog.h"
#include "backdrop.h"
#include "save.h"

#include <SDL_events.h>
#include <base/GsEventContainer.h>
#include <base/GsLogging.h>
#include <base/video/CVideoDriver.h>
#include <engine/core/CBehaviorEngine.h>
#include <graphics/GsGraphics.h>
#include <widgets/GsMenuController.h>
#include <base/CInput.h>

#include <fileio/CSaveGameController.h>

#include <engine/core/menu/MainMenu.h>

#include "cosmosintro.h"
#include "CosmoEngine.h"

typedef unsigned short uint16;
typedef unsigned char  uint8;

int run_gameplay();
void load_current_level();
void load_level(int level_number);
bool executeLogics(sint8 &local_selected_ingame_hint_dialog);

bool set_backdrop(uint16 backdrop_index);

void set_backdropEv(int backdrop_index);

void loadLevelEv(int level_number);

uint8 get_episode_number();

std::string get_level_filename(int level_number);

bool open_file(const char *filename, File *file);



uint16 getNumBackdropTiles();

int getMapWidth();
int getMapHeight();

uint16 map_get_tile_cell(int x, int y);

uint16 getNumBgTiles();
uint16 getNumFgTiles();

Tile *map_get_bg_tile(uint16 tile_num);
Tile *map_get_fg_tile(uint16 tile_num);

uint16 *map_data_ptr();

struct SetBackdropEvent : public CEvent
{

    SetBackdropEvent(const int index) :
        mIndex(index) {}
    const int mIndex = 0;
};

struct LoadLevelEvent : public CEvent
{
    LoadLevelEvent(const int level) :
        mLevel(level) {}
    const int mLevel = 0;
};


void loadLevelEv(int level_number)
{
    gEventManager.add( new LoadLevelEvent(level_number) );
}

void set_backdropEv(int backdrop_index)
{
    gEventManager.add( new SetBackdropEvent(backdrop_index) );
}

CosmoGameplay::CosmoGameplay() :
    mpMap(new CMap)
{
}


void CosmoGameplay::setMapPtrs()
{
    gCosmoPlayer.setMapPtr(mpMap);
    gActorMan.setMapPtr(mpMap);
}

void CosmoGameplay::loadCurrentLevel()
{
    load_current_level();
}

bool CosmoGameplay::start()
{
    setMapPtrs();
    return true;
}


bool CosmoGameplay::load_level_data(const int level_number)
{
    File map_file;
    const auto level_fname = get_level_filename(level_number);
    if(!open_file(level_fname, &map_file))
    {
        gLogging << "Error: loading level data. "<< level_fname << CLogFile::endl;
        return false;
    }

    file_seek(&map_file, 2);
    const auto map_width_in_tiles = file_read2(&map_file);
    const auto map_height_in_tiles = 32768 / map_width_in_tiles - 1;

    mpMap->setupEmptyDataPlanes(3, 8, map_width_in_tiles, map_height_in_tiles);

    gLogging.ftextOut("map width (in tiles): %d\n", map_width_in_tiles);

    return true;
}


auto extractTilemap(GsTilemap &tilemap,
                    const Tile *tiles,
                    const int num_tiles,
                    const int skipRow) -> bool
{
    SDL_Surface *sfc = tilemap.getSDLSurface();
    const auto col = tilemap.getNumColumn();

    GsWeakSurface weak(sfc);
    weak.fill(COLORKEY);

    for(int t=0 ; t<num_tiles ; t++)
    {
        const Tile *tile = &(tiles[t]);

        const uint16 tx = (t%col);
        const uint16 ty = (t/col);

        if(skipRow == ty)
            continue;

        const GsRect<Uint16> dstRect(tx*8, ty*8,
                                     tile->sfc.width(), tile->sfc.height());
        tile->sfc.blitTo(weak, dstRect);
    }

    return true;
}

bool CosmoGameplay::loadLevel(const int level_number)
{
    gGraphics.freeTilemap();
    gGraphics.createEmptyTilemaps(4);

    File map_file;
    auto levelFname = get_level_filename(level_number);
    if(!open_file(levelFname, &map_file))
    {
        gLogging << "Error: loading level data. " << levelFname << CLogFile::endl;
        return false;
    }

    file_close(&map_file);

    load_level_data(level_number);

    gGraphics.Palette.setupColorPalettes(nullptr, 0);

    GsTilemap &tilemap0 = gGraphics.getTileMap(0);
    const auto num_backdrop_Tiles = getNumBackdropTiles();
    tilemap0.CreateSurface( gGraphics.Palette.m_Palette, SDL_SWSURFACE,
                           num_backdrop_Tiles, 3, 40 );

    auto &scrollSfcBackdrop = gVideoDriver.getScrollSurfaceVec()[0];
    scrollSfcBackdrop.setScale(1.4f);


    // Mask the RGB color 0xA8,0x00, 0xA8 so clear color can shine through.
    // This is mostly used in the stage with the spooky forest (level 4)
    tilemap0.setColorKey(5);


    auto &tiles0 = getBackdropTilesRef();
    extractTilemap(tilemap0, tiles0.data(), num_backdrop_Tiles, -1);

    GsTilemap &tilemap1 = gGraphics.getTileMap(1);
    const auto num_bg_Tiles = getNumBgTiles();
    tilemap1.CreateSurface( gGraphics.Palette.m_Palette, SDL_SWSURFACE,
                            num_bg_Tiles, 3, 40 );

    auto *tiles1 = map_get_bg_tile(0);
    extractTilemap(tilemap1, tiles1, num_bg_Tiles, 0);

    GsTilemap &tilemap2 = gGraphics.getTileMap(2);
    const auto num_fg_Tiles = getNumFgTiles();
    tilemap2.CreateSurface( gGraphics.Palette.m_Palette, SDL_SWSURFACE,
                            num_fg_Tiles, 3, 40 );

    auto *tiles2 = map_get_fg_tile(0);
    extractTilemap(tilemap2, tiles2, num_fg_Tiles, -1);

    // Get the MAPHEAD Location from within the Exe File or an external file
    std::vector<char> mapHeadContainer;

    // Set Map position and some flags for the freshly loaded level
    mpMap->gotoPos(0, 0);
    mpMap->setLevel(level_number);
    mpMap->isSecret = false;
    mpMap->mNumFuses = 0;

    // Create empty tileproperties map
    CTileProperties emptyTileProperties;

    std::vector<CTileProperties> &bdTileProperties = gBehaviorEngine.getTileProperties(0);
    bdTileProperties.assign(getNumBackdropTiles(), emptyTileProperties);
    std::vector<CTileProperties> &bgTileProperties = gBehaviorEngine.getTileProperties(1);
    bgTileProperties.assign(getNumBgTiles(), emptyTileProperties);
    std::vector<CTileProperties> &fgTileProperties = gBehaviorEngine.getTileProperties(2);
    fgTileProperties.assign(getNumFgTiles(), emptyTileProperties);

    const auto width = getMapWidth();
    const auto height = getMapHeight();
    mpMap->setupEmptyDataPlanes(3, 8, width, height);

    auto &scrollPlane2 = mpMap->getScrollingPlane(2);
    const int transparentTile = 0xFFFF;
    scrollPlane2.setTransparentTile(transparentTile);

    gLogging.textOut("Reading plane 0 (Backdrop)<br>" );

    auto *map0_data = mpMap->getData(0);
    const auto col = tilemap0.getNumColumn();
    for(int y = 0 ; y<height ; y++)
    {
        for(int x = 0 ; x<width ; x++)
        {
            const auto offset = x + y*width;

            auto backdrop_tile = (x%col) + y*col;
            backdrop_tile = backdrop_tile % num_backdrop_Tiles;
            map0_data[offset] = backdrop_tile;
        }
    }

    gLogging.textOut("Reading plane 1 and 2 (Back and Foreground)<br>" );
    auto *map1_data = mpMap->getData(1);
    auto *map2_data = mpMap->getData(2);

    auto *cosmo_map_data = map_data_ptr();
    for(int i = 0 ; i<height*width ; i++)
    {
        const auto map_cell = cosmo_map_data[i];
        const uint16 tile = map_cell/8;

        map2_data[i] = transparentTile;

        if(tile < bgTileProperties.size() && tile < 2000)
        {
            if(tile)
            {
                map1_data[i] = tile;
            }
        }
        else
        {
            uint16 fgTile = (tile-2000)/5;

            if(fgTile < fgTileProperties.size())
            {
                map2_data[i] = fgTile;
            }

        }
    }

    mpMap->insertHorBlocker(0);
    mpMap->insertHorBlocker((height)<<8); // TODO: Only if status is not displayed
    mpMap->insertVertBlocker(0);
    mpMap->insertVertBlocker((width+8)<<8);
    mpMap->setupAnimationTimer();


    mpMap->setSubscrollUnits(0, 2);
    mpMap->lockAxisY(0, true);

    // Set Scrollbuffer
    mpMap->drawAll();

    gLogging.textOut("Map got loaded successfully!");

    return true;
}


void CosmoGameplay::loadGameState()
{
    if(cosmos_engine::loadXMLGameState())
    {
        load_level(current_level);
    }
    else
    {
        gLogging << "Error loading Gamestate" << CLogFile::endl;
    }
    gInput.flushAll();
}

void CosmoGameplay::pumpEvent(const std::shared_ptr<CEvent> &evPtr)
{
    cosmos_engine::pumpCommonCosmosEvents(evPtr, mMenuStyle);
    if( const auto levelEv = std::dynamic_pointer_cast<const LoadLevelEvent>(evPtr) )
    {
        if(!loadLevel(levelEv->mLevel))
        {
            gEventManager.add( new cosmos_engine::SwitchScene(new CosmosIntro) );
        }
    }
    else if( std::dynamic_pointer_cast<const SaveGameEvent>(evPtr) )
    {
        cosmos_engine::saveXMLGameState();
        gInput.flushAll();
    }
    else if( std::dynamic_pointer_cast<const EventEndGamePlay>(evPtr) )
    {
        gEventManager.add( new cosmos_engine::SwitchScene(new CosmosIntro) );
    }
}


void CosmoGameplay::handleScrolling()
{
    if(!mpMap->getNumScrollingPlanes())
        return;

    auto frontCoords = mpMap->getScrollCoords(1);

    // 1 tile = 8px

    // NOTE: mpMap->m_scrollx is pixel based
    const auto mapwindow_x_offset_pix = mapwindow_x_offset*8.0f;
    const auto mapwindow_y_offset_pix = mapwindow_y_offset*8.0f;


    int scroll_diff_x = (frontCoords.x-mapwindow_x_offset_pix);
    int scroll_diff_y = (frontCoords.y-mapwindow_y_offset_pix);

    const int DIST_FOR_SLOMO = 7;

    if(scroll_diff_x < 0)
    {
        do
        {
            scroll_diff_x = (frontCoords.x-mapwindow_x_offset_pix);
            if(!mpMap->scrollRight())
                break;

            frontCoords = mpMap->getScrollCoords(1);
        } while(scroll_diff_x < -DIST_FOR_SLOMO);
    }
    else if(scroll_diff_x > 0)
    {
        do
        {
            scroll_diff_x = (frontCoords.x-mapwindow_x_offset_pix);
            if(!mpMap->scrollLeft())
                break;

            frontCoords = mpMap->getScrollCoords(1);
        } while(scroll_diff_x > DIST_FOR_SLOMO);
    }


    if(scroll_diff_y < 0)
    {
        do
        {
            scroll_diff_y = (frontCoords.y-mapwindow_y_offset_pix);
            if(!mpMap->scrollDown())
                break;

            frontCoords = mpMap->getScrollCoords(1);
        } while(scroll_diff_y < -DIST_FOR_SLOMO);

    }
    else if(scroll_diff_y > 0)
    {
        do
        {
            scroll_diff_y = (frontCoords.y-mapwindow_y_offset_pix);
            if(!mpMap->scrollUp())
                break;

            frontCoords = mpMap->getScrollCoords(1);
        } while(scroll_diff_y > DIST_FOR_SLOMO);
    }

}

void draw_dialog_frame_for_menus(GsWeakSurface& sfc);
void draw_dialog_twirl(GsWeakSurface& sfc, const GsRect<int> lRect);

void CosmoGameplay::openMainMenu()
{
    auto &menuCtrl = gMenuController;

    if( !menuCtrl.empty() )
        return;

    mPaused = true;

    menuCtrl.mBackroundDrawFcn = draw_dialog_frame_for_menus;
    menuCtrl.mDrawTwirlFcn = draw_dialog_twirl;
    menuCtrl.mExecAfterClose = [&] () { mPaused = false; };
    gEventManager.add( new OpenMenuEvent(
                           new MainMenu(true,
                                        mMenuStyle,
                                        true, "Cosmos") ) );
}


void CosmoGameplay::ponder(const float deltaT)
{
    gStatus.hide(false);

    // Did the player press the quit/back button
    if( gMenuController.empty() )
    {   // If no menu is open, there is a chance to get opened
        if( gInput.getPressedCommand(IC_BACK) )
        {
            openMainMenu();
        }
    }

/*
    int input_state = process_ext_input_one_ev();

    if (input_state == QUIT)
    {
        //return false;
        return;
    }
*/
    if(mPaused)
    {
        return;
    }


    if(mpMap->isEmpty())
        return;

    handleScrolling();
    mpMap->calcVisibleArea();
    mpMap->refreshVisibleArea();

    if(mSelected_ingame_hint_dialog == -1 )
    {
        if(!executeLogics(mSelected_ingame_hint_dialog))
        { // Finished games, go to intro
            gEventManager.add( new cosmos_engine::SwitchScene(new CosmosIntro) );
        }
    }
    else
    {
        update_ingame_hint_dialog(mSelected_ingame_hint_dialog);
        display_ingame_hint_dialog();
    }


    mpMap->animateAllTiles();
}

void CosmoGameplay::render()
{
    gVideoDriver.blitScrollSurfaces();
    gStatus.displayEverything();
}
