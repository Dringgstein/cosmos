#include "cosmosintro.h"
#include "game.h"
#include "dialog.h"
#include "input.h"
#include "demo.h"
#include "sound/music.h"

#include "fullscreen_image.h"

#include "engine/core/menu/SelectionMenu.h"
#include "cosmogameplay.h"
#include "CosmoEngine.h"

#include <widgets/GsMenuController.h>
#include <engine/core/menu/MainMenu.h>
#include <base/CInput.h>
#include <base/GsApp.h>

CosmosIntro::CosmosIntro()
{}


bool CosmosIntro::start()
{
    //display_fullscreen_image(1);
    a_game_by_dialog();

    if(gInput.getExitEvent())
        return false;

    set_initial_game_state();
    show_one_moment_screen_flag = 0;
    load_music(0x12);
    display_fullscreen_image(1);
    flush_input();
    return true;
}



void CosmosIntro::pumpEvent(const std::shared_ptr<CEvent> &evPtr)
{
    cosmos_engine::pumpCommonCosmosEvents(evPtr, mMenuStyle);
}

// TODO: Move those into appropiate headers.
void draw_dialog_frame_for_menus(GsWeakSurface& sfc);
void draw_dialog_twirl(GsWeakSurface& sfc, const GsRect<int> lRect);


void CosmosIntro::ponder(const float deltaT)
{
    // If menu is closed and if a button was pressed
    if( !gMenuController.active() )
    {
        if( gInput.getPressedAnyCommand() || gInput.mouseClicked() )
        {
            if( gMenuController.empty() ) // If no menu is open, open the main menu
            {
                gMenuController.mEnableTwirl = true;
                gMenuController.mBackroundDrawFcn = draw_dialog_frame_for_menus;
                gMenuController.mDrawTwirlFcn = draw_dialog_twirl;
                gEventManager.add( new OpenMenuEvent(
                                       new MainMenu(false, mMenuStyle, true, "Cosmos") ) );
            }
        }
    }


    /*SDL_Keycode key = poll_for_key_press(false);
    if(key != SDLK_UNKNOWN)
    {
        if (key == SDLK_q || key == SDLK_ESCAPE)
        {
            if (GAME_PLAY_MODE::QUIT_dialog())
            {
                return GAME_PLAY_MODE::QUIT;
            }
        }

        uint16 restore_status = 0;
        for(int return_to_title=0;!return_to_title;)
        {
            main_menu_dialog();
            bool key_handled = true;
            do {
                key = display_menu_items_in_dialog(11, main_menu_items, 0x1c, 0x15);
                switch(key)
                {
                    case SDLK_ESCAPE :
                    case SDLK_q :
                        if(GAME_PLAY_MODE::QUIT_dialog())
                        {
                            return GAME_PLAY_MODE::QUIT;
                        }
                        return_to_title = 1;
                        i = 0;
                        break;

                    case SDLK_b :
                    case SDLK_SPACE :
                        stop_music();
                        show_one_moment_screen_flag = 1;
                        show_monster_attack_hint = 0;
                        play_sfx(0x30);
                        return GAME_PLAY_MODE::GAME;

                    case SDLK_r :
                        restore_status = restore_savegame_dialog();
                        if(restore_status == 1)
                        {
                            stop_music();
                            return GAME_PLAY_MODE::GAME;
                        }

                        if(restore_status == 0)
                        {
                            missing_savegame_dialog();
                        }
                        break;

                    case SDLK_i :
                        instructions_dialog();
                        break;

                    case SDLK_s :
                        story_dialog();
                        break;

                    case SDLK_g :
                        game_redefine();
                        break;

                    case SDLK_F11 :
                        if (cheat_mode_flag)
                        {
                            return RECORD_DEMO;
                        }
                        break;

                    case SDLK_o :
                        if (get_episode_number() == 1) {
                            ordering_info_dialog();
                        } else {
                            commercial_ordering_information_dialog();
                        }
                        break;

                    case SDLK_f :
                        foreign_orders_dialog();
                        break;

                    case SDLK_a :
                        apogee_bbs_dialog();
                        break;

                    case SDLK_d :
                        return GAME_PLAY_MODE::DEMO;

                    case SDLK_h :
                        fade_to_black_speed_3();
                        video_fill_screen_with_black();
                        display_high_score_dialog(true);
                        break;

                    case SDLK_c :
                        display_fullscreen_image(2);
                        while(poll_for_key_press(false)==SDLK_UNKNOWN)
                        {}
                        break;

                    case SDLK_t :
                        return_to_title = 1;
                        i = 0;
                        break;

                    default :
                        key_handled = false;
                        break;
                }
            } while(!key_handled && !return_to_title);

            display_fullscreen_image(1);
        }
    }
    else*/
    {
        if(mTimer == 600)
        {
            display_fullscreen_image(2);
        }
        if(mTimer == 1200)
        {
            load_demo();
            game_play_mode = GAME_PLAY_MODE::DEMO;
            gEventManager.add( new StartNewGameEvent(NORMAL) );
        }
    }

    gApp.render();
    gApp.ignoreLogicCounter();
    SDL_Delay((Uint32)(8 * 3));

    mTimer++;
}

void CosmosIntro::render()
{

}
