//
// Created by efry on 3/11/2017.
//

#include <SDL_system.h>
#include "video.h"
#include "palette.h"
#include "tile.h"
#include "input.h"
#include "b800_font.h"

#include "base/video/CVideoDriver.h"

#include <base/GsApp.h>
#include <base/CInput.h>


typedef struct VideoSurface {
    SDL_Texture *texture;
    SDL_Surface *surface;
    SDL_Surface *windowSurface;
} VideoSurface;

GsSurface overlay;

VideoSurface game_surface;
VideoSurface text_surface;

bool is_game_mode = true;
bool is_fullscreen = false;
bool video_has_initialised = false;
int video_scale_factor = DEFAULT_SCALE_FACTOR;


SDL_Surface *get_game_surface()
{
    return game_surface.surface;
}

void video_fill_surface_with_black(SDL_Surface *surface);

void fade_to_black_speed_3()
{
    fade_to_black(3);
}

bool init_surface(VideoSurface *surface, int width, int height)
{
    surface->surface = SDL_CreateRGBSurface(0, width, height, 8, 0, 0, 0, 0);
    if(surface->surface == NULL)
    {
        printf("Error: creating indexed surface. %s\n", SDL_GetError());
        return false;
    }

    const SDL_Rect rect = { 0,0, width, height };
    overlay.createRGBSurface(rect);

    surface->windowSurface = SDL_CreateRGBSurface(0,
                                         surface->surface->w, surface->surface->h,
                                         32, 0, 0, 0, 0);
    if(surface->windowSurface == NULL)
    {
        printf("Error: creating window surface. %s\n", SDL_GetError());
        return false;
    }

    return true;
}

bool video_init()
{
    init_surface(&game_surface, SCREEN_WIDTH, SCREEN_HEIGHT);
    set_palette_on_surface(game_surface.surface);

    init_surface(&text_surface, SCREEN_WIDTH*2, SCREEN_HEIGHT*2);
    set_palette_on_surface(text_surface.surface);
    video_fill_surface_with_black(text_surface.surface);

    set_game_mode();

    video_has_initialised = true;
    return true;
}

void video_shutdown()
{
}

void set_text_mode()
{
    if(!is_game_mode)
        return;
    is_game_mode = false;
}

void set_game_mode()
{
    if(is_game_mode)
        return;
    is_game_mode = true;
}

SDL_Surface *get_cur_vid_surface()
{
    return is_game_mode ? game_surface.surface : text_surface.surface;
}

GsSurface &get_vid_surface_overlay()
{
    return overlay;
}


void drawSpriteToOverlay( GsSprite &spr,
                 const int x,
                 const int y )
{
    auto &overlay = get_vid_surface_overlay();
    spr.drawSprite(overlay.getSDLSurface(), x, y);
}


void set_colorKey_and_flush_gamesfc()
{
    SDL_SetColorKey(game_surface.surface, SDL_TRUE, TRANSPARENT_COLOR);
    SDL_FillRect(game_surface.surface, NULL, TRANSPARENT_COLOR);
}

void video_draw_tileSfc_dst(GsWeakSurface &tileSfc,
                            int x, int y, GsWeakSurface &dst)
{
    GsWeakSurface weak(game_surface.surface);

    auto height = tileSfc.height();
    auto width =  tileSfc.width();

    if(x+width > weak.width())
        width = weak.width()-x;

    if(y+height > weak.height())
        height = weak.height()-y;

    GsRect<Uint16> dstRect(x, y, width, height);
    GsRect<Uint16> srcRect(0, 0, width, height);

    if(x<0)
    {
        dstRect.pos.x = 0;
        srcRect.pos.x = -x;
        dstRect.dim.x = srcRect.dim.x;
    }

    if(y<0)
    {
        dstRect.pos.y = 0;
        srcRect.pos.y = -y;
        dstRect.dim.y = srcRect.dim.y;
    }

    tileSfc.blitTo(dst, srcRect.SDLRect(), dstRect.SDLRect());
}

void video_draw_tileSfc(GsWeakSurface &tileSfc, int x, int y)
{
    assert(tileSfc.getSDLSurface() != nullptr);
    auto &overlay = get_vid_surface_overlay();
    video_draw_tileSfc_dst(tileSfc, x, y, overlay);
}


void video_draw_tile(Tile *tile, int x, int y)
{
    video_draw_tileSfc(tile->sfc, x, y);
}

void video_draw_font_tile(Tile *tile, uint16 x, uint16 y, uint8 font_color)
{
    video_draw_tile(tile, x, y);
}

void video_draw_tile_solid_white(Tile *tile, uint16 x, uint16 y)
{
    if(tile->type == TileType::SOLID)
    {
        video_draw_tile(tile, x, y);
    }
    else
    {               
        tile->sfc.setAlpha(0xAF);
        video_draw_tile(tile, x, y);
        tile->sfc.setAlpha(0xFF);
        /*
        uint8 *pixel = (uint8 *)game_surface.surface->pixels + x + y * SCREEN_WIDTH;
        uint8 *tile_pixel = tile->pixels;

        for(int i=0;i<TILE_HEIGHT;i++)
        {
            for(int j=0;j<TILE_WIDTH;j++)
            {
                if(*tile_pixel != TRANSPARENT_COLOR)
                {
                    *pixel = 0xf;
                }
                pixel++;
                tile_pixel++;
            }
            pixel += SCREEN_WIDTH - TILE_WIDTH;
        }
        */
    }
}

void video_draw_tile_light(Tile *tile, uint16 x, uint16 y)
{
    tile->sfc.setAlpha(0x8F);
    video_draw_tile(tile, x, y);
    tile->sfc.setAlpha(0xFF);
}

void drawHighlightGameSfc()
{
    //GsWeakSurface gameSfc(game_surface.surface);
    //gameSfc.setAlpha(0x80);

    GsWeakSurface weak(game_surface.surface);
    auto &overlay = get_vid_surface_overlay();
    weak.blitTo(overlay);
}

void video_draw_highlight_effect(const int x,
                                 const int y,
                                 const int type)
{
    assert(type>=0);

    // Cropping mechanism for cases we cannot draw fully the 8x8 blocks
    const int left =   (x<0) ? -x : 0;
    const int top =    (y<0) ? -y : 0;
    const int right =  (x+TILE_WIDTH >= game_surface.surface->w) ?
                        x-game_surface.surface->w : TILE_WIDTH;
    const int bottom = (y+TILE_HEIGHT >= game_surface.surface->h) ?
                        y-game_surface.surface->h : TILE_HEIGHT;

    auto *sfc = game_surface.surface;
    uint8 *pixel = (uint8 *)sfc->pixels +
                    ((left>0) ? 0 : x) +
                    ((top>0) ? 0 : y * (sfc->pitch));

    for(int i=0;i<bottom;i++)
    {
        for(int j=0;j<right;j++)
        {
            if((type == 0 && i + j >= 7) ||
               type == 1 ||
               (type == 2 && i >= j))
            {
                *pixel = 0xF; //set color to white
            }
            pixel++;
        }
        pixel += SCREEN_WIDTH - TILE_WIDTH;
    }
}

void video_draw_tile_with_clip_rect(Tile *tile,
                                    uint16 x,
                                    uint16 y,
                                    uint16 clip_x,
                                    uint16 clip_y,
                                    uint16 clip_w,
                                    uint16 clip_h)
{
    uint16 tx = 0;
    uint16 ty = 0;
    uint16 w = TILE_WIDTH;
    uint16 h = TILE_HEIGHT;

    if (x + w < clip_x ||
        y + h < clip_y ||
        x > clip_x + clip_w ||
        y > clip_y + clip_h)
    {
        return;
    }

    if (x < clip_x)
    {
        tx = (clip_x - x);
        w = TILE_WIDTH - tx;
        x = clip_x;
    }

    if (x + w > clip_x + clip_w)
    {
        w -= ((x + w) - (clip_x + clip_w));
    }

    if (y < clip_y)
    {
        ty = (clip_y - y);
        h = TILE_HEIGHT - ty;
        y = clip_y;
    }

    if (y + h > clip_y + clip_h)
    {
        h -= ((y + h) - (clip_y + clip_h));
    }

    uint8 *pixel = (uint8 *)game_surface.surface->pixels + x + y * SCREEN_WIDTH;
    uint8 *tile_pixel = &tile->pixels[tx + ty * TILE_WIDTH];
        for(int i=0;i<h;i++)
        {
            for(int j=0; j < w; j++)
            {
                if(tile_pixel[j] != TRANSPARENT_COLOR)
                {
                    pixel[j] = tile_pixel[j];
                }
            }
            pixel += SCREEN_WIDTH;
            tile_pixel += TILE_WIDTH;
        }
}

void video_draw_tile_flipped(Tile *tile, const int x, const int y)
{
    if(tile->sfcFlipped.empty())
    {
        tile->sfcFlipped.createCopy(tile->sfc);
        tile->sfcFlipped.mirrorSurfaceHoriz();
    }

    video_draw_tileSfc(tile->sfcFlipped, x, y);
}

void video_update_palette(int palette_index, SDL_Color new_color)
{
    SDL_SetPaletteColors(game_surface.surface->format->palette, &new_color, palette_index, 1);
}

void fade_to_black(uint16 wait_time)
{
    for(int i=0;i < 16; i++)
    {
        // TODO: Set Overlay color to black
        float intensity = (1.0f - (float(i))/15.0f);
        gVideoDriver.setLightIntensity(intensity);
        cosmo_wait(wait_time);
    }

    auto &overlay = get_vid_surface_overlay();
    overlay.fillRGB(0x00, 0x00, 0x00);
}

void fade_to_white(uint16 wait_time)
{
    for(int i=0;i < 16; i++)
    {
        // TODO: Set Overlay color to white
        float intensity = (1.0f - (float(i))/15.0f);
        gVideoDriver.setLightIntensity(intensity);
        cosmo_wait(wait_time);
    }

    auto &overlay = get_vid_surface_overlay();
    overlay.fillRGB(0xFF, 0xFF, 0xFF);
}

void fade_in_from_black(uint16 wait_time)
{
    if(gInput.getExitEvent())
        return;

    for(int i=0;i < 16; i++)
    {
        float intensity = (float(i)/15.0f);
        gVideoDriver.setLightIntensity(intensity);
        cosmo_wait(wait_time);
    }
}

void fade_in_from_black_with_delay_3()
{
    fade_in_from_black(3);
}

void video_fill_surface_with_black(SDL_Surface *surface)
{
    SDL_Rect rect;
    rect.x = 0;
    rect.y = 0;
    rect.w = surface->w;
    rect.h = surface->h;
    SDL_FillRect(surface, &rect, 0);
}

void video_fill_screen_with_black()
{
    video_fill_surface_with_black(game_surface.surface);
}

void video_draw_fullscreen_image(uint8 *pixels)
{
    memcpy(game_surface.surface->pixels, pixels, 320 * 200);        
    GsWeakSurface weak(game_surface.surface);
    auto &overlay = get_vid_surface_overlay();
    weak.blitTo(overlay);
}

void video_draw_text(uint8 character, int fg, int bg, int x, int y)
{
    uint8 *pixel = (uint8 *)text_surface.surface->pixels + x*B800_FONT_WIDTH + y*B800_FONT_HEIGHT*text_surface.surface->w;

    int count=0;
    for(int i=0;i<B800_FONT_HEIGHT;i++)
    {
        for (int j = 0; j < B800_FONT_WIDTH; j++)
        {
            int p = (int10_font_16[character * B800_FONT_HEIGHT + (count / 8)] >> (7 - (count % 8))) & 1;
            pixel[j] = p ? (uint8) fg : (uint8) bg;
            count++;
        }
        pixel += text_surface.surface->w;
    }
}

void video_set_scale_factor(int scale_factor)
{
    video_scale_factor = scale_factor;
}
