//
// cosmo_engine created on 27/Nov/2017.
//

#include "platforms.h"
#include "defines.h"
#include "player.h"
#include "map.h"
#include "input.h"
#include "actor.h"
#include "video.h"

#include <graphics/GsGraphics.h>
#include <base/video/CVideoDriver.h>

uint16 num_mud_fountains = 0;
MovingPlatform moving_platform_tbl[10];
MudFountain mud_fountain_tbl[10];

GsSprite platform_sprite;

void update_mud_fountains(const bool draw_only)
{
    if(draw_only)
        return;

    auto &player = gCosmoPlayer;

    for(int si=0;si < num_mud_fountains; si++)
    {
        MudFountain *mud_fountain = &mud_fountain_tbl[si];
        if(mud_fountain->pause_counter == 0)
        {
            mud_fountain->length_counter++;
            if(mud_fountain->length_counter != mud_fountain->max_height)
            {
                map_write_tile_cell(0, mud_fountain->x, mud_fountain->y);
                map_write_tile_cell(0, mud_fountain->x + 1 + 1, mud_fountain->y);
                if(player.death_counter == 0)
                {
                    if(mud_fountain->y - 1 == gCosmoPlayer.yPos())
                    {
                        if(mud_fountain->direction == 0)
                        {
                            gCosmoPlayer.moveOnPlatform(mud_fountain->x, mud_fountain->x + 2, 0, 1);
                        }
                        else
                        {
                            gCosmoPlayer.moveOnPlatform(mud_fountain->x, mud_fountain->x + 2, 0, 5);
                        }
                    }
                }
                
                if(mud_fountain->direction == 0)
                {
                    mud_fountain->y--;
                    mud_fountain->current_height++;
                }
                else
                {
                    mud_fountain->y++;
                    mud_fountain->current_height--;
                }
                
                map_write_tile_cell(0x48, mud_fountain->x, mud_fountain->y);
                map_write_tile_cell(0x48, mud_fountain->x + 2, mud_fountain->y);
            }
            else
            {
                mud_fountain->length_counter = 0;
                mud_fountain->direction = (uint16)((mud_fountain->direction ? -1 : 0) + 1);
                mud_fountain->pause_counter = 10;
            }
        }
        else
        {
            mud_fountain->pause_counter--;
        }
    }

    return;
}

void display_mud_fountains()
{
    static uint16 frame_counter = 0;
    static uint16 frame_num = 0;
    frame_counter++;

    auto &actorMan = gActorMan;

    if ((frame_counter & 1) != 0)
    {
        frame_num++;
    }
    for (int i = 0; i < num_mud_fountains; i++)
    {
        MudFountain *mud_fountain = &mud_fountain_tbl[i];
        actorMan.display_sprite_maybe(0x4f, frame_num & 1, mud_fountain->x, mud_fountain->y + 1, DrawMode::NORMAL);

        for (int j = 0; j < mud_fountain->current_height + 1; j++)
        {
            actorMan.display_sprite_maybe(0x4f, (frame_num & 1) + 1 + 1, mud_fountain->x + 1, mud_fountain->y + j + 1, DrawMode::NORMAL);

            if (gCosmoPlayer.checkCollisionWithActor(0x4f, 2, mud_fountain->x + 1, mud_fountain->y + j + 1) != 0)
            {
                gCosmoPlayer.decreaseHealth();
            }
        }
    }

    return;
}

static void load_platform_sprite()
{
    const auto pal = gGraphics.Palette.m_Palette;
    const auto flags = gVideoDriver.mpVideoEngine->getBlitSurface()->flags;

    const auto width = 40;
    const auto height = 8;
    platform_sprite.setSize(width, height);
    platform_sprite.createSurface(flags, pal);

    auto &gsSfc = platform_sprite.Surface();

    GsTilemap &tilemap1 = gGraphics.getTileMap(1);

    GsWeakSurface tileWeakSfc(tilemap1.getSDLSurface());

    GsRect<Uint16> srcRect(144, 392, width, height);
    GsRect<Uint16> dstRect(0, 0, width, height);
    tileWeakSfc.blitTo(gsSfc, srcRect.SDLRect(), dstRect.SDLRect());
}

void update_moving_platforms(const bool draw_only)
{
    if(draw_only)
        return;

    auto &player = gCosmoPlayer;

    for(int i=0 ; i < num_moving_platforms ; i++)
    {
        MovingPlatform *platform = &moving_platform_tbl[i];

        for (int si = 2; si < 7; si++)
        {
            map_write_tile_cell(platform->map_tiles[si - 2], platform->x + si - 4, platform->y);
        }



        uint16 map_tile_num = (uint16) (map_get_tile_cell(platform->x, platform->y) / 8);
        if (player.death_counter == 0)
        {
            if (platform->y - 1 == player.yPos() && move_platform_flag != 0)
            {
                player.moveOnPlatform(platform->x - 2, platform->x + 2, map_tile_num, map_tile_num);
            }
        }

        if (move_platform_flag != 0)
        {
            platform->x += player_x_offset_tbl[map_tile_num];
            platform->y += player_y_offset_tbl[map_tile_num];
        }

        for (int si = 2; si < 7; si++)
        {
            platform->map_tiles[si - 2] = (uint16) map_get_tile_cell(platform->x + si - 4, platform->y);
        }

        for (int si = 2; si < 7; si++)
        {
            map_write_tile_cell(((si - 2) / 8) + 0x3dd0, platform->x + si - 4, platform->y);
        }
    }
}

void display_moving_platforms()
{
    auto &player = gCosmoPlayer;
    for(int i=0 ; i < num_moving_platforms ; i++)
    {
        MovingPlatform *platform = &moving_platform_tbl[i];

        // Platform: Copy certain tile so they become a sprite
        if(platform_sprite.empty())
        {
            load_platform_sprite();
        }
        else
        {
            const auto scroll = player.getLocalMapScroll();
            const uint16 screen_x = (platform->x) * 8.0f - (scroll.x) - 16;
            const uint16 screen_y = (platform->y) * 8.0f - (scroll.y);

            drawSpriteToOverlay(platform_sprite,
                                screen_x, screen_y);

        }
    }
}
