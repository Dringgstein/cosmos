#include <stdio.h>
#include "status.h"
#include "defines.h"
#include "video.h"
#include "map.h"
#include "game.h"
#include "font.h"
#include "player.h"

#include <base/video/CVideoDriver.h>

#include <base/GsLogging.h>
#include <engine/core/CBehaviorEngine.h>

namespace cosmos_engine
{


void Status::hide(const bool value)
{
    mHidden = value;
}

bool Status::isHidden()
{
    return mHidden;
}

void Status::display_number(int x_pos, int y_pos, uint32 number)
{
    uint8 font_color = FONT_WHITE;
    char buf[32];
    sprintf(buf, "%d", number);

    const bool modernStyle = gBehaviorEngine.mOptions[GameOption::MODERN].value;
    if(modernStyle)
    {
        for( int i=0 ; i < int(strlen(buf)) ; i++ )
        {
            video_draw_font_tile(&font_tiles[buf[i]-48+26], (x_pos - strlen(buf) + i + 1) * 8, (y_pos * 8) + 4, font_color);
        }
    }
    else
    {
        for( int i=0 ; i < int(strlen(buf)) ; i++ )
        {
            video_draw_font_tile(&font_tiles[buf[i]-48+26], (x_pos - strlen(buf) + i + 1) * 8, (y_pos * 8), font_color);
        }
    }
}


void Status::loadTiles()
{
    uint16 num_tiles;
    mTiles = load_tiles("STATUS.MNI", TileType::SOLID, &num_tiles);
    gLogging << "Loading " << num_tiles << "status tiles." << CLogFile::endl;
}

void Status::init()
{
    numStarsCollected = 0;
}

void Status::initPanel()
{
    video_fill_screen_with_black();
    displayBox();
    addToScoreUpdateOnDisplay(0);
    updateHealthBarDisplay();
    displayNumStarsCollected();
    displayNumBombsLeft();
}


void Status::displayBox()
{
    if(mTiles.empty())
        return;

    if(mHidden)
        return;

    const bool modernStyle = gBehaviorEngine.mOptions[GameOption::MODERN].value;

    if(modernStyle)
    {
        auto &overlay = get_vid_surface_overlay();

        const auto color = overlay.mapRGB(0xA0, 0xA0, 0xa0);
        overlay.fill(mHUDDim, GsColor(0x50, 0x50, 0x50));
        overlay.drawFrameRect(mHUDDim, 1, color);
    }
    else
    {
        const auto startY = mHUDDim.pos.y;

        for(int y=0; y < mBarHeight; y++)
        {
            for (int x = 0; x < mBarWidth; x++)
            {
                auto &tile = mTiles.at(x + y * mBarWidth);
                video_draw_tile(&tile,
                                (x + 1) * TILE_WIDTH, (y + 1) * TILE_HEIGHT + startY);
            }
        }
    }
}

void Status::displayEverything()
{
    const auto &optHUD = gBehaviorEngine.mOptions[GameOption::HUD];
    if( !optHUD.value )
        return;

    if(mHidden)
        return;

    GsWeakSurface blitSfc(gVideoDriver.getBlitSurface());

    const auto gameRes = gVideoDriver.getGameResolution();

    const bool modernStyle = gBehaviorEngine.mOptions[GameOption::MODERN].value;
    if(modernStyle)
    {
        mHUDDim.dim = GsVec2D<int>(80, 30);
        mHUDDim.pos.y = 0;
        mHUDDim.pos.x = 0;
        blitSfc.fill(mHUDDim, GsColor(0x50, 0x50, 0x50));
    }
    else
    {
        mHUDDim.dim = GsVec2D<int>(320, 56);
        mHUDDim.pos = gameRes.dim - mHUDDim.dim;
        mHUDDim.pos.x = 0;
        blitSfc.fill(mHUDDim, GsColor(0, 0, 0));
    }



    displayBox();
    addToScoreUpdateOnDisplay(0);
    updateHealthBarDisplay();
    displayNumStarsCollected();
    displayNumBombsLeft();
}

void Status::addToScoreUpdateOnDisplay(const int amount_to_add_low)
{
    if(mHidden)
        return;

    GsVec2D<int> pos(mHUDDim.pos.x/8 + 9, mHUDDim.pos.y/8 + 4);

    const bool modernStyle = gBehaviorEngine.mOptions[GameOption::MODERN].value;
    if(modernStyle)
    {
        pos.x = 6;
        pos.y = 0;
    }

    score += amount_to_add_low;

    const auto &optHUD = gBehaviorEngine.mOptions[GameOption::HUD];
    if( !optHUD.value )
        return;

    display_number(pos.x, pos.y, score);
}

void Status::updateHealthBarDisplay()
{
    if(mHidden)
        return;

    const auto &optHUD = gBehaviorEngine.mOptions[GameOption::HUD];
    if( !optHUD.value )
        return;

    int x = mHUDDim.pos.x/8 + 0x11;
    int y = mHUDDim.pos.y/8 + 0x04;

    const bool modernStyle = gBehaviorEngine.mOptions[GameOption::MODERN].value;
    if(modernStyle)
    {
        x = 5; y = 2;

        for(int i=0;i< num_health_bars;i++)
        {
            if(health - 1 > i)
            {
                video_draw_tile(&font_tiles[95], (x - i) * TILE_WIDTH, y * TILE_HEIGHT + 3);
                video_draw_tile(&font_tiles[96], (x - i) * TILE_WIDTH, (y*TILE_HEIGHT) + 1 + 3);
            }
            else
            {
                video_draw_tile(&font_tiles[9], (x - i) * TILE_WIDTH, y * TILE_HEIGHT + 3);
                video_draw_tile(&font_tiles[8], (x - i) * TILE_WIDTH, (y*TILE_HEIGHT) + 1 + 3);
            }
        }
    }
    else
    {
        for(int i=0;i< num_health_bars;i++)
        {
            if(health - 1 > i)
            {
                video_draw_tile(&font_tiles[95], (x - i) * TILE_WIDTH, y * TILE_HEIGHT);
                video_draw_tile(&font_tiles[96], (x - i) * TILE_WIDTH, (y+1) * TILE_HEIGHT);
            }
            else
            {
                video_draw_tile(&font_tiles[9], (x - i) * TILE_WIDTH, y * TILE_HEIGHT);
                video_draw_tile(&font_tiles[8], (x - i) * TILE_WIDTH, (y+1) * TILE_HEIGHT);
            }
        }
    }

}

void Status::displayNumStarsCollected()
{
    if(mHidden)
        return;

    const auto &optHUD = gBehaviorEngine.mOptions[GameOption::HUD];
    if( !optHUD.value )
        return;

    int x = mHUDDim.pos.x/8 + 0x23;
    int y = mHUDDim.pos.y/8 + 0x04;

    const bool modernStyle = gBehaviorEngine.mOptions[GameOption::MODERN].value;
    if(modernStyle)
    {
        x = 8;
        y = 2;
    }

    display_number(x, y, numStarsCollected);
}


void Status::displayNumBombsLeft()
{
    if(mHidden)
        return;

    int x = mHUDDim.pos.x/8 + 0x18;
    int y = mHUDDim.pos.y/8 + 0x05;

    const bool modernStyle = gBehaviorEngine.mOptions[GameOption::MODERN].value;
    if(modernStyle)
    {
        x = 8;
        y = 0;
    }

    const auto &optHUD = gBehaviorEngine.mOptions[GameOption::HUD];
    if( !optHUD.value )
        return;

    if(!modernStyle)
        video_draw_tile(&font_tiles[97], x * TILE_WIDTH, y * TILE_HEIGHT);
    display_number(x, y, num_bombs);
}

int Status::numStartsCollected() const
{
    return numStarsCollected;
}

void Status::setNumStartsCollected(const int value)
{
    numStarsCollected = value;
}

void Status::addStar()
{
    numStarsCollected++;
}

};

