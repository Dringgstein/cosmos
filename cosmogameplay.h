#ifndef COSMOGAMEPLAY_H
#define COSMOGAMEPLAY_H

#include <base/GsEngine.h>
#include "engine/core/CMap.h"

#include "engine/core/menu/widgets/KeenStyles.h"

class CosmoGameplay : public GsEngine
{
public:
    CosmoGameplay();

    void setMapPtrs();

    void loadCurrentLevel();

    /**
     * @brief start Usually this is started before anything else but still after the construction.
     */
    bool start() override;

    bool load_level_data(const int level_number);

    /**
     * @brief loadLevel Load level of given number. Parts of the code may be passed to
     *                  C code part
     * @param level_number     level to load
     * @return true if everything went fine, otherwise false
     */
    bool loadLevel(const int level_number);

    void loadGameState();

    /**
     * @brief pumpEvent Events like user closes windows or mouse presses are processed here.
     * @param evPtr
     */
    virtual void pumpEvent(const std::shared_ptr<CEvent> &evPtr) override;

    /**
     * @brief handleScrolling
     */
    void handleScrolling();

    /**
     * @brief openMainMenu  Trigger the main menu
     */
    void openMainMenu();

    /**
     * @brief ponder    Logic cycle run usually at 120 LPS
     * @param deltaT    how much time of logic to do
     */
    virtual void ponder(const float deltaT) override;

    /**
     * @brief render Everything that needs to be rendered representing the current state of the object
     */
    virtual void render() override;

private:

    std::shared_ptr<CMap> mpMap;

    Sint8 mSelected_ingame_hint_dialog = -1;

    Style mMenuStyle = Style::VORTICON;

    bool mPaused = false;
};

#endif // COSMOGAMEPLAY_H
