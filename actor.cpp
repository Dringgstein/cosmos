#include <stdio.h>
#include <assert.h>
#include "actor.h"
#include "defines.h"
#include "map.h"
#include "player.h"
#include "tile.h"
#include "util.h"
#include "actor_worktype.h"
#include "effects.h"
#include "video.h"
#include "actor_collision.h"
#include "platforms.h"

#include <base/GsLogging.h>
#include <base/video/CVideoDriver.h>
#include <engine/core/CBehaviorEngine.h>

#include <vector>

//Data

std::vector< ActorData > actors;

uint16 question_mark_code = 0;
bool speech_bubble_hamburger_shown_flag = false;
bool speech_bubble_red_plant_shown_flag = false;
bool speech_bubble_special_switch_shown_flag = false;
bool speech_bubble_clam_trap_shown_flag = false;
bool speech_bubble_silver_robot_shown_flag = false;
bool speech_bubble_purple_boss_shown_flag = false;
bool speech_bubble_pipe_shown_flag = false;
bool speech_bubble_teleporter_shown_flag = false;
bool speech_bubble_hoverboard_shown_flag = false;
bool speech_bubble_rubber_wall_shown_flag = false;
bool speech_bubble_floor_spring_shown_flag = false;

uint8 protection_barrier_active = 0;

uint16 rnd_index = 0; //range 0 - 35

uint8 energy_beam_enabled_flag = 0;

uint16 num_containers = 0;

uint16 num_eye_plants_remaining_to_blow_up = 0;

uint16 num_moving_platforms = 0;

uint16 brightness_effect_enabled_flag = 0;
uint16 obj_switch_151_flag = 0;


uint8 move_platform_flag = 0;
DrawMode actor_tile_display_func_index = DrawMode::NORMAL;


std::vector<Tile> actor_tiles;
std::vector<Sprite> actor_sprites;


const uint16 cosmo_rnd_seed[] = {
        31, 12, 17, 233,
        99, 8, 64, 12,
        199, 49, 5, 6,
        143, 1, 35, 46,
        52, 5, 8, 21,
        44, 8, 3, 77,
        2, 103, 34, 23,
        78, 2, 67, 2,
        79, 46, 1, 98
};



//TODO work out what this should be called.
uint16 cosmo_random()
{
    auto &player = gCosmoPlayer;

    rnd_index++;
    if(rnd_index > 0x23)
    {
        rnd_index = 0;
    }

    return cosmo_rnd_seed[rnd_index] +
            mapwindow_x_offset + mapwindow_y_offset + rnd_index
            + player.xPos() + player.yPos();
}

void actor_add_new(int image_index, int x_pos, int y_pos)
{
    for (decltype(actors.size()) i = 0 ; i < actors.size() ; i++)
    {
        ActorData *actor = &actors[i];
        if (actor->is_deactivated_flag_maybe != 0)
        {
            actor_init(i, image_index, x_pos, y_pos);
            if (image_index == 0x56)
            {
                actor->update_while_off_screen_flag = 1;
            }
            return;
        }
    }

    actors.resize(actors.size()+1);

    ActorData *actor = &actors[actors.size()-1];
    actor_init(actors.size()-1, image_index, x_pos, y_pos);
    if (image_index == 0x56)
    {
        actor->update_while_off_screen_flag = 1;
    }
}

void standard_actor_draw(ActorData *actor)
{
    auto &actorMan = gActorMan;

    if (actor_update_impl(actor, actor->actorInfoIndex,
                          actor->frame_num, actor->x, actor->y) == 0 &&
            actor_tile_display_func_index != DrawMode::INVISIBLE)
    {
        actorMan.display_sprite_maybe(actor->actorInfoIndex,
                                      actor->frame_num,
                                      actor->x, actor->y,
                                      actor_tile_display_func_index);
    }
}

void actor_wt_pink_eye_plant_draw(ActorData *actor)
{
    actor_tile_display_func_index = DrawMode(actor->data_5);
    standard_actor_draw(actor);
}

void actor_wt_robot_with_blue_arc_draw(ActorData *actor)
{
    auto &actorMan = gActorMan;

    static uint16 spark_frame_num = 0;

    if (actor->data_2 == 0)
    {
        actorMan.display_sprite_maybe(0x5a, actor->data_5, actor->x, actor->y, DrawMode::NORMAL);
        spark_frame_num++;
        int si;
        for(si=2; si < 21 && sprite_blocking_check(0, 0x5a, 2, actor->x + 1, actor->y - si) == NOT_BLOCKED; si++)
        {
            actorMan.display_sprite_maybe(0x5a, (spark_frame_num & 3) + 4, actor->x + 1, actor->y - si, DrawMode::NORMAL);
        }

        actorMan.display_sprite_maybe(0x5a, actor->data_5 + 1 + 1, actor->x + 1, actor->y - si + 1, DrawMode::NORMAL);
    }
    standard_actor_draw(actor);
}

void actor_wt_blue_ball_draw(ActorData *actor)
{
    auto &actorMan = gActorMan;

    if(actor->falling_counter != 0)
    {
        if(actor->falling_counter < 2 || actor->falling_counter > 4)
        {
            actorMan.display_sprite_maybe(0x56, 9, actor->x, actor->y - 2, DrawMode::NORMAL);
        }
        else
        {
            actorMan.display_sprite_maybe(0x56, 8, actor->x, actor->y - 2, DrawMode::NORMAL);
        }
    }
    standard_actor_draw(actor);
}



void actor_wt_boss_draw(ActorData *actor)
{

    auto &actorMan = gActorMan;

    const int frame_num = actor->data_5 <= 3 ? 1 : 5;

    if((actor->has_moved_left_flag & 1) == 0 ||
       (actor->has_moved_right_flag & 1) == 0)
    {
       actorMan.display_sprite_maybe(0x66, 0, actor->x, actor->y, DrawMode::NORMAL);
       actorMan.display_sprite_maybe(0x66, frame_num, actor->x, actor->y - 4, DrawMode::NORMAL);
    }
    else
    {
        actorMan.display_sprite_maybe(0x66, 0, actor->x, actor->y, DrawMode::SOLID_WHITE);
        actorMan.display_sprite_maybe(0x66, frame_num, actor->x, actor->y - 4, DrawMode::SOLID_WHITE);
    }

    if (actor->data_5 == 12)
    {
        if((int(actor->y) & 1) == 0)
        {
            actorMan.display_sprite_maybe(0x66, 0, actor->x, actor->y, DrawMode::NORMAL);
            actorMan.display_sprite_maybe(0x66, 5, actor->x, actor->y - 4, DrawMode::NORMAL);
        }
        else
        {
            actorMan.display_sprite_maybe(0x66, 0, actor->x, actor->y, DrawMode::SOLID_WHITE);
            actorMan.display_sprite_maybe(0x66, 5, actor->x, actor->y - 4, DrawMode::SOLID_WHITE);
        }

        standard_actor_draw(actor);
        return;
    }

    if(actor->data_5 < 4)
    {
        actorMan.display_sprite_maybe(0x66, 1, actor->x, actor->y - 4, DrawMode::NORMAL);
        standard_actor_draw(actor);
        return;
    }

    if(actor->x + 1 > gCosmoPlayer.xPos())
    {
        actorMan.display_sprite_maybe(0x66, 2, actor->x + 1, actor->y - 4, DrawMode::NORMAL);
        return;
    }

    if(actor->x + 1 + 1 >= gCosmoPlayer.xPos())
    {
        actorMan.display_sprite_maybe(0x66, 3, actor->x + 1, actor->y - 4, DrawMode::NORMAL);
    }
    else
    {
        actorMan.display_sprite_maybe(0x66, 4, actor->x + 1, actor->y - 4, DrawMode::NORMAL);
    }

    standard_actor_draw(actor);
}

void actor_wt_energy_beam_draw(ActorData *actor)
{
    auto &actorMan = gActorMan;

    actor->data_1 = 0;

    actor_tile_display_func_index = DrawMode::INVISIBLE;

    if (actor->data_5 != 0)
    {
        for(;;actor->data_1++)
        {
            if((map_get_tile_attr(actor->x + actor->data_1, actor->y) & TILE_ATTR_BLOCK_RIGHT) != 0)
            {
                break;
            }

            actorMan.display_sprite_maybe(actor->actorInfoIndex,
                                          actor->data_4,
                                          actor->x + actor->data_1,
                                          actor->y, DrawMode::NORMAL);
        }
    }
    else
    {
        for(;;actor->data_1++)
        {
            if((map_get_tile_attr(actor->x, actor->y - actor->data_1) & TILE_ATTR_BLOCK_UP) != 0)
            {
                break;
            }

            actorMan.display_sprite_maybe(actor->actorInfoIndex,
                                          actor->data_4,
                                          actor->x, actor->y - actor->data_1,
                                          DrawMode::NORMAL);
        }
    }

    standard_actor_draw(actor);
}


void actor_wt_destructable_pedestal_draw(ActorData *actor)
{
    auto &actorMan = gActorMan;

    int i=0;
    for(; i < actor->data_1;i++)
    {
        actorMan.display_sprite_maybe(0xc0, 1,
                                      actor->x, actor->y - i, DrawMode::NORMAL);
    }

    actorMan.display_sprite_maybe(0xc0, 0,
                                  actor->x - 2, actor->y - i, DrawMode::NORMAL);

    actor_tile_display_func_index = DrawMode::INVISIBLE;
    standard_actor_draw(actor);
}

void actor_wt_spring_draw(ActorData *actor)
{
    if(actor->data_5 != 0)
    {
        actor_tile_display_func_index = DrawMode::FLIPPED;
    }
    standard_actor_draw(actor);
}

void actor_wt_two_tons_draw(ActorData *actor)
{
    gActorMan.display_sprite_maybe(0x2d, 4, actor->x - 1, actor->y + 3, DrawMode::NORMAL);
    standard_actor_draw(actor);
}


void actor_wt_speech_bubble_draw(ActorData *actor)
{
    auto &actorMan = gActorMan;

    actor_tile_display_func_index = DrawMode::INVISIBLE;
    if(actor->data_1 != 0x14)
    {
        actorMan.display_sprite_maybe(actor->actorInfoIndex, 0,
                                      gCosmoPlayer.xPos() - 1,
                                      gCosmoPlayer.yPos() - 5,
                                      DrawMode::SOLID);
    }
    standard_actor_draw(actor);
}

void actor_wt_smoke_rising_draw(ActorData *actor)
{
    actor_tile_display_func_index = DrawMode::INVISIBLE;
    standard_actor_draw(actor);
}

void actor_wt_horizontal_flame_draw(ActorData *actor)
{
    auto &actorMan = gActorMan;

    if(actor->data_1 != 0)
    {
        actor_tile_display_func_index = DrawMode::INVISIBLE;
    }

    if (actor_update_impl(actor, actor->actorInfoIndex,
                          actor->frame_num, actor->x, actor->y) == 0 &&
            actor_tile_display_func_index != DrawMode::INVISIBLE)
    {
        actorMan.display_sprite_maybe(actor->actorInfoIndex,
                                      actor->frame_num,
                                      actor->x, actor->y,
                                      actor_tile_display_func_index);
    }

}

void actor_wt_blue_platform_draw(ActorData *actor)
{
    auto &actorMan = gActorMan;

    if(actor->data_1 != 0)
    {
        if (actor->data_1 != 1 || actor->y - 2 != gCosmoPlayer.yPos())
        {
            if (actor->data_1 == 2)
            {
                if (actor->data_2 >= 5 && actor->data_2 < 8)
                {
                    actor_tile_display_func_index = DrawMode::INVISIBLE;
                    actorMan.display_sprite_maybe(0x5b, 1, actor->x - (actor->data_2 - 5), actor->y, DrawMode::NORMAL);
                    actorMan.display_sprite_maybe(0x5b, 2, actor->x + actor->data_2 - 3, actor->y, DrawMode::NORMAL);
                }
            }
        }
    }

    if(actor->data_1 != 3)
    {
        standard_actor_draw(actor);
        return;
    }
    actor_tile_display_func_index = DrawMode::INVISIBLE;

    actorMan.display_sprite_maybe(0x5b, 1, actor->x + actor->data_2 - 2, actor->y, DrawMode::NORMAL);
    actorMan.display_sprite_maybe(0x5b, 2, actor->x + 4 - actor->data_2, actor->y, DrawMode::NORMAL);

    if(actor->data_2 == 3)
    {
        actor_tile_display_func_index = DrawMode::NORMAL;
    }

    standard_actor_draw(actor);
}


void actor_wt_bonus_item_draw(ActorData *actor)
{
    auto &actorMan = gActorMan;

    if(actor->data_1 != 0)
    {
        actor_tile_display_func_index = DrawMode::FLIPPED;
    }

    if (actor_update_impl(actor, actor->actorInfoIndex,
                          actor->frame_num, actor->x, actor->y) == 0 &&
            actor_tile_display_func_index != DrawMode::INVISIBLE)
    {
        actorMan.display_sprite_maybe(actor->actorInfoIndex,
                                      actor->frame_num,
                                      actor->x, actor->y,
                                      actor_tile_display_func_index);
    }
}

void actor_wt_teleporter_draw(ActorData *actor)
{
    auto &actorMan = gActorMan;

    actor_tile_display_func_index = DrawMode::INVISIBLE;
    if(teleporter_counter == 0 || (cosmo_rand() & 1) == 0)
    {
        actorMan.display_sprite_maybe(0x6b, 0, actor->x, actor->y, DrawMode::NORMAL);
    }
    else
    {

        actorMan.display_sprite_maybe(0x6b, 0, actor->x, actor->y, DrawMode::SOLID_WHITE);
    }
    if((cosmo_random() & 1) != 0)
    {
        actorMan.display_sprite_maybe(0x6b, (cosmo_rand() & 1) + 1, actor->x, actor->y, DrawMode::NORMAL);
    }


    if (actor_update_impl(actor, actor->actorInfoIndex,
                          actor->frame_num, actor->x, actor->y) == 0 &&
            actor_tile_display_func_index != DrawMode::INVISIBLE)
    {
        actorMan.display_sprite_maybe(actor->actorInfoIndex,
                                      actor->frame_num,
                                      actor->x, actor->y,
                                      actor_tile_display_func_index);
    }
}

void actor_wt_hoverboard_draw(ActorData *actor)
{
    const auto &player = gCosmoPlayer;
    if(player_hoverboard_counter != 0)
    {
        auto &actorMan = gActorMan;
        const auto localPlayerScroll = player.getLocalMapScroll();

        if (actor_update_impl(actor, actor->actorInfoIndex,
                              actor->frame_num, actor->x, actor->y) == 0 &&
                actor_tile_display_func_index != DrawMode::INVISIBLE)
        {
            actorMan.display_sprite_maybe(actor->actorInfoIndex,
                                          actor->frame_num,
                                          actor->x, actor->y,
                                          actor_tile_display_func_index,
                                          localPlayerScroll);
        }

        actor->x = player.xPos();
        actor->y = player.yPos() + 1;
        return;
    }

    standard_actor_draw(actor);
}

const static uint8 alien_eating_space_plant_f_num_tbl[] = {5, 6, 7, 8};

void actor_wt_alien_eating_space_plant_draw(ActorData *actor)
{
    auto &actorMan = gActorMan;

    if(actor->frame_num == 0 && actor->data_5 == 0)
    {
        actorMan.display_sprite_maybe(0xba, alien_eating_space_plant_f_num_tbl[actor->data_1 & 3], actor->x + 1 + 1, actor->y - 3, DrawMode::NORMAL);
    }

    standard_actor_draw(actor);
}

void actor_wt_floating_score_effect_draw(ActorData *actor)
{
    const sint8 score_effect_x_tbl[] = {
            -2, -1, 0, 1, 2, 2, 1, 0, -1, -2
    };

    auto &actorMan = gActorMan;

    actor->data_1++;

    if(actor->data_1 > (0x1f)*4)
    {
        actor->y = actor->y - 0.25f;
        actor->x = actor->x + float(score_effect_x_tbl[(actor->data_1 - 32) % 10])*0.25f;
    }

    if(actor->data_1 < 4*4)
    {
        actor->y = actor->y - 0.25f;
    }

    if(actor->data_1 == (0x64)*4 || is_sprite_on_screen(actor->actorInfoIndex, actor->frame_num, actor->x, actor->y) == 0)
    {
        actor->is_deactivated_flag_maybe = 1;
    }


    if (actor_update_impl(actor, actor->actorInfoIndex,
                          actor->frame_num, actor->x, actor->y) == 0 &&
            actor_tile_display_func_index != DrawMode::INVISIBLE)
    {
        actorMan.display_sprite_maybe(actor->actorInfoIndex,
                                      actor->frame_num,
                                      actor->x, actor->y,
                                      actor_tile_display_func_index);
    }
}

const static uint8 jaws_and_tongue_frame_num_tbl[] = {2, 3, 4, 3};
void actor_wt_jaws_and_tongue_draw(ActorData *actor)
{
    auto &actorMan = gActorMan;

    if(actor->frame_num != 0)
    {
        actorMan.display_sprite_maybe(0x95, jaws_and_tongue_frame_num_tbl[actor->data_3 & 3], actor->x + 6 - actor->data_5, actor->y - 3, DrawMode::NORMAL);
    }

    actor_tile_display_func_index = DrawMode::INVISIBLE;

    actorMan.display_sprite_maybe(actor->actorInfoIndex, 1, actor->x, actor->y, DrawMode::NORMAL);

    if(actor->data_5 != 0 && actor->data_5 < 4)
        return;

    actorMan.display_sprite_maybe(actor->actorInfoIndex, 0, actor->x, actor->y - 1 - actor->data_5, DrawMode::NORMAL);

    standard_actor_draw(actor);
}



void ActorData::init_struct(int actor_init_cur_actor_num,
                       int actorInfoIndex,
                       int x,
                       int y,
                       int update_while_offscreen,
                       int can_update_if_goes_offscreen,
                       int can_fall_down,
                       int is_non_blocking,
                       void (*update_function)(ActorData *),
                       int data_1,
                       int data_2,
                       int data_3,
                       int data_4,
                       int data_5)
{
    if(data_2 == 0x1e || data_2 == 0xb9)
    {
        num_containers++;
    }

    ActorData *actor = &actors[actor_init_cur_actor_num];

    actor->actorInfoIndex = actorInfoIndex;
    actor->frame_num = 0;
    actor->x = x;
    actor->y = y;
    actor->update_while_off_screen_flag = update_while_offscreen;
    actor->can_update_if_goes_off_screen_flag = can_update_if_goes_offscreen;
    actor->can_fall_down_flag = can_fall_down;
    actor->non_blocking_flag_maybe = is_non_blocking;
    actor->is_deactivated_flag_maybe = 0;
    actor->update_function = update_function;
    actor->has_moved_left_flag = 0;
    actor->has_moved_right_flag = 0;
    actor->falling_counter = 0;
    actor->data_1 = data_1;
    actor->data_2 = data_2;
    actor->data_3 = data_3;
    actor->data_4 = data_4;
    actor->data_5 = data_5;
    actor->count_down_timer = 0;

    draw_function = standard_actor_draw;

    return;
}

int actor_init(const int actor_num, int image_index, int x_pos, int y_pos)
{
    auto &actor = actors[actor_num];

    const Difficulty difficulty = gBehaviorEngine.mDifficulty;


    switch (image_index)
    {
        case 0:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0, 0xb9, 0, 0, 0);
            break;

        case 1:
            actor.init_struct(actor_num, 1, x_pos, y_pos, 0, 0, 0, 0, actor_wt_bonus_item, 0, 0, 0, 0, 4);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 2:
            actor.init_struct(actor_num, 2, x_pos, y_pos, 0, 1, 1, 0, actor_wt_spring, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_spring_draw;
            break;

        case 3:
            actor.init_struct(actor_num, 3, x_pos, y_pos, 0, 1, 0, 0, actor_wt_extending_arrow, 0, 0, 0, 0, 0);
            break;

        case 4:
            actor.init_struct(actor_num, 4, x_pos - 4, y_pos, 0, 1, 0, 0, actor_wt_extending_arrow, 0, 0, 0, 0, 1);
            break;

        case 5:
            actor.init_struct(actor_num, 5, x_pos, y_pos, 1, 0, 0, 0, actor_wt_plasma_fireball, 0, x_pos, y_pos, 0, 0);
            break;

        case 6:
            actor.init_struct(actor_num, 5, x_pos - 1, y_pos, 1, 0, 0, 0, actor_wt_plasma_fireball, 0, x_pos - 1, y_pos, 0, 1);
            break;

        case 7:
            actor.init_struct(actor_num, 7, x_pos, y_pos + 1, 0, 0, 0, 0, actor_wt_switch, 0, 0, 0, 0, 11);
            break;

        case 8:
            actor.init_struct(actor_num, 8, x_pos, y_pos + 1, 0, 0, 0, 0, actor_wt_switch, 0, 0, 0, 0, 12);
            break;

        case 9:
            actor.init_struct(actor_num, 9, x_pos, y_pos + 1, 0, 0, 0, 0, actor_wt_switch, 0, 0, 0, 0, 13);
            break;

        case 10:
            actor.init_struct(actor_num, 10, x_pos, y_pos + 1, 0, 0, 0, 0, actor_wt_switch, 0, 0, 0, 0, 14);
            break;

        case 11:
            actor.init_struct(actor_num, 11, x_pos, y_pos, 0, 0, 0, 0, actor_wt_door, 0, 0, 0, 0, 0);
            break;

        case 12:
            actor.init_struct(actor_num, 12, x_pos, y_pos, 0, 0, 0, 0, actor_wt_door, 0, 0, 0, 0, 0);
            break;

        case 13:
            actor.init_struct(actor_num, 13, x_pos, y_pos, 0, 0, 0, 0, actor_wt_door, 0, 0, 0, 0, 0);
            break;

        case 14:
            actor.init_struct(actor_num, 14, x_pos, y_pos, 0, 0, 0, 0, actor_wt_door, 0, 0, 0, 0, 0);
            break;

        case 16:
            actor.init_struct(actor_num, 0x10, x_pos, y_pos, 1, 0, 0, 0, actor_wt_blue_mobile_trampoline_car, 0, 0, 0, 0, 0);
            break;

        case 17:
            actor.init_struct(actor_num, 0x11, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 18:
            actor.init_struct(actor_num, 0x12, x_pos, y_pos, 0, 0, 0, 0, actor_wt_retracting_spikes, 1, 0, 0, 0, 0);
            break;

        case 20:
            actor.init_struct(actor_num, 0x14, x_pos, y_pos, 0, 1, 0, 1, actor_wt_big_saw_blade, 0, 0, 0, 0, 0);
            break;

        case 22:
            actor.init_struct(actor_num, 0x14, x_pos, y_pos, 1, 0, 0, 1, actor_wt_robotic_spike_ground, 0, 0, 0, 0, 1);
            break;

        case 24:
            actor.init_struct(actor_num, 0x18, x_pos, y_pos, 1, 0, 1, 1, actor_wt_bomb, 0, 0, 0, 0, 0);
            break;

        case 25:
            actor.init_struct(actor_num, 0x19, x_pos, y_pos, 0, 1, 1, 1, actor_wt_green_pruny_cabbage_ball, 1, 0, 0, 0, 0);
            break;

        case 28:
            actor.init_struct(actor_num, 0x1c, x_pos, y_pos, 1, 0, 1, 0, actor_wt_bonus_item, 0, 0, 0, 1, 6);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 29:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x1c, 0x1e, 0, 0, 0);
            break;

        case 31:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x20, 0xb9, 0, 0, 0);
            break;

        case 32:
            actor.init_struct(actor_num, 0x20, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 33:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x22, 0xb9, 0, 0, 0);
            break;

        case 34:
            actor.init_struct(actor_num, 0x22, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 35:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x24, 0x1e, 0, 0, 0);
            break;

        case 36:
            actor.init_struct(actor_num, 0x24, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 37:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x26, 0x1e, 0, 0, 0);
            break;

        case 38:
            actor.init_struct(actor_num, 0x26, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 39:
            actor.init_struct(actor_num, 0x27, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 40:
            actor.init_struct(actor_num, 0x29, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 41:
            actor.init_struct(actor_num, 0x29, x_pos, y_pos, 0, 0, 0, 0, actor_wt_spear_vertical, 0, 0, 0, 0, 0);
            break;

        case 42:
            actor.init_struct(actor_num, 0x2b, x_pos, y_pos + 1, 0, 0, 0, 0, actor_wt_acid, 0, 0, 0, 0, 0);
            break;

        case 43:
            actor.init_struct(actor_num, 0x2b, x_pos, y_pos + 1, 0, 1, 0, 0, actor_wt_acid, x_pos, y_pos + 1, 0, 0, 1);
            break;

        case 44:
            actor.init_struct(actor_num, 0x2c, x_pos, y_pos, 1, 0, 0, 0, actor_wt_plasma_energy_blue_sprite, 0, 0, 0, 0, 0);
            break;

        case 45:
            actor.init_struct(actor_num, 0x2d, x_pos, y_pos, 0, 1, 0, 0, actor_wt_two_tons, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_two_tons_draw;
            break;

        case 46:
            actor.init_struct(actor_num, 0x2e, x_pos, y_pos, 0, 1, 0, 0, actor_wt_jumping_bullet_head, 0, 0, 0, 0, 0);
            break;

        case 47:
            actor.init_struct(actor_num, 0x2f, x_pos, y_pos, 0, 1, 0, 0, actor_wt_stone_head, 0, 0, 0, 0, 0);
            break;

        case 48:
            actor.init_struct(actor_num, 0x31, x_pos, y_pos + 1, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 49:
            actor.init_struct(actor_num, 0x31, x_pos, y_pos + 1, 0, 1, 0, 1, actor_wt_big_yellow_spike, 0, 0, 0, 0, 0);
            break;

        case 50:
            actor.init_struct(actor_num, 0x31, x_pos, y_pos, 0, 0, 0, 0, actor_wt_big_yellow_spike, 0, 0, 0, 0, 1);
            break;

        case 51:
            actor.init_struct(actor_num, 0x33, x_pos, y_pos, 0, 1, 0, 0, actor_wt_ghost, 0, 0, 0, 0, 4);
            break;

        case 54:
            actor.init_struct(actor_num, 0x36, x_pos, y_pos, 0, 0, 0, 1, actor_wt_angry_moon, 0, 0, 0, 0, 4);
            break;

        case 55:
            actor.init_struct(actor_num, 0x37, x_pos, y_pos, 0, 0, 0, 0, actor_wt_small_red_plant, 0, 0, 0, 0, 0);
            break;

        case 56:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x39, 0x1e, 0, 0, 0);
            break;

        case 57:
            actor.init_struct(actor_num, 0x39, x_pos, y_pos, 1, 0, 1, 0, actor_wt_bonus_bomb, 0, 0, 0, 0, 0);
            break;

        case 58:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 1, actor_wt_container, 2, 0x1e, 0, 0, 0);
            break;

        case 59:
            actor.init_struct(actor_num, 0x3c, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0x3b);
            move_platform_flag = 0;
            break;

        case 61:
            actor.init_struct(actor_num, 0x3c, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0x3d);
            break;

        case 62:
            actor.init_struct(actor_num, 0x3e, x_pos, y_pos, 1, 0, 0, 0, actor_wt_question_mark_block, 0, 0, 0, 0, 0);
            question_mark_code = 0;
            break;

        case 65:
            actor.init_struct(actor_num, 0x41, x_pos, y_pos, 0, 1, 1, 0, actor_wt_mini_ghost, 0, 0, 0, 0, 0);
            break;

        case 66:
            actor.init_struct(actor_num, 0x44, x_pos, y_pos, 1, 0, 0, 1, actor_wt_projectile_flashing_ball, 0, 0, 0, 0, 1);
            break;

        case 67:
            actor.init_struct(actor_num, 0x44, x_pos, y_pos, 1, 0, 0, 1, actor_wt_projectile_flashing_ball, 0, 0, 0, 0, 3);
            break;

        case 68:
            actor.init_struct(actor_num, 0x44, x_pos, y_pos, 1, 0, 0, 1, actor_wt_projectile_flashing_ball, 0, 0, 0, 0, 2);
            break;

        case 69:
            actor.init_struct(actor_num, 0x45, x_pos, y_pos, 0, 1, 0, 0, actor_wt_green_roamer_worm, 0, 3, 0, 0, 0);
            break;

        case 70:
            actor.init_struct(actor_num, 0x46, x_pos, y_pos, 0, 0, 0, 0, actor_wt_pipe_transit_direction, 0, 0, 0, 0, 0);
            break;

        case 71:
            actor.init_struct(actor_num, 0x47, x_pos, y_pos, 0, 0, 0, 0, actor_wt_pipe_transit_direction, 0, 0, 0, 0, 0);
            break;

        case 72:
            actor.init_struct(actor_num, 0x48, x_pos, y_pos, 0, 1, 0, 0, actor_wt_pipe_transit_direction, 0, 0, 0, 0, 0);
            break;

        case 73:
            actor.init_struct(actor_num, 0x49, x_pos, y_pos, 0, 1, 0, 0, actor_wt_pipe_transit_direction, 0, 0, 0, 0, 0);
            break;

        case 74:
            actor.init_struct(actor_num, 0x4b, x_pos, y_pos, 0, 0, 0, 0, actor_wt_egg_head, 0, 0, 0, 0, 0);
            break;

        case 75:
            actor.init_struct(actor_num, 0x4b, x_pos, y_pos, 0, 0, 0, 0, actor_wt_egg_head, 0, 0, 0, 0, 1);
            break;

        case 78:
            actor.init_struct(actor_num, 0x4e, x_pos, y_pos, 0, 1, 0, 0, actor_wt_robotic_spike_ground, 8, 0, 0, 0, 1);
            break;

        case 80:
            actor.init_struct(actor_num, 0x50, x_pos, y_pos + 1 + 1, 0, 1, 0, 0, actor_wt_robotic_spike_ceiling, 0, 0, 0, 0, 0);
            break;

        case 81:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x52, 0xb9, 0, 0, 0);
            break;

        case 82:
            actor.init_struct(actor_num, 0x52, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 83:
            actor.init_struct(actor_num, 0x53, x_pos, y_pos, 0, 0, 0, 0, actor_wt_red_blue_plant, 0, 0, 0, 0, 0);
            break;

        case 84:
            actor.init_struct(actor_num, 0x53, x_pos, y_pos + 1 + 1, 0, 0, 0, 0, actor_wt_red_blue_plant, 0, 0, 0, 0, 4);
            break;

        case 85:
            actor.init_struct(actor_num, 0x55, x_pos, y_pos + 1 + 1, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 86:
            actor.init_struct(actor_num, 0x56, x_pos, y_pos, 0, 1, 1, 1, actor_wt_blue_ball, 0, 0x14, 0, 0, 2);
            actor.draw_function = actor_wt_blue_ball_draw;
            break;

        case 87:
            actor.init_struct(actor_num, 0x57, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 88:
            actor.init_struct(actor_num, 0x58, x_pos, y_pos, 0, 0, 0, 0, actor_wt_retracting_spikes, 1, 0, 0, 0, 0);
            break;

        case 89:
            actor.init_struct(actor_num, 0x59, x_pos - 3, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 90:
            actor.init_struct(actor_num, 0x5a, x_pos, y_pos, 1, 0, 0, 0, actor_wt_robot_with_blue_arc, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_robot_with_blue_arc_draw;
            break;

        case 91:
            actor.init_struct(actor_num, 0x5b, x_pos, y_pos, 1, 0, 0, 0, actor_wt_blue_platform, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_blue_platform_draw;
            break;

        case 92:
            actor.init_struct(actor_num, 0x5c, x_pos, y_pos, 0, 1, 0, 0, actor_wt_spark, 0, 0, 0, 0, 0);
            break;

        case 93:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x5e, 0xb9, 0, 0, 0);
            break;

        case 94:
            actor.init_struct(actor_num, 0x5e, x_pos, y_pos, 1, 0, 1, 0, actor_wt_bonus_item, 0, 0, 0, 1, 2);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 95:
            // Eye Plant
            actor.init_struct(actor_num, 0x5f, x_pos, y_pos, 0, 1, 0, 0, actor_wt_pink_eye_plant, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_pink_eye_plant_draw;
            if(num_eye_plants_remaining_to_blow_up < 15)
            {
                num_eye_plants_remaining_to_blow_up++;
            }
            break;

        case 96:
            actor.init_struct(actor_num, 0x5f, x_pos, y_pos + 1, 0, 0, 0, 0, actor_wt_pink_eye_plant, 0, 0, 0, 0, 4);
            actor.draw_function = actor_wt_pink_eye_plant_draw;
            break;

        case 100:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xfb, 0x1e, 0, 0, 0);
            break;

        case 101:
            actor.init_struct(actor_num, 0x65, x_pos, y_pos, 0, 1, 0, 0, actor_wt_big_red_jumper, 0, 0, 0, 0, 7);
            break;

        case 102:
            actor.init_struct(actor_num, 0x66, x_pos, y_pos, 0, 1, 0, 0, actor_wt_boss_purple, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_boss_draw;
            break;

        case 104:
            actor.init_struct(actor_num, 0x69, x_pos - 1, y_pos + 1 + 1, 1, 0, 0, 0, actor_wt_pneumatic_pipe, 0, 0, 0, 0, 0);
            break;

        case 105:
            actor.init_struct(actor_num, 0x69, x_pos - 1, y_pos + 1 + 1, 0, 1, 0, 0, actor_wt_pneumatic_pipe, 0, 1, 0, 0, 0);
            break;

        case 106:
            actor.init_struct(actor_num, 0x6a, x_pos, y_pos, 0, 1, 0, 0, actor_wt_suction_cup_alien_enemy, 0, 0, 0, 0, 0);
            break;

        case 107:
            actor.init_struct(actor_num, 0x6c, x_pos, y_pos, 1, 0, 0, 0, actor_wt_teleporter, 0, 0, 0, 0, 2);
            actor.draw_function = actor_wt_teleporter_draw;
            break;

        case 108:
            actor.init_struct(actor_num, 0x6c, x_pos, y_pos, 1, 0, 0, 0, actor_wt_teleporter, 0, 0, 0, 0, 1);
            actor.draw_function = actor_wt_teleporter_draw;
            break;

        case 109:
            actor.init_struct(actor_num, 0x44, x_pos, y_pos, 1, 0, 0, 0, actor_wt_projectile_flashing_ball, 0, 0, 0, 0, 0);
            break;

        case 110:
            actor.init_struct(actor_num, 0x44, x_pos, y_pos, 1, 0, 0, 0, actor_wt_projectile_flashing_ball, 0, 0, 0, 0, 4);
            break;

        case 112:
            actor.init_struct(actor_num, 0x70, x_pos - 3, y_pos, 0, 0, 0, 0, actor_wt_cyan_spitting_plant, 0, 0, 0, 0, 2);
            break;

        case 111:
            actor.init_struct(actor_num, 0x6f, x_pos, y_pos, 0, 0, 0, 0, actor_wt_cyan_spitting_plant, 0, 0, 0, 0, 3);
            break;

        case 113:
            actor.init_struct(actor_num, 0x71, x_pos, y_pos, 0, 1, 0, 0, actor_wt_blue_turret_alien, 0, 10, x_pos, 0, 3);
            break;

        case 114:
            actor.init_struct(actor_num, 0x72, x_pos, y_pos, 0, 1, 0, 0, actor_wt_hoverboard, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_hoverboard_draw;
            break;

        case 118:
            actor.init_struct(actor_num, 0x76, x_pos, y_pos, 0, 1, 1, 0, actor_wt_red_chomper_alien, 0, 0, 0, 0, 0);
            break;

        case 120:
            actor.init_struct(actor_num, 0x3c, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0x78);
            brightness_effect_enabled_flag = 0;
            obj_switch_151_flag = 1;
            break;

        case 121:
            actor.init_struct(actor_num, 0x3c, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0x79);
            break;

        case 122:
            actor.init_struct(actor_num, 0x7a, x_pos, y_pos, 1, 0, 0, 0, actor_wt_energy_beam, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_energy_beam_draw;
            break;

        case 123:
            actor.init_struct(actor_num, 0x7b, x_pos, y_pos, 1, 0, 0, 0, actor_wt_energy_beam, 0, 0, 0, 0, 1);
            actor.draw_function = actor_wt_energy_beam_draw;
            break;

        case 124:
            actor.init_struct(actor_num, 0x7c, x_pos, y_pos, 0, 1, 1, 0, actor_wt_pink_slug, 0, 0, 0, 0, 0);
            break;

        case 125:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 126:
            actor.init_struct(actor_num, 0x7e, x_pos, y_pos, 0, 1, 0, 0, actor_wt_silver_robot, 0, 0, 0, 0, 4);
            break;

        case 127:
            actor.init_struct(actor_num, 0x7f, x_pos, y_pos, 0, 1, 0, 0, actor_wt_security_robot, 0, 0, 0, 0, 4);
            break;

        case 128:
            actor.init_struct(actor_num, 0x80, x_pos, y_pos, 0, 0, 1, 0, actor_wt_159_unknown, 0, 0, 0, 0, 3);
            break;

        case 129:
            actor.init_struct(actor_num, 0x81, x_pos, y_pos, 0, 1, 0, 0, actor_wt_dragonfly, 0, 0, 0, 0, 0);
            break;

        case 130:
            actor.init_struct(actor_num, 0x82, x_pos, y_pos, 1, 0, 0, 0, actor_wt_crate_bomb_box, 0, 0, 0, 0, (cosmo_random() % 0x14) * 5 + 0x32);
            break;

        case 134:
            actor.init_struct(actor_num, 0x86, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 135:
            actor.init_struct(actor_num, 0x87, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 136:
            actor.init_struct(actor_num, 0x88, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 137:
            actor.init_struct(actor_num, 0x89, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 138:
            actor.init_struct(actor_num, 0x8a, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 139:
            actor.init_struct(actor_num, 0x8b, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 140:
            actor.init_struct(actor_num, 0x8c, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 141:
            actor.init_struct(actor_num, 0x8d, x_pos, y_pos + 1 + 1, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 142:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x86, 0x1e, 0, 0, 0);
            break;

        case 52:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x87, 0xb9, 0, 0, 0);
            break;

        case 53:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x88, 0xb9, 0, 0, 0);
            break;

        case 119:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x89, 0xb9, 0, 0, 0);
            break;

        case 115:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x8a, 0xb9, 0, 0, 0);
            break;

        case 116:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 0, 0, actor_wt_container, 0x8b, 0xb9, 0, 0, 0);
            break;

        case 117:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x8c, 0x1e, 0, 0, 0);
            break;

        case 143:
            actor.init_struct(actor_num, 0x8f, x_pos, y_pos, 0, 0, 0, 0, actor_wt_satellite, 0, 0, 0, 0, 0);
            break;

        case 145:
            actor.init_struct(actor_num, 0x91, x_pos, y_pos + 7, 0, 1, 0, 0, actor_wt_green_plant, 5, 0, 0, 7, 0);
            break;

        case 146:
            actor.init_struct(actor_num, 0x92, x_pos, y_pos + 1 + 1, 1, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 147:
            actor.init_struct(actor_num, 0x93, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 148:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x93, 0xb9, 0, 0, 0);
            break;

        case 149:
            actor.init_struct(actor_num, 0x95, x_pos - 4, y_pos, 0, 1, 0, 0, actor_wt_jaws_and_tongue, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_jaws_and_tongue_draw;
            break;

        case 150:
            actor.init_struct(actor_num, 0x96, x_pos, y_pos, 1, 0, 0, 0, actor_wt_invisible_exit_marker_right, 0, 0, 0, 0, 0);
            break;

        case 151:
            actor.init_struct(actor_num, 0x97, x_pos, y_pos, 0, 0, 0, 0, actor_wt_small_flame, 0, 0, 0, 0, 0);
            break;

        case 153:
            actor.init_struct(actor_num, 0x99, x_pos, y_pos, 1, 0, 1, 0, actor_wt_bonus_item, 0, 0, 0, 0, 4);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 154:
            actor.init_struct(actor_num, 0x9a, x_pos, y_pos, 1, 0, 1, 0, actor_wt_bonus_item, 0, 0, 0, 0, 5);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 155:
            actor.init_struct(actor_num, 0x9b, x_pos, y_pos, 1, 0, 1, 0, actor_wt_bonus_item, 0, 0, 0, 0, 6);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 156:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x99, 0x1e, 0, 0, 0);
            break;

        case 157:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x9a, 0x1e, 0, 0, 0);
            break;

        case 158:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0x9b, 0x1e, 0, 0, 0);
            break;

        case 159:
            actor.init_struct(actor_num, 0x20, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 160:
            actor.init_struct(actor_num, 0x22, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 161:
            actor.init_struct(actor_num, 0x24, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 162:
            actor.init_struct(actor_num, 0xa2, x_pos, y_pos, 0, 0, 0, 0, actor_wt_clam_trap, 0, 0, 0, 0, 0);
            break;

        case 163:
            actor.init_struct(actor_num, 0xa3, x_pos, y_pos, 0, 1, 0, 0, actor_wt_blue_cube_platform, 0, 0, 0, 0, 0);
            break;

        case 164:
        case 165:
        case 166:
            actor.init_struct(actor_num, 0xa4, x_pos, y_pos, 1, 0, 0, 0, actor_wt_short_dialog, image_index, 0, 0, 0, 0);
            break;

        case 168:
            actor.init_struct(actor_num, 0xa8, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 167:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xa8, 0xb9, 0, 0, 0);
            break;

        case 170:
            actor.init_struct(actor_num, 0xaa, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 169:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xaa, 0xb9, 0, 0, 0);
            break;

        case 172:
            actor.init_struct(actor_num, 0xac, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 171:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xac, 0xb9, 0, 0, 0);
            break;

        case 174:
            actor.init_struct(actor_num, 0xae, x_pos, y_pos, 1, 0, 1, 0, actor_wt_bonus_item, 0, 0, 0, 0, 5);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 173:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xae, 0x1e, 0, 0, 0);
            break;

        case 176:
            actor.init_struct(actor_num, 0xb0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_bonus_item, 0, 0, 0, 0, 4);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 175:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xb0, 0x1e, 0, 0, 0);
            break;

        case 177:
            actor.init_struct(actor_num, 0xb1, x_pos, y_pos, 0, 1, 0, 0, actor_wt_floating_score_effect, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_floating_score_effect_draw;
            break;

        case 178:
            actor.init_struct(actor_num, 0xb2, x_pos, y_pos, 0, 1, 0, 0, actor_wt_floating_score_effect, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_floating_score_effect_draw;
            break;

        case 179:
            actor.init_struct(actor_num, 0xb3, x_pos, y_pos, 0, 1, 0, 0, actor_wt_floating_score_effect, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_floating_score_effect_draw;
            break;

        case 180:
            actor.init_struct(actor_num, 0xb4, x_pos, y_pos, 0, 1, 0, 0, actor_wt_floating_score_effect, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_floating_score_effect_draw;
            break;

        case 181:
            actor.init_struct(actor_num, 0xb5, x_pos, y_pos, 0, 1, 0, 0, actor_wt_floating_score_effect, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_floating_score_effect_draw;
            break;

        case 182:
            actor.init_struct(actor_num, 0xb6, x_pos, y_pos, 0, 1, 0, 0, actor_wt_floating_score_effect, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_floating_score_effect_draw;
            break;

        case 183:
            actor.init_struct(actor_num, 0xb7, x_pos, y_pos, 0, 1, 0, 0, actor_wt_floating_score_effect, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_floating_score_effect_draw;
            break;

        case 184:
            actor.init_struct(actor_num, 0xb8, x_pos, y_pos, 0, 1, 0, 0, actor_wt_floating_score_effect, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_floating_score_effect_draw;
            break;

        case 186:
            actor.init_struct(actor_num, 0xba, x_pos, y_pos, 0, 1, 0, 0, actor_wt_alien_eating_space_plant, 0, 0, 0x1e, 0, 0);
            actor.draw_function = actor_wt_alien_eating_space_plant_draw;
            break;

        case 187:
            if( difficulty > EASY )
            {
                actor.init_struct(actor_num, 0xbb, x_pos, y_pos, 0, 1, 0, 0, actor_wt_blue_bird, 0, 0, 0, 0, 0);
            }
            else
            {
                return 0;
            }
            break;

        case 188:
            actor.init_struct(actor_num, 0xbc, x_pos, y_pos, 0, 1, 0, 0, actor_wt_rocket, 0x3c, 10, 0, 0, 0);
            break;

        case 189:
            actor.init_struct(actor_num, 0xbd, x_pos, y_pos, 0, 0, 0, 0, actor_wt_bonus_item, 0, 0, 0, 0, 4);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 190:
            actor.init_struct(actor_num, 0xc0, x_pos, y_pos, 1, 0, 0, 0, actor_wt_destructable_pedestal, 13, 0, 0, 0, 0);
            actor.draw_function = actor_wt_destructable_pedestal_draw;
            break;

        case 191:
            actor.init_struct(actor_num, 0xc0, x_pos, y_pos, 1, 0, 0, 0, actor_wt_destructable_pedestal, 0x13, 0, 0, 0, 0);
            actor.draw_function = actor_wt_destructable_pedestal_draw;
            break;

        case 192:
            actor.init_struct(actor_num, 0xc0, x_pos, y_pos, 1, 0, 0, 0, actor_wt_destructable_pedestal, 0x19, 0, 0, 0, 0);
            actor.draw_function = actor_wt_destructable_pedestal_draw;
            break;

        case 201:
            actor.init_struct(actor_num, 0xc9, x_pos, y_pos, 0, 0, 0, 0, actor_wt_protection_barrier, 0, 0, 0, 0, 0);
            break;

        case 193:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xc2, 0x1e, 0, 0, 0);
            break;

        case 194:
            actor.init_struct(actor_num, 0xc2, x_pos, y_pos, 1, 0, 1, 0, actor_wt_bonus_item, 3, 2, 0, 0, 1);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 195:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xc4, 0x1e, 0, 0, 0);
            break;

        case 196:
            actor.init_struct(actor_num, 0xc4, x_pos, y_pos, 1, 0, 1, 0, actor_wt_bonus_item, 2, 2, 0, 0, 1);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 197:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xc6, 0x1e, 0, 0, 0);
            break;

        case 198:
            actor.init_struct(actor_num, 0xc6, x_pos, y_pos, 1, 0, 1, 0, actor_wt_bonus_item, 2, 2, 0, 0, 1);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 199:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xc8, 0x1e, 0, 0, 0);
            break;

        case 200:
            actor.init_struct(actor_num, 0xc8, x_pos, y_pos, 1, 0, 1, 0, actor_wt_bonus_item, 2, 2, 0, 0, 1);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 202:
            actor.init_struct(actor_num, 0xca, x_pos, y_pos + 1 + 1, 0, 0, 0, 0, actor_wt_bonus_item, 0, 0, 0, 0, 4);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 203:
            actor.init_struct(actor_num, 0x6c, x_pos, y_pos, 1, 0, 0, 0, actor_wt_teleporter, 0, 0, 0, 0, 3);
            actor.draw_function = actor_wt_teleporter_draw;
            break;

        case 204:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 1);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 205:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 2);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 206:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 3);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 207:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 4);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 208:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 5);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 209:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 6);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 210:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 7);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 211:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 8);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 212:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 9);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 63:
            actor.init_struct(actor_num, 0x3f, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 64:
            actor.init_struct(actor_num, 0x40, x_pos, y_pos, 0, 0, 0, 0, actor_wt_rubber_wall, 0, 0, 0, 0, 0);
            break;

        case 213:
            actor.init_struct(actor_num, 0xc2, x_pos, y_pos, 0, 0, 0, 0, actor_wt_bonus_item, 3, 2, 0, 0, 1);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 214:
            actor.init_struct(actor_num, 0xc4, x_pos, y_pos, 0, 0, 0, 0, actor_wt_bonus_item, 2, 2, 0, 0, 1);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 215:
            actor.init_struct(actor_num, 0xc6, x_pos, y_pos, 0, 0, 0, 0, actor_wt_bonus_item, 2, 2, 0, 0, 1);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 216:
            actor.init_struct(actor_num, 0xc8, x_pos, y_pos, 0, 0, 0, 0, actor_wt_bonus_item, 2, 2, 0, 0, 1);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 152:
            actor.init_struct(actor_num, 0x98, x_pos, y_pos, 0, 0, 0, 0, actor_wt_big_red_plant, 0, 0x1e, 0, 0, 0);
            break;

        case 217:
            actor.init_struct(actor_num, 2, x_pos, y_pos, 1, 0, 0, 0, actor_wt_spring, 0, 0, y_pos + 1, y_pos + 3, 1);
            actor.draw_function = actor_wt_spring_draw;
            break;

        case 218:
            actor.init_struct(actor_num, 0x1d, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xdc, 0x1e, 0, 0, 0);
            break;

        case 219:
            actor.init_struct(actor_num, 0xdc, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 220:
            actor.init_struct(actor_num, 0xdc, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 221:
            actor.init_struct(actor_num, 0xdd, x_pos, y_pos, 0, 0, 0, 0, actor_wt_frozen_duke_nukum, 0, 0, 0, 0, 0);
            break;

        case 223:
            actor.init_struct(actor_num, 0xdf, x_pos, y_pos + 1, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 224:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xe2, 0xb9, 0, 0, 0);
            break;

        case 225:
            actor.init_struct(actor_num, 0xe2, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 226:
            actor.init_struct(actor_num, 0xe2, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 227:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xe5, 0xb9, 0, 0, 0);
            break;

        case 228:
            actor.init_struct(actor_num, 0xe5, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 229:
            actor.init_struct(actor_num, 0xe5, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 230:
            actor.init_struct(actor_num, 0, x_pos, y_pos, 1, 0, 1, 0, actor_wt_container, 0xe8, 0xb9, 0, 0, 0);
            break;

        case 231:
            actor.init_struct(actor_num, 0xe8, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 232:
            actor.init_struct(actor_num, 0xe8, x_pos, y_pos, 1, 0, 1, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 233:
            actor.init_struct(actor_num, 0xe9, x_pos - 1, y_pos, 0, 0, 0, 0, actor_wt_horizontal_flame, 0, 0, 0, 0, 1);
            actor.draw_function = actor_wt_horizontal_flame_draw;
            break;

        case 234:
            actor.init_struct(actor_num, 0xea, x_pos, y_pos, 0, 0, 0, 0, actor_wt_horizontal_flame, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_horizontal_flame_draw;
            break;

        case 235:
            actor.init_struct(actor_num, 0xeb, x_pos, y_pos, 1, 0, 0, 0, actor_wt_speech_bubble, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_speech_bubble_draw;
            break;

        case 236:
            actor.init_struct(actor_num, 0xed, x_pos, y_pos + 1, 0, 0, 0, 0, actor_wt_acid, 0, 0, 0, 0, 0);
            break;

        case 237:
            actor.init_struct(actor_num, 0xed, x_pos, y_pos + 1, 0, 1, 0, 0, actor_wt_acid, x_pos, y_pos + 1, 0, 0, 1);
            break;

        case 238:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 10);
            break;

        case 239:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 11);
            break;

        case 240:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 12);
            break;

        case 241:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 13);
            break;

        case 242:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 14);
            break;

        case 243:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 15);
            break;


        case 244:
            actor.init_struct(actor_num, 0xf4, x_pos, y_pos, 1, 0, 0, 0, actor_wt_speech_bubble, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_speech_bubble_draw;
            break;

        case 245:
            actor.init_struct(actor_num, 0xf5, x_pos, y_pos, 1, 0, 0, 0, actor_wt_speech_bubble, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_speech_bubble_draw;
            break;

        case 246:
            actor.init_struct(actor_num, 0xf6, x_pos, y_pos, 1, 0, 0, 0, actor_wt_speech_bubble, 0, 0, 0, 0, 0);
            actor.draw_function = actor_wt_speech_bubble_draw;
            break;

        case 247:
            actor.init_struct(actor_num, 0xf7, x_pos, y_pos, 0, 0, 0, 0, actor_wt_switch_multi_use, 0, 0, 0, 0, 0);
            break;

        case 248:
            actor.init_struct(actor_num, 0xf8, x_pos, y_pos, 0, 0, 0, 0, actor_wt_smoke_rising, 0, 0, 0, 0, 1);
            actor.draw_function = actor_wt_smoke_rising_draw;
            break;

        case 249:
            actor.init_struct(actor_num, 0xf9, x_pos, y_pos, 0, 0, 0, 0, actor_wt_smoke_rising, 1, 0, 0, 0, 0);
            actor.draw_function = actor_wt_smoke_rising_draw;
            break;

        case 250:
            actor.init_struct(actor_num, 0xfa, x_pos, y_pos, 1, 0, 0, 0, actor_wt_end_of_level_marker, 0, 0, 0, 0, 0);
            break;

        case 251:
            actor.init_struct(actor_num, 0x19, x_pos, y_pos, 1, 0, 1, 1, actor_wt_green_pruny_cabbage_ball, 2, 0, 0, 0, 0);
            break;

        case 252:
            actor.init_struct(actor_num, 0x9b, x_pos, y_pos + 1, 0, 0, 0, 0, actor_wt_bonus_item, 1, 0, 0, 0, 6);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 253:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 0x10);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 254:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 0x11);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 255:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 0x12);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 256:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 0x13);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 257:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 0x14);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 258:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 0x15);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 259:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 0x16);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 260:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 0x17);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 261:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 0x18);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 262:
            actor.init_struct(actor_num, 0x7d, x_pos, y_pos, 0, 0, 0, 0, actor_wt_hint_dialog, 0, 0, 0, 0, 0x19);
            actor.draw_function = actor_wt_hint_dialog_draw;
            break;

        case 263:
            actor.init_struct(actor_num, 0x1c, x_pos, y_pos, 0, 1, 1, 0, actor_wt_bonus_item, 0, 0, 0, 1, 6);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 264:
            actor.init_struct(actor_num, 1, x_pos, y_pos, 0, 1, 1, 0, actor_wt_bonus_item, 0, 0, 0, 0, 4);
            actor.draw_function = actor_wt_bonus_item_draw;
            break;

        case 265:
            actor.init_struct(actor_num, 0x109, x_pos, y_pos + 3, 1, 0, 0, 0, actor_wt_end_of_level_marker, 1, 0, 0, 0, 0);
            break;

        default :
            gLogging << "Unknown actor " << actor_num << " at pos (" << x_pos << "," << y_pos << ")" << CLogFile::endl;
            return 0;
    }

    return 1;
}

void load_actor(int actor_num, int actorType, int x_pos, int y_pos)
{
    //printf("Loading Actor: %d, (%d,%d)\n", actorType, x_pos, y_pos);
    if (actorType < 0x20)
    {
        switch (actorType)
        {
            case 0: // The player
                if (map_width_in_tiles - 15 >= x_pos)
                {
                    if (map_stride_bit_shift_amt <= 5 || x_pos - 15 < 0)
                    {
                        mapwindow_x_offset = 0;
                    }
                    else
                    {
                        mapwindow_x_offset = x_pos - 15;
                    }
                }
                else
                {
                    mapwindow_x_offset = map_width_in_tiles - 38;
                }

                if (y_pos - 10 < 0)
                {
                    mapwindow_y_offset = 0;
                }
                else
                {
                    mapwindow_y_offset = y_pos - 10;
                }

                gCosmoPlayer.setPos(x_pos, y_pos);
                gCosmoPlayer.syncDrawPosition();
                break;

            case 1: // Platform
                moving_platform_tbl[num_moving_platforms].x = x_pos;
                moving_platform_tbl[num_moving_platforms].y = y_pos;
                num_moving_platforms++;
                break;

            case 2: // Fountains
            case 3:
            case 4:
            case 5:
                mud_fountain_tbl[num_mud_fountains].x = x_pos - 1;
                mud_fountain_tbl[num_mud_fountains].y = y_pos - 1;
                mud_fountain_tbl[num_mud_fountains].direction = 0;
                mud_fountain_tbl[num_mud_fountains].length_counter = 0;
                mud_fountain_tbl[num_mud_fountains].current_height = 0;
                mud_fountain_tbl[num_mud_fountains].max_height = actorType * 3;
                mud_fountain_tbl[num_mud_fountains].pause_counter = 0;
                num_mud_fountains++;
                break;

            case 6:
            case 7:
            case 8:
                add_brightness_obj(actorType - 6, x_pos, y_pos);
                break;
            default: break;
        }
    }

    if (actorType >= 0x1f)
    {
        if (actor_init(actor_num, actorType - 31, x_pos, y_pos) != 0)
        {
            actors.resize(actors.size()+1);
        }
    }
    return;
}

void actor_load_tiles()
{
    uint16 num_tile_info_records;
    uint16 num_tiles;
    actor_tiles = load_tiles("ACTORS.MNI", TileType::TRANSP, &num_tiles);
    gLogging << "Loading " << num_tiles << " actor tiles." << CLogFile::endl;

    actor_sprites = load_tile_info("ACTRINFO.MNI", &num_tile_info_records);
    gLogging << "Loading " << num_tile_info_records << " actor tile info records." << CLogFile::endl;
}


void ActorData::update(const bool draw_only)
{
    if(!draw_function)
        return;


    if (is_deactivated_flag_maybe != 0)
        return;

    if (y > map_max_y_offset + 0x15)
    {
        is_deactivated_flag_maybe = 1;
        return;
    }

    actor_tile_display_func_index = DrawMode::NORMAL;
    if (count_down_timer != 0)
    {
        count_down_timer = count_down_timer - 1;
    }

    if (is_sprite_on_screen(actorInfoIndex, frame_num, x, y) == 0)
    {
        if (update_while_off_screen_flag == 0)
        {
            return;
        }
        actor_tile_display_func_index = DrawMode::INVISIBLE;
    }
    else
    {

        if (can_update_if_goes_off_screen_flag != 0)
        {
            update_while_off_screen_flag = 1;
        }
    }

    if (can_fall_down_flag != 0)
    {
        if (sprite_blocking_check(1, actorInfoIndex, 0, x, y) != 0)
        {
            y = y - 1;
            falling_counter = 0;
        }

        if (sprite_blocking_check(1, actorInfoIndex, 0, x, y + 1) != 0)
        {
            falling_counter = 0;
        }
        else
        {

            if (falling_counter < 5)
            {
                falling_counter = falling_counter + 1;
            }

            if (falling_counter > 1 && falling_counter < 6)
            {
                //y = y + 1;
                y = y + 0.25f;
            }

            if (falling_counter == 5)
            {
                if (sprite_blocking_check(1, actorInfoIndex, 0, x, y + 1) != 0)
                {
                    falling_counter = 0;
                }
                else
                {
                    //y = y + 1;
                    y = y + 0.25f;
                }
            }
        }
    }

    if (is_sprite_on_screen(actorInfoIndex, frame_num, x, y) != 0)
    {
        actor_tile_display_func_index = DrawMode::NORMAL;
    }

    if(!draw_only)
    {
        update_function(this);
    }

    if (explosion_collision_test(actorInfoIndex, frame_num, x, y) != 0 &&
            blow_up_actor_with_bomb(actorInfoIndex, frame_num, x, y) != 0)
    {
        is_deactivated_flag_maybe = 1;
        return;
    }


    draw_function(this);

    return;
}

void actor_update_all(const bool draw_only)
{
    auto &player = gCosmoPlayer;
    player.hit_hint_globe = 0;

    int idx = 0;
    for(auto &actor : actors)
    {
        actor.update(draw_only);
        idx++;
    }

    if (question_mark_code != 0)
    {
        question_mark_code = 0;
    }
    return;
}

BlockingType sprite_blocking_check(int blocking_dir, int actorInfoIndex, int frame_num, int x_pos, int y_pos)
{
    uint16 sprite_height = actor_sprites[actorInfoIndex].frames[frame_num].height;
    uint16 sprite_width = actor_sprites[actorInfoIndex].frames[frame_num].width;

    switch (blocking_dir)
    {
        case 0:
            for (int i = 0; i < sprite_width; i++)
            {
                if(map_get_tile_attr(x_pos + i, y_pos - sprite_height + 1) & TILE_ATTR_BLOCK_UP)
                {
                    return BLOCKED;
                }
            }
            break;

        case 1:
            for (int i = 0; i < sprite_width; i++)
            {
                uint16 tile_attr = map_get_tile_attr(x_pos + i, y_pos);
                if(tile_attr & TILE_ATTR_SLOPED)
                {
                    return SLOPE;
                }
                if(tile_attr & TILE_ATTR_BLOCK_DOWN)
                {
                    return BLOCKED;
                }
            }
            break;

        case 2:
            if(x_pos == 0)
            {
                return BLOCKED;
            }

            for(int i=0;i<sprite_height;i++)
            {
                uint16 tile_attr = map_get_tile_attr(x_pos, y_pos - i);
                if(i == 0 &&
                        (tile_attr & TILE_ATTR_SLOPED) &&
                        (map_get_tile_attr(x_pos, y_pos - 1) & TILE_ATTR_BLOCK_LEFT) == 0)
                {
                    return SLOPE;
                }

                if(tile_attr & TILE_ATTR_BLOCK_LEFT)
                {
                    return BLOCKED;
                }
            }
            break;

        case 3:
            if (x_pos + sprite_width == map_width_in_tiles)
            {
                return BLOCKED;
            }

            for(int i=0;i<sprite_height;i++)
            {
                uint16 tile_attr = map_get_tile_attr(x_pos + sprite_width - 1, y_pos - i);
                if(i == 0 &&
                   tile_attr & TILE_ATTR_SLOPED &&
                   (map_get_tile_attr(x_pos + sprite_width - 1, y_pos - 1) & TILE_ATTR_BLOCK_RIGHT) == 0)
                {
                    return SLOPE;
                }

                if(tile_attr & TILE_ATTR_BLOCK_RIGHT)
                {
                    return BLOCKED;
                }
            }
            break;

        default: break;
    }

    return NOT_BLOCKED;
}

int is_sprite_on_screen(int actorInfoIndex, int frame_num, int x_pos, int y_pos)
{
    assert(actorInfoIndex < int(actor_sprites.size()));

    auto &actorSpr = actor_sprites[actorInfoIndex];

    if(frame_num >= int(actorSpr.frames.size()))
        return false;

    uint16 sprite_height = actorSpr.frames[frame_num].height;
    uint16 sprite_width = actorSpr.frames[frame_num].width;

    const auto gameRes = gVideoDriver.getGameResolution();
    const auto gameResTileBased = gameRes/8;

    if(x_pos + sprite_width < mapwindow_x_offset)
        return false;

    if(x_pos > mapwindow_x_offset + gameResTileBased.dim.x+2)
        return false;

    if(y_pos + sprite_height < mapwindow_y_offset)
        return false;

    if(y_pos > mapwindow_y_offset + gameResTileBased.dim.y+2)
        return false;

    return true;
}

TileInfo *actor_get_tile_info(int actorInfoIndex, int frame_num)
{
    assert(actorInfoIndex < int(actor_sprites.size()));
    assert(!actor_sprites[actorInfoIndex].frames.empty());

    if(frame_num >= actor_sprites[actorInfoIndex].num_frames)
       frame_num =  actor_sprites[actorInfoIndex].frames.size()-1;

    return &actor_sprites[actorInfoIndex].frames[frame_num];
}

ActorData *get_actor(uint16 actor_num)
{
    assert(actor_num < actors.size());
    return &actors[actor_num];
}


namespace cosmos_engine
{


void ActorManager::setMapPtr(std::shared_ptr<CMap> &mapPtr)
{
    mpMap = mapPtr;
}

void ActorManager::display_sprite_maybe(const int actorInfoIndex,
                         const int frame_num,
                         const float x_pos,
                         const float y_pos,
                         const DrawMode draw_mode)
{
    GsVec2D<int> scroll(0,0);

    if(mpMap)
    {
        scroll = mpMap->getScrollCoords(1);
    }

    display_sprite_maybe(actorInfoIndex,
                         frame_num,
                         x_pos,
                         y_pos,
                         draw_mode,
                         scroll);
}

void ActorManager::display_sprite_maybe(const int actorInfoIndex,
                         const int frame_num,
                         const float x_pos,
                         const float y_pos,
                         const DrawMode draw_mode,
                         const GsVec2D<int> &scroll)
{
    if(actor_sprites[actorInfoIndex].num_frames==0)
    {
        gLogging << "WARN: actorInfoIndex " << actorInfoIndex
                 << " has no frames!. Wanted frame_num: "
                 << frame_num  << CLogFile::endl;
        return;
    }

    const auto numFrames = actor_sprites[actorInfoIndex].num_frames;

    // Protection against some broken frame accesses
    if(frame_num >= numFrames)
        return;

    TileInfo *info = &actor_sprites[actorInfoIndex].frames[frame_num];
    Tile *tile = &actor_tiles[info->tile_num];

    if(draw_mode == DrawMode::FLIPPED)
    {
        for(int y=info->height-1;y >= 0;y--)
        {
            const uint16 screen_y = (y_pos - info->height + 1 + y) * 8 - scroll.y;

            for (int x = 0; x < info->width; x++)
            {
                const uint16 screen_x = (x_pos + x) * 8 - scroll.x;
                uint16 tile_attr = map_get_tile_attr(x_pos+x, y_pos - info->height + y + 1);
                if(!(tile_attr & TILE_ATTR_IN_FRONT))
                {
                    video_draw_tile_flipped(tile, screen_x, screen_y);
                }
                tile++;
            }
        }
        return;
    }

    for(int y=0; y < info->height; y++)
    {
        int screen_y = (y_pos - info->height + 1 + y) * 8 - scroll.y;

        for(int x=0; x < info->width; x++)
        {
            int screen_x = (x_pos + x) * 8 - scroll.x;
            uint16 tile_attr = map_get_tile_attr(x_pos+x, y_pos - info->height + y + 1);
            if(draw_mode == DrawMode::ON_DIALOG)
            {
                screen_y = (y_pos - info->height + 1 + y) * 8;
                screen_x = (x_pos + x + 1) * 8;
                video_draw_tile(tile, screen_x, screen_y);
            }
            else
            {
                if((!(tile_attr & TILE_ATTR_IN_FRONT) ||
                    draw_mode == DrawMode::SOLID ||
                    draw_mode == DrawMode::ON_DIALOG))
                {
                    if (draw_mode == DrawMode::SOLID_WHITE)
                    {
                        video_draw_tile_solid_white(tile, screen_x, screen_y);
                    }
                    else if(draw_mode == DrawMode::SOLID_LIGHT)
                    {
                        video_draw_tile_light(tile, screen_x, screen_y);
                    }
                    else
                    {
                        video_draw_tile(tile, screen_x, screen_y);
                    }
                }
            }
            tile++;
        }
    }
}

};

